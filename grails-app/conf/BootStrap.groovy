import com.osem.auth.*
import com.osem.Company
import com.osem.Position
import com.osem.Dept

class BootStrap {

    def init = { servletContext ->
        def roleAdmin = Authority.findByAuthority('ROLE_ADMIN')?:new Authority(authority:'ROLE_ADMIN').save(flush:true)
        def roleUser = Authority.findByAuthority('ROLE_HR')?:new Authority(authority:'ROLE_HR').save(flush:true)
		
        def pos1 = Position.findByPosName('ANGGOTA')?: new Position(posName:'ANGGOTA').save(flush:true)
        def pos2 = Position.findByPosName('DANRU')?: new Position(posName:'DANRU').save(flush:true)
        def pos3 = Position.findByPosName('WADANRU')?: new Position(posName:'WADANRU').save(flush:true)
        def pos4 = Position.findByPosName('DAN UNIT')?: new Position(posName:'DAN UNIT').save(flush:true)
        def pos5 = Position.findByPosName('WADAN UNIT')?: new Position(posName:'WADAN UNIT').save(flush:true)
        def pos6 = Position.findByPosName('DAN SEKTOR')?: new Position(posName:'DAN SEKTOR').save(flush:true)
        def pos7 = Position.findByPosName('WADAN SEKTOR')?: new Position(posName:'WADAN SEKTOR').save(flush:true)
        def pos8 = Position.findByPosName('SEKWAN')?: new Position(posName:'SEKWAN').save(flush:true)
        def pos9 = Position.findByPosName('CS')?: new Position(posName:'CS').save(flush:true)
        def pos10 = Position.findByPosName('OB')?: new Position(posName:'OB').save(flush:true)
        def pos11 = Position.findByPosName('STAFF')?: new Position(posName:'STAFF').save(flush:true)
        def pos12 = Position.findByPosName('SUPERVISOR')?: new Position(posName:'SUPERVISOR').save(flush:true)
        def pos13 = Position.findByPosName('MANAGER')?: new Position(posName:'MANAGER').save(flush:true)
        def pos14 = Position.findByPosName('DIREKTUR')?: new Position(posName:'DIREKTUR').save(flush:true)
        
        def dept1 = Dept.findByDeptName('OPERASIONAL')?: new Dept(deptName:'OPERASIONAL').save(flush:true)
        def dept2 = Dept.findByDeptName('HR')?: new Dept(deptName:'HR').save(flush:true)
        def dept3 = Dept.findByDeptName('FINANCE')?: new Dept(deptName:'FINANCE').save(flush:true)
        def dept4 = Dept.findByDeptName('TRAINER')?: new Dept(deptName:'TRAINER').save(flush:true)
        def dept5 = Dept.findByDeptName('MARKETING')?: new Dept(deptName:'MARKETING').save(flush:true)
        def dept6 = Dept.findByDeptName('AUDIT')?: new Dept(deptName:'AUDIT').save(flush:true)
        def dept7 = Dept.findByDeptName('IT')?: new Dept(deptName:'IT').save(flush:true)
        def dept8 = Dept.findByDeptName('LEGAL')?: new Dept(deptName:'LEGAL').save(flush:true)
        def dept9 = Dept.findByDeptName('GA')?: new Dept(deptName:'GA').save(flush:true)
        
        def comp2 = Company.findByCompName('PT. MITRA GARDA MANDIRI')?: new Company(compInitial:'MGM',compName:'PT. MITRA GARDA MANDIRI',compAddress:'Ruko Blog Blue Dot Com Paramount Blog D, No.26/Type C',compCity:'Tangerang',compPhone:'(021) 45768979', compEmail:'marketing@mitragardamandiri.co.id', compWebsite:'http://www.mitragardamandiri.co.id',compLogo:'MGM_KOP.jpg').save(flush:true,failOnError:true)
        def userHr2 = User.findByUsername('hr@mitragardamandiri.co.id')?:new User(username:'hr@mitragardamandiri.co.id',password:'mgm123',fullname:'HR MGM',company:comp2, position: pos13, dept: dept2).save(flush:true,failOnError:true)
		
        if (!userHr2.authorities.contains(roleUser)) {
            UserAuthority.create userHr2, roleUser
        }

        def comp = Company.findByCompName('PT. MITRA CITRA MANDIRI')?: new Company(compInitial:'MCM',compName:'PT. MITRA CITRA MANDIRI',compAddress:'Ruko Blog Blue Dot Com Paramount Blog D, No.26/Type C',compCity:'Tangerang',compPhone:'(021) 45768979', compEmail:'marketing@mitrachitramandiri.co.id', compWebsite:'http://www.mitrachitramandiri.co.id',compLogo:'MCM_KOP.jpg').save(flush:true,failOnError:true)
        def userHr = User.findByUsername('hr@mcm.co.id')?:new User(username:'hr@mcm.co.id',password:'mcm123',fullname:'HR USER',company:comp, position: pos13, dept: dept2).save(flush:true,failOnError:true)

        if (!userHr.authorities.contains(roleUser)) {
            UserAuthority.create userHr, roleUser
        }
    }
    def destroy = {
    }
}
