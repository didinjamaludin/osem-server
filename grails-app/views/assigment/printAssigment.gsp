<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Pernyataan Siap Tugas</title>
        <link rel="license"
        href="http://www.opensource.org/licenses/mit-license/">
        <style>
            /* reset */
            * {
            border: 0;
            box-sizing: content-box;
            color: inherit;
            font-family: inherit;
            font-size: inherit;
            font-style: inherit;
            font-weight: inherit;
            line-height: inherit;
            margin: 0;
            padding: 0;
            text-decoration: none;
            vertical-align: top;
            }

            /* content editable */
            *[contenteditable] {
            border-radius: 0.25em;
            min-width: 1em;
            outline: 0;
            }

            *[contenteditable] {
            cursor: pointer;
            }

            *[contenteditable]:hover, *[contenteditable]:focus, td:hover *[contenteditable],
            td:focus *[contenteditable], img.hover {
            background: #DEF;
            box-shadow: 0 0 1em 0.5em #DEF;
            }

            span[contenteditable] {
            display: inline-block;
            }

            /* heading */
            h1 {
            font: bold 36px sans-serif;
            letter-spacing: 0.1em;
            text-align: center;
            text-transform: uppercase;
            }

            /* page */
            html {
            font: 16px/1 'Open Sans', sans-serif;
            overflow: auto;
            padding: 0.5in;
            }

            html {
            background: #999;
            cursor: default;
            }

            body {
            box-sizing: border-box;
            height: 11.69in;
            margin: 0 auto;
            overflow: hidden;
            padding: 0.5in;
            width: 8.27in;
            }

            body {
            background: #FFF;
            border-radius: 1px;
            box-shadow: 0 0 1in -0.25in rgba(0, 0, 0, 0.5);
            }

            /* header */
            header {
            margin: 0;
            padding-bottom:5px;
            border-bottom: solid 1px #999;
            }

            header:after {
            clear: both;
            content: "";
            display: table;
            }

            header .header-logo {
            float:left;
            width:100%;
            text-align:center;
            }

            /* article */
            article, article address, table.meta, table.inventory {
            margin: 0.1em 0 6em;
            padding-top: 1em;
            border-top: solid 3px #999;
            }

            article:after {
            clear: both;
            content: "";
            display: table;
            }

            article  {
            font-size:12px;	
            line-height: 15px;

            }

            article h1 {
            font-size: 18px;
            text-decoration: underline;
            }

            article p {
            padding: 10px 0 0 0;
            }

            article table {
            border: 0;
            }

            article table td {
            padding: 5px;
            }

            ol {
            margin-bottom: 10px;
            }

            ol li {
            padding: 5px 10px;
            margin-left: 20px;
            }


            footer {
            position: relative;
            bottom: 0;
            font-size: 10px;
            border-bottom: solid 20px red;
            }

            /* javascript */
            .add, .cut {
            border-width: 1px;
            display: block;
            font-size: .8rem;
            padding: 0.25em 0.5em;
            float: left;
            text-align: center;
            width: 0.6em;
            }

            .add, .cut {
            background: #9AF;
            box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
            background-image: -moz-linear-gradient(#00ADEE 5%, #0078A5 100%);
            background-image: -webkit-linear-gradient(#00ADEE 5%, #0078A5 100%);
            border-radius: 0.5em;
            border-color: #0076A3;
            color: #FFF;
            cursor: pointer;
            font-weight: bold;
            text-shadow: 0 -1px 2px rgba(0, 0, 0, 0.333);
            }

            .add {
            margin: -2.5em 0 0;
            }

            .add:hover {
            background: #00ADEE;
            }

            .cut {
            opacity: 0;
            position: absolute;
            top: 0;
            left: -1.5em;
            }

            .cut {
            -webkit-transition: opacity 100ms ease-in;
            }

            tr:hover .cut {
            opacity: 1;
            }

            .control {
            position:absolute;
            top: 10px;
            left: 2.3in;
            }

            .control a {
            padding: 3px 10px;
            background: #ccc;
            font-size: 12px;
            font-weight: bold;
            margin: 5px;
            }

            .control a:hover {
            background: #fff;
            }

            @media print {
            * {
            -webkit-print-color-adjust: exact;
            }
            html {
            background: none;
            padding: 0;
            }
            body {
            box-shadow: none;
            margin: 0;
            }
            span:empty {
            display: none;
            }
            .add, .cut {
            display: none;
            }
            .control {
            display:none;
            }
            }

            @page {
            margin: 0;
            }
        </style>
    </head>
    <body>
        <div class="control"><a href="#" onclick="PrintWebPage()">Print</a><g:link controller="hr">Close</g:link></div>
            <header>
                <div class="header-logo">
                    <img alt="" src="https://s3-ap-southeast-1.amazonaws.com/static.mitragardamandiri/${userInstance?.company?.compLogo}" />
            </div>
        </header>
        <article>
            <h1>SURAT TUGAS</h1>
            <h4 style="border: none; text-align: center; margin-bottom: 20px;">No. ${assigmentInstance?.assCode}</h4>

            <table>
                <tr>
                    <td><strong>I.</strong></td>
                    <td>Pertimbangan</td>
                    <td>:</td>
                    <td>Dalam rangka pelaksanaan membantu tugas Kepolisian Nasional Republik
                        Indonesia di bidang Satuan Pengamanan WilayahPolda Metro Jaya dan
                        Sekitarnya dipandang perlu meengeluarkan Surat Perintah Tugas</td>
                </tr>
                <tr>
                    <td><strong>II.</strong></td>
                    <td>Dasar</td>
                    <td>:</td>
                    <td>Surat Kontrak Kerja dibidang keamanan PT. ${userInstance?.company?.compInitial} ( ${userInstance?.company?.compName} )</td>
                </tr>
            </table>

            <h3 style="text-align:center;margin:20px 0;">DIPERINTAHKAN</h3>
            <table>
                <tr>
                    <td><strong>III.</strong></td>
                    <td>Kepada</td>
                    <td>:</td>
                    <td style="padding-top: 0;">
                        <table>
                            <tr>
                                <td width="25%">Nama</td>
                                <td width="75%">:&nbsp;${assigmentInstance?.person?.fullname}</td>
                            </tr>
                            <tr>
                                <td>Tempat/Tgl Lahir</td>
                                <td>:&nbsp;${assigmentInstance?.person?.pob}/<g:formatDate date="${assigmentInstance?.person?.dob}" format="dd-MM-yy"/></td>
                            </tr>
                            <tr>
                                <td>Alamat</td>
                                <td>:&nbsp;${assigmentInstance?.person?.currentAddr}</td>
                            </tr>
                            <tr>
                                <td>HP</td>
                                <td>:&nbsp;${assigmentInstance?.person?.hp}</td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td><strong>IV.</strong></td>
                    <td>Untuk</td>
                    <td>:</td>
                    <td style="padding-top: 0;">
                        <ol>
                            <li>Melaksanakan tugas sebagai satuan pengamanan</li>
                            <li>Melaksanakan perintah ini dengan penuh rasa tanggung jawab dan mentaati
                                peraturan ${userInstance?.company?.compName}</li>
                                <g:if test="${assigmentInstance?.assStatus.equals('Baru')}">
                                <li>Sebagai pegawai baru</li>
                                </g:if>
                                <g:else>
                                <li>Menggantikan ${assigmentInstance?.repName}</li>
                                </g:else>
                        </ol>
                    </td>
                </tr>
            </table>

            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <table style="width:300px;float:right">
                <tr>
                    <td width="40%">Dikeluarkan di:</td>
                    <td width="60%">${userInstance?.company?.compCity}</td>
                </tr>
                <tr>
                    <td>Pada tanggal:</td>
                    <td><g:formatDate date="${new Date()}" format="dd MMMMM yyyy" /></td>
                </tr>
            </table>

            <table width="100%" class="sign-table">
                <tr>
                    <td style="text-align: center">&nbsp;</td>
                    <td style="text-align: center">${userInstance?.company?.compName}</td>
                </tr>
                <tr>
                    <td style="text-align: center">Tanda tangan pemegang</td>
                    <td style="text-align: center">HRD</td>
                </tr>
                <tr>
                    <td style="height:80px;">&nbsp;</td>
                    <td style="height:80px;">&nbsp;</td>
                </tr>
                <tr>
                    <td style="text-align: center;"><span style="border-bottom:solid 1px #999;width:150px;">${assigmentInstance?.person?.fullname}</span></td>
                    <td style="text-align: center;"><span style="border-bottom:solid 1px #999;width:150px;">${userInstance?.fullname}</span></td>
                </tr>
                <tr>
                    <td style="text-align: center">${assigmentInstance?.position?.posName}</td>
                    <td style="text-align: center">${userInstance?.position?.posName} ${userInstance?.dept.deptName}</td>
                </tr>
            </table>

        </article>
        <footer>
            <p style="text-align:center"><strong>${userInstance?.company?.compName}</strong><br>
                ${userInstance?.company?.compAddress}, ${userInstance?.company?.compCity}, Indonesia<br>
                Phone : ${userInstance?.company?.compPhone}<br>
                Email : ${userInstance?.company?.compEmail}<br>
                Website : ${userInstance?.company?.compWebsite}</p>
        </footer>
        <script>
            function PrintWebPage() {
            window.print();
            }
        </script>
    </body>
</html>