

<%@ page import="com.osem.Assigment" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<g:set var="entityName"
	value="${message(code: 'assigment.label', default: 'Assigment')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
</head>
<body>
	<div class="col-md-2 bootstrap-admin-col-left">
		<ul class="nav navbar-collapse collapse bootstrap-admin-navbar-side">
			<li><g:link action="index">
					<i class="glyphicon glyphicon-list-alt"></i>
					<g:message code="default.list.label" args="[entityName]" />
				</g:link></li>
			<li><g:link action="create">
					<i class="glyphicon glyphicon-plus"></i>
					<g:message code="default.new.label" args="[entityName]" />
				</g:link></li>
		</ul>
	</div>

	<div class="col-md-10">
		<div class="row">
			<div class="col-lg-12">
				<div class="page-header">
					<h1>
						<g:message code="default.show.label" args="[entityName]" />
					</h1>
				</div>
				<g:if test="${flash.message}">
					<div class="page-body">
						<div class="alert alert-success" role="status">
							<a class="close" data-dismiss="alert" href="#">&times;</a>${flash.message}
						</div>
					</div>
				</g:if>
				<g:hasErrors bean="${assigmentInstance}">
					<div class="page-body">
						<ul class="alert alert-danger" role="alert">
							<g:eachError bean="${assigmentInstance}" var="error">
								<li
									<g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message
										error="${error}" /></li>
							</g:eachError>
						</ul>
					</div>
				</g:hasErrors>
			</div>
		</div>

		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default bootstrap-admin-no-table-panel">
					<div
						class="bootstrap-admin-no-table-panel-content bootstrap-admin-panel-content collapse in">
						<dl class="dl-horizontal">
							
				<g:if test="${assigmentInstance?.assCode}">
					<dt><g:message code="assigment.assCode.label" default="Ass Code" /></dt>
					
						<dd><g:fieldValue bean="${assigmentInstance}" field="assCode"/></dd>
					
				</g:if>
			
				<g:if test="${assigmentInstance?.assDate}">
					<dt><g:message code="assigment.assDate.label" default="Ass Date" /></dt>
					
						<dd><g:formatDate date="${assigmentInstance?.assDate}" /></dd>
					
				</g:if>
			
				<g:if test="${assigmentInstance?.assGroup}">
					<dt><g:message code="assigment.assGroup.label" default="Ass Group" /></dt>
					
						<dd><g:fieldValue bean="${assigmentInstance}" field="assGroup"/></dd>
					
				</g:if>
			
				<g:if test="${assigmentInstance?.person}">
					<dt><g:message code="assigment.person.label" default="Person" /></dt>
					
						<dd><g:link controller="person" action="show" id="${assigmentInstance?.person?.id}">${assigmentInstance?.person?.encodeAsHTML()}</g:link></dd>
					
				</g:if>
			
				<g:if test="${assigmentInstance?.subunit}">
					<dt><g:message code="assigment.subunit.label" default="Subunit" /></dt>
					
						<dd><g:link controller="subUnit" action="show" id="${assigmentInstance?.subunit?.id}">${assigmentInstance?.subunit?.encodeAsHTML()}</g:link></dd>
					
				</g:if>
			
				<g:if test="${assigmentInstance?.unit}">
					<dt><g:message code="assigment.unit.label" default="Unit" /></dt>
					
						<dd><g:link controller="unit" action="show" id="${assigmentInstance?.unit?.id}">${assigmentInstance?.unit?.encodeAsHTML()}</g:link></dd>
					
				</g:if>
			
						</dl>
						<g:form url="[resource:assigmentInstance, action:'delete']"
							method="DELETE">
							<fieldset class="buttons">
								<g:link class="btn btn-primary" action="edit"
									resource="${assigmentInstance}">
									<g:message code="default.button.edit.label" default="Edit" />
								</g:link>
								<g:actionSubmit class="btn btn-default" action="delete"
									value="${message(code: 'default.button.delete.label', default: 'Delete')}"
									onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
							</fieldset>
						</g:form>
					</div>
				</div>
			</div>
		</div>

	</div>
</body>
</html>
