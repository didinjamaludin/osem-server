<%@ page import="com.osem.Assigment"%>

<div
	class="form-group fieldcontain ${hasErrors(bean: assigmentInstance, field: 'person', 'error')} ">
	<label class="col-lg-2 control-label" for="person"> <g:message
			code="assigment.person.label" default="Person" />

	</label>
	<div class="col-lg-10">
		<g:if test="${params?.action?.equals('create')}">
			<g:select class="form-control" id="person" name="person.id"
				from="${com.osem.Person.list()}" optionKey="id" required=""
				value="${params?.personid}" />
		</g:if>
		<g:else>
			<g:select class="form-control" id="person" name="person.id"
				from="${com.osem.Person.list()}" optionKey="id" required=""
				value="${assigmentInstance?.person?.id}" />
		</g:else>
	</div>

</div>

<div
	class="form-group fieldcontain ${hasErrors(bean: assigmentInstance, field: 'assCode', 'error')} ">
	<label class="col-lg-2 control-label" for="assCode"> <g:message
			code="assigment.assCode.label" default="Kode Surat Tugas" />

	</label>
	<div class="col-lg-10">
		<g:if test="${params?.action?.equals('create')}">
			<g:textField class="form-control" name="assCode" value="${assgNbr}" />
		</g:if>
		<g:else>
			<g:textField class="form-control" name="assCode"
				value="${assigmentInstance?.assCode}" />
		</g:else>
	</div>

</div>

<div
	class="form-group fieldcontain ${hasErrors(bean: assigmentInstance, field: 'assDate', 'error')} ">
	<label class="col-lg-2 control-label" for="assDate"> <g:message
			code="assigment.assDate.label" default="Tgl Penugasan" />

	</label>
	<div class="col-lg-10">
		<div class="form-control">
			<g:datePicker class="form-control" name="assDate" precision="day"
				value="${assigmentInstance?.assDate}" />
		</div>
	</div>

</div>

<div
	class="form-group fieldcontain ${hasErrors(bean: assigmentInstance, field: 'unit', 'error')} ">
	<label class="col-lg-2 control-label" for="unit"> <g:message
			code="assigment.unit.label" default="Unit" />

	</label>
	<div class="col-lg-10">
		<g:select id="unit" name="unit.id" from="${com.osem.Unit.list()}"
			optionKey="id" required="" value="${assigmentInstance?.unit?.id}"
			class="form-control"
			onchange="${remoteFunction(
            controller:'subUnit', 
            action:'getSubUnitFromUnit', 
            params:'\'id=\' + escape(this.value)', 
            onSuccess:'updateSubUnit(data)')}" />
	</div>
</div>

<div
	class="form-group fieldcontain ${hasErrors(bean: assigmentInstance, field: 'subunit', 'error')} ">
	<label class="col-lg-2 control-label" for="subunit"> <g:message
			code="assigment.subunit.label" default="Sub Unit" />
	</label>
	<div class="col-lg-10">
		<g:hiddenField name="subunitid"
			value="${assigmentInstance?.subunit?.id}" />
		<select id="subunit" name="subunit.id" class="form-control">
		</select>
	</div>
</div>

<div
	class="form-group fieldcontain ${hasErrors(bean: assigmentInstance, field: 'dept', 'error')} ">
	<label class="col-lg-2 control-label" for="dept"> <g:message
			code="assigment.subunit.label" default="Dept" />
	</label>
	<div class="col-lg-10">
		<g:select id="dept" name="dept.id" from="${com.osem.Dept.list()}"
			optionKey="id" required="" value="${assigmentInstance?.dept?.id}"
			class="form-control" />
	</div>
</div>

<div
	class="form-group fieldcontain ${hasErrors(bean: assigmentInstance, field: 'position', 'error')} ">
	<label class="col-lg-2 control-label" for="position"> <g:message
			code="assigment.subunit.label" default="Posisi/Jabatan" />
	</label>
	<div class="col-lg-10">
		<g:select id="position" name="position.id" from="${com.osem.Position.list()}"
			optionKey="id" required="" value="${assigmentInstance?.position?.id}"
			class="form-control" />
	</div>
</div>


<div
	class="form-group fieldcontain ${hasErrors(bean: assigmentInstance, field: 'assGroup', 'error')} ">
	<label class="col-lg-2 control-label" for="assGroup"> <g:message
			code="assigment.assGroup.label" default="Regu" />

	</label>
	<div class="col-lg-10">
		<g:field class="form-control" type="number" name="assGroup"
			value="${assigmentInstance.assGroup}" />
	</div>

</div>

<div
	class="form-group fieldcontain ${hasErrors(bean: assigmentInstance, field: 'assStatus', 'error')} ">
	<label class="col-lg-2 control-label" for="assStatus"> <g:message
			code="assigment.assStatus.label" default="Status Penugasan" />

	</label>
	<div class="col-lg-10">
                <g:select from="${['Baru','Pengganti']}" name="assStatus" class="form-control" value="${assigmentInstance.assStatus}" />
	</div>

</div>

<div
	class="form-group fieldcontain ${hasErrors(bean: assigmentInstance, field: 'repName', 'error')} ">
	<label class="col-lg-2 control-label" for="repName"> <g:message
			code="assigment.assGroup.label" default="Yang digantikan" />

	</label>
	<div class="col-lg-10">
		<g:field class="form-control" type="text" name="repName"
			value="${assigmentInstance.repName}" />
	</div>

</div>

<div class="form-group">
	<label class="col-lg-2 control-label" for="printAssg"> Cetak
		Surat Tugas </label>
	<div class="col-lg-10">
		<div class="form-control">
			<g:checkBox name="printAssg" />
		</div>
	</div>
</div>

<script>	
	function updateSubUnit(data) {
		var htm="";
   		$("#subunit option").remove();
    	for(var i=0;i<data.length;i++) {
        	htm+="<option value='"+data[i].id+"'>"+data[i].subUnitCode+"</option>";
    	}
    	$("#subunit").append(htm);
    	if($("#subunit").val()) {
    		$("#subunit").val($("#subunitid").val());
        }
	}

	function getSubUnit() {
		$.ajax({
			url: "${request.contextPath}/subUnit/getSubUnitFromUnit",
			data : 'id=' + $("#unit").val()
		}).done(function(data) {
			updateSubUnit(data);
		});
	}
	
	$(function() {
		getSubUnit();

		$('input:checkbox').iCheck({
			checkboxClass : 'icheckbox_minimal-grey',
			radioClass : 'iradio_minimal-grey'
		});
	});
	
</script>

