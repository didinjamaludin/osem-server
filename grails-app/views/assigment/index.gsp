

<%@ page import="com.osem.Assigment" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<g:set var="entityName"
	value="${message(code: 'assigment.label', default: 'Assigment')}" />
<title><g:message code="default.list.label" args="[entityName]" /></title>
</head>
<body>
	<div class="col-md-2 bootstrap-admin-col-left">
		<ul class="nav navbar-collapse collapse bootstrap-admin-navbar-side">
			<li><g:link action="create">
					<i class="glyphicon glyphicon-plus"></i>
					<g:message code="default.new.label" args="[entityName]" />
				</g:link></li>
		</ul>
	</div>

	<div class="col-md-10">
		<div class="row">
			<div class="col-lg-12">
				<div class="page-header">
					<h1>
						<g:message code="default.list.label" args="[entityName]" />
					</h1>
				</div>
				<g:if test="${flash.message}">
					<div class="page-body">
						<div class="alert alert-success" role="status">
							<a class="close" data-dismiss="alert" href="#">&times;</a>${flash.message}
						</div>
					</div>
				</g:if>
			</div>
		</div>

		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="bootstrap-admin-panel-content">
						<g:if test="${assigmentInstanceList}">
							<table class="table bootstrap-admin-table-with-actions">
								<thead>
									<tr>
										
						<g:sortableColumn property="assCode" title="${message(code: 'assigment.assCode.label', default: 'Ass Code')}" />
					
						<g:sortableColumn property="assDate" title="${message(code: 'assigment.assDate.label', default: 'Ass Date')}" />
					
						<g:sortableColumn property="assGroup" title="${message(code: 'assigment.assGroup.label', default: 'Ass Group')}" />
					
						<th><g:message code="assigment.person.label" default="Person" /></th>
					
						<th><g:message code="assigment.subunit.label" default="Subunit" /></th>
					
						<th><g:message code="assigment.unit.label" default="Unit" /></th>
					
									</tr>
								</thead>
								<tbody>
									<g:each in="${assigmentInstanceList}" status="i"
										var="assigmentInstance">
										<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
											
						<td><g:link action="show" id="${assigmentInstance.id}">${fieldValue(bean: assigmentInstance, field: "assCode")}</g:link></td>
					
						<td><g:formatDate date="${assigmentInstance.assDate}" /></td>
					
						<td>${fieldValue(bean: assigmentInstance, field: "assGroup")}</td>
					
						<td>${fieldValue(bean: assigmentInstance, field: "person")}</td>
					
						<td>${fieldValue(bean: assigmentInstance, field: "subunit")}</td>
					
						<td>${fieldValue(bean: assigmentInstance, field: "unit")}</td>
					
										</tr>
									</g:each>
								</tbody>
							</table>
							<div class="pagination">
								<g:paginate total="${assigmentInstanceCount ?: 0}" />
							</div>
						</g:if>
						<g:else>
							No assigment found
						</g:else>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
