<%@ page import="com.osem.Division" %>



<div class="form-group fieldcontain ${hasErrors(bean: divisionInstance, field: 'divCode', 'error')} ">
	<label class="col-lg-2 control-label" for="divCode">
		<g:message code="division.divCode.label" default="Div Code" />
		
	</label>
	<div class="col-lg-10"><g:textField class="form-control" name="divCode" value="${divisionInstance?.divCode}" /></div>

</div>

<div class="form-group fieldcontain ${hasErrors(bean: divisionInstance, field: 'divName', 'error')} ">
	<label class="col-lg-2 control-label" for="divName">
		<g:message code="division.divName.label" default="Div Name" />
		
	</label>
	<div class="col-lg-10"><g:textField class="form-control" name="divName" value="${divisionInstance?.divName}" /></div>

</div>

