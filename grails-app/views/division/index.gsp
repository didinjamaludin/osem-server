

<%@ page import="com.osem.Division" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<g:set var="entityName"
	value="${message(code: 'division.label', default: 'Division')}" />
<title><g:message code="default.list.label" args="[entityName]" /></title>
</head>
<body>
	<div class="col-md-2 bootstrap-admin-col-left">
		<ul class="nav navbar-collapse collapse bootstrap-admin-navbar-side">
			<li><g:link action="create">
					<i class="glyphicon glyphicon-plus"></i>
					<g:message code="default.new.label" args="[entityName]" />
				</g:link></li>
		</ul>
	</div>

	<div class="col-md-10">
		<div class="row">
			<div class="col-lg-12">
				<div class="page-header">
					<h1>
						<g:message code="default.list.label" args="[entityName]" />
					</h1>
				</div>
				<g:if test="${flash.message}">
					<div class="page-body">
						<div class="alert alert-success" role="status">
							<a class="close" data-dismiss="alert" href="#">&times;</a>${flash.message}
						</div>
					</div>
				</g:if>
			</div>
		</div>

		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="bootstrap-admin-panel-content">
						<g:if test="${divisionInstanceList}">
							<table class="table bootstrap-admin-table-with-actions">
								<thead>
									<tr>
										
						<g:sortableColumn property="divCode" title="${message(code: 'division.divCode.label', default: 'Div Code')}" />
					
						<g:sortableColumn property="divName" title="${message(code: 'division.divName.label', default: 'Div Name')}" />
					
									</tr>
								</thead>
								<tbody>
									<g:each in="${divisionInstanceList}" status="i"
										var="divisionInstance">
										<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
											
						<td><g:link action="show" id="${divisionInstance.id}">${fieldValue(bean: divisionInstance, field: "divCode")}</g:link></td>
					
						<td>${fieldValue(bean: divisionInstance, field: "divName")}</td>
					
										</tr>
									</g:each>
								</tbody>
							</table>
							<div class="pagination">
								<g:paginate total="${divisionInstanceCount ?: 0}" />
							</div>
						</g:if>
						<g:else>
							No division found
						</g:else>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
