<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Pernyataan Siap Tugas</title>
        <link rel="license"
        href="http://www.opensource.org/licenses/mit-license/">
        <style>
            /* reset */
            * {
            border: 0;
            box-sizing: content-box;
            color: inherit;
            font-family: inherit;
            font-size: inherit;
            font-style: inherit;
            font-weight: inherit;
            line-height: inherit;
            margin: 0;
            padding: 0;
            text-decoration: none;
            vertical-align: top;
            }

            /* content editable */
            *[contenteditable] {
            border-radius: 0.25em;
            min-width: 1em;
            outline: 0;
            }

            *[contenteditable] {
            cursor: pointer;
            }

            *[contenteditable]:hover, *[contenteditable]:focus, td:hover *[contenteditable],
            td:focus *[contenteditable], img.hover {
            background: #DEF;
            box-shadow: 0 0 1em 0.5em #DEF;
            }

            span[contenteditable] {
            display: inline-block;
            }

            /* heading */
            h1 {
            font: bold 36px sans-serif;
            letter-spacing: 0.1em;
            text-align: center;
            text-transform: uppercase;
            }

            /* page */
            html {
            font: 16px/1 'Open Sans', sans-serif;
            overflow: auto;
            padding: 0.5in;
            }

            html {
            background: #999;
            cursor: default;
            }

            body {
            box-sizing: border-box;
            height: 11.69in;
            margin: 0 auto;
            overflow: hidden;
            padding: 0.5in;
            width: 8.27in;
            }

            body {
            background: #FFF;
            border-radius: 1px;
            box-shadow: 0 0 1in -0.25in rgba(0, 0, 0, 0.5);
            }

            /* header */
            header {
            margin: 0 0 1em;
            padding-bottom:5px;
            border-bottom: solid 2px #999;
            }

            header:after {
            clear: both;
            content: "";
            display: table;
            }

            header .header-logo {
            float:left;
            width:96%;
            text-align:center;
            }

            /* article */
            article, article address, table.meta, table.inventory {
            margin: 0 0 2em;
            }

            article:after {
            clear: both;
            content: "";
            display: table;
            }

            article  {
            font-size:12px;	
            line-height: 15px;

            }

            article h1 {
            font-size: 18px;
            text-decoration: underline;
            margin-bottom: 20px;
            }

            article p {
            padding: 10px 0 0 0;
            }

            article table {
            border: 0;
            }

            article table td {
            padding: 5px;
            }

            ol {
            margin-bottom: 10px;
            }

            ol li {
            padding: 5px 10px;
            margin-left: 20px;
            }


            footer {
            position: relative;
            bottom: 0;
            font-size: 10px;
            border-top: solid 3px #999;
            }

            /* javascript */
            .add, .cut {
            border-width: 1px;
            display: block;
            font-size: .8rem;
            padding: 0.25em 0.5em;
            float: left;
            text-align: center;
            width: 0.6em;
            }

            .add, .cut {
            background: #9AF;
            box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
            background-image: -moz-linear-gradient(#00ADEE 5%, #0078A5 100%);
            background-image: -webkit-linear-gradient(#00ADEE 5%, #0078A5 100%);
            border-radius: 0.5em;
            border-color: #0076A3;
            color: #FFF;
            cursor: pointer;
            font-weight: bold;
            text-shadow: 0 -1px 2px rgba(0, 0, 0, 0.333);
            }

            .add {
            margin: -2.5em 0 0;
            }

            .add:hover {
            background: #00ADEE;
            }

            .cut {
            opacity: 0;
            position: absolute;
            top: 0;
            left: -1.5em;
            }

            .cut {
            -webkit-transition: opacity 100ms ease-in;
            }

            tr:hover .cut {
            opacity: 1;
            }

            .control {
            position:absolute;
            top: 10px;
            left: 2.3in;
            }

            .control a {
            padding: 3px 10px;
            background: #ccc;
            font-size: 12px;
            font-weight: bold;
            margin: 5px;
            }

            .control a:hover {
            background: #fff;
            }



            @media print {
            * {
            -webkit-print-color-adjust: exact;
            }
            html {
            background: none;
            padding: 0;
            }
            body {
            box-shadow: none;
            margin: 0;
            }
            span:empty {
            display: none;
            }
            .add, .cut {
            display: none;
            }
            .control {
            display:none;
            }
            }

            @page {
            margin: 0;
            }
        </style>
    </head>
    <body>
        <div class="control"><a href="#" onclick="PrintWebPage()">Print</a><g:link controller="hr">Close</g:link></div>
            <header>
                <div class="header-logo">
                    <img alt="" src="https://s3-ap-southeast-1.amazonaws.com/static.mitragardamandiri/${userInstance?.company?.compLogo}" />
            </div>
        </header>
        <article>
            <h1>SURAT PERNYATAAN SIAP TUGAS</h1>
            <p>Dengan hormat,</p>
            <p>Saya yang bertanda tangan di bawah ini :</p>
            <table class="meta">
                <tr>
                    <td width="25%">Nama</td>
                    <td width="75%">:&nbsp;${preStateTaskInstance?.person?.fullname}</td>
                </tr>
                <tr>
                    <td>Tempat/Tgl Lahir</td>
                    <td>${preStateTaskInstance?.person?.pob}/<g:formatDate date="${preStateTaskInstance?.person?.dob}" format="dd-MM-yy"/></td>
                </tr>
                <tr>
                    <td>Alamat</td>
                    <td>${preStateTaskInstance?.person?.currentAddr}</td>
                </tr>
                <tr>
                    <td>HP</td>
                    <td>${preStateTaskInstance?.person?.hp}</td>
                </tr>
            </table>

            <p>Menyatakan siap untuk bergabung dan bekerja di ${userInstance?.company?.compName}, dan untuk itu maka saya :</p>
            <ol>
                <li>Siap untuk ditempatkan disemua unit kerja ${userInstance?.company?.compName}.</li>
                <li>Siap untuk mengikuti peraturan ${userInstance?.company?.compName}.</li>
                <li>Siap untuk melaksanakan tugas dari pimpinan.</li>
                <li>Siap membantu Kepolisian dalam rangka Pengamanan Swakarsa.</li>
                <li>Bertanggung jawab pada tugas dan dapat dipercaya.</li>
                <li>Tidak menyebar gossip, berbau SARA atau hal yang merugikan ${userInstance?.company?.compName}.</li>
                <li>Tidak menjadikan ${userInstance?.company?.compName} sebagai ajang politik.</li>
                <li>Tidak terlibat dalam penyalahgunaan NARKOBA atau zat adiktif lainnya.</li>
                <li>Tidak akan terlibat dalam tindakan kriminal atau tindakan yang bertentangan dengan hukum yang berlaku di Negara Indonesia.</li>
                <li>Tidak melakukan sogok/suap untuk dapat bergabung dengan ${userInstance?.company?.compName}</li>
                <li>Tidak menerima sogok/suap dalam bentuk apapun dari Pihak Pertama, perusahaan pengguna jasa maupun dari pihak luar yang dapat merugikan dan/atau mencemarkan nama baik Pihak Pertama maupun perusahaan pengguna jasa.</li>
                <li>Besar gaji ditentukan oleh kontrak yang disepakati dengan Perusahaan pengguna jasa.</li>

            </ol>
            <p>Apabila dikemudian hari saya melanggar hal-hal tersebut diatas, maka saya siap untuk diberhentikan dari ${userInstance?.company?.compName} tanpa tuntutan apapun.</p>
            <p>Demikian Surat Pernyataan Siap Tugas ini saya buat dengan sebenar-benarnya, dalam keadaan sehat, sadar sepenuhnya dan tanpa paksaan dari pihak manapun.</p>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <table style="width:200px;float:right">
                <tr>
                    <td width="20%">Tangerang,<g:formatDate date="${new Date()}" format="dd MMMMM yyyy" /></td>
                    <td width="80%"></td>
                </tr>
                <tr><td style="text-align:center" colspan="2">Hormat saya,</td></tr>
                <tr><td>&nbsp;</td><td>&nbsp;</td></tr>
                <tr><td>&nbsp;</td><td>&nbsp;</td></tr>
                <tr><td style="text-align:center;border-bottom:1px solid #999" colspan="2">${preStateTaskInstance?.person?.fullname}</td></tr>
                <tr><td style="text-align:center;" colspan="2">NIP: ${preStateTaskInstance?.person?.pid}</td></tr>
            </table>
        </article>
        <footer>
            <p style="text-align:center"><strong>${userInstance?.company?.compName}</strong><br>
                ${userInstance?.company?.compAddress}, ${userInstance?.company?.compCity}, Indonesia<br>
                Phone : ${userInstance?.company?.compPhone}<br>
                Email : ${userInstance?.company?.compEmail}<br>
                Website : ${userInstance?.company?.compWebsite}</p>
        </footer>
        <script>
            function PrintWebPage() {
            window.print();
            }
        </script>
    </body>
</html>