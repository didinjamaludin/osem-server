

<%@ page import="com.osem.PreStateTask" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<g:set var="entityName"
	value="${message(code: 'preStateTask.label', default: 'PreStateTask')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
</head>
<body>
	<div class="col-md-2 bootstrap-admin-col-left">
		<ul class="nav navbar-collapse collapse bootstrap-admin-navbar-side">
			<li><g:link action="index">
					<i class="glyphicon glyphicon-list-alt"></i>
					<g:message code="default.list.label" args="[entityName]" />
				</g:link></li>
			<li><g:link action="create">
					<i class="glyphicon glyphicon-plus"></i>
					<g:message code="default.new.label" args="[entityName]" />
				</g:link></li>
		</ul>
	</div>

	<div class="col-md-10">
		<div class="row">
			<div class="col-lg-12">
				<div class="page-header">
					<h1>
						<g:message code="default.show.label" args="[entityName]" />
					</h1>
				</div>
				<g:if test="${flash.message}">
					<div class="page-body">
						<div class="alert alert-success" role="status">
							<a class="close" data-dismiss="alert" href="#">&times;</a>${flash.message}
						</div>
					</div>
				</g:if>
				<g:hasErrors bean="${preStateTaskInstance}">
					<div class="page-body">
						<ul class="alert alert-danger" role="alert">
							<g:eachError bean="${preStateTaskInstance}" var="error">
								<li
									<g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message
										error="${error}" /></li>
							</g:eachError>
						</ul>
					</div>
				</g:hasErrors>
			</div>
		</div>

		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default bootstrap-admin-no-table-panel">
					<div
						class="bootstrap-admin-no-table-panel-content bootstrap-admin-panel-content collapse in">
						<dl class="dl-horizontal">
							
				<g:if test="${preStateTaskInstance?.person}">
					<dt><g:message code="preStateTask.person.label" default="Person" /></dt>
					
						<dd><g:link controller="person" action="show" id="${preStateTaskInstance?.person?.id}">${preStateTaskInstance?.person?.encodeAsHTML()}</g:link></dd>
					
				</g:if>
			
				<g:if test="${preStateTaskInstance?.pstCode}">
					<dt><g:message code="preStateTask.pstCode.label" default="Pst Code" /></dt>
					
						<dd><g:fieldValue bean="${preStateTaskInstance}" field="pstCode"/></dd>
					
				</g:if>
			
				<g:if test="${preStateTaskInstance?.pstDate}">
					<dt><g:message code="preStateTask.pstDate.label" default="Pst Date" /></dt>
					
						<dd><g:formatDate date="${preStateTaskInstance?.pstDate}" /></dd>
					
				</g:if>
			
						</dl>
						<g:form url="[resource:preStateTaskInstance, action:'delete']"
							method="DELETE">
							<fieldset class="buttons">
								<g:link class="btn btn-primary" action="edit"
									resource="${preStateTaskInstance}">
									<g:message code="default.button.edit.label" default="Edit" />
								</g:link>
								<g:actionSubmit class="btn btn-default" action="delete"
									value="${message(code: 'default.button.delete.label', default: 'Delete')}"
									onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
							</fieldset>
						</g:form>
					</div>
				</div>
			</div>
		</div>

	</div>
</body>
</html>
