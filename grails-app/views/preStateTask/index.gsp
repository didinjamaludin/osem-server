

<%@ page import="com.osem.PreStateTask" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<g:set var="entityName"
	value="${message(code: 'preStateTask.label', default: 'PreStateTask')}" />
<title><g:message code="default.list.label" args="[entityName]" /></title>
</head>
<body>
	<div class="col-md-2 bootstrap-admin-col-left">
		<ul class="nav navbar-collapse collapse bootstrap-admin-navbar-side">
			<li><g:link action="create">
					<i class="glyphicon glyphicon-plus"></i>
					<g:message code="default.new.label" args="[entityName]" />
				</g:link></li>
		</ul>
	</div>

	<div class="col-md-10">
		<div class="row">
			<div class="col-lg-12">
				<div class="page-header">
					<h1>
						<g:message code="default.list.label" args="[entityName]" />
					</h1>
				</div>
				<g:if test="${flash.message}">
					<div class="page-body">
						<div class="alert alert-success" role="status">
							<a class="close" data-dismiss="alert" href="#">&times;</a>${flash.message}
						</div>
					</div>
				</g:if>
			</div>
		</div>

		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="bootstrap-admin-panel-content">
						<g:if test="${preStateTaskInstanceList}">
							<table class="table bootstrap-admin-table-with-actions">
								<thead>
									<tr>
										
						<th><g:message code="preStateTask.person.label" default="Person" /></th>
					
						<g:sortableColumn property="pstCode" title="${message(code: 'preStateTask.pstCode.label', default: 'Pst Code')}" />
					
						<g:sortableColumn property="pstDate" title="${message(code: 'preStateTask.pstDate.label', default: 'Pst Date')}" />
					
									</tr>
								</thead>
								<tbody>
									<g:each in="${preStateTaskInstanceList}" status="i"
										var="preStateTaskInstance">
										<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
											
						<td><g:link action="show" id="${preStateTaskInstance.id}">${fieldValue(bean: preStateTaskInstance, field: "person")}</g:link></td>
					
						<td>${fieldValue(bean: preStateTaskInstance, field: "pstCode")}</td>
					
						<td><g:formatDate date="${preStateTaskInstance.pstDate}" /></td>
					
										</tr>
									</g:each>
								</tbody>
							</table>
							<div class="pagination">
								<g:paginate total="${preStateTaskInstanceCount ?: 0}" />
							</div>
						</g:if>
						<g:else>
							No preStateTask found
						</g:else>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
