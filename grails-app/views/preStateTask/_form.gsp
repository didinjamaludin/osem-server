<%@ page import="com.osem.PreStateTask"%>



<div
	class="form-group fieldcontain ${hasErrors(bean: preStateTaskInstance, field: 'person', 'error')} ">
	<label class="col-lg-2 control-label" for="person"> <g:message
			code="preStateTask.person.label" default="Karyawan" />

	</label>
	<div class="col-lg-10">
		<g:if test="${params?.action?.equals('create')}">
			<g:select class="form-control" id="person" name="person.id"
				from="${com.osem.Person.list()}" optionKey="id" required=""
				value="${params?.personid}" />
		</g:if>
		<g:else>
			<g:select class="form-control" id="person" name="person.id"
				from="${com.osem.Person.list()}" optionKey="id" required=""
				value="${preStateTaskInstance?.person?.id}" />
		</g:else>
	</div>

</div>

<div
	class="form-group fieldcontain ${hasErrors(bean: preStateTaskInstance, field: 'pstCode', 'error')} ">
	<label class="col-lg-2 control-label" for="pstCode"> <g:message
			code="preStateTask.pstCode.label" default="Nomor Siap Tugas" />

	</label>
	<div class="col-lg-10">
		<g:if test="${params?.action?.equals('create')}">
			<g:textField class="form-control" name="pstCode" value="${pstNbr}" />
		</g:if>
		<g:else>
			<g:textField class="form-control" name="pstCode"
				value="${preStateTaskInstance?.pstCode}" />
		</g:else>
	</div>

</div>

<div class="form-group">
	<label class="col-lg-2 control-label" for="printPst"> Cetak
		Pernyataan </label>
	<div class="col-lg-10">
		<div class="form-control">
			<g:checkBox name="printPst" />
		</div>
	</div>
</div>

<script>
	$('input:checkbox').iCheck({
		checkboxClass : 'icheckbox_minimal-grey',
		radioClass : 'iradio_minimal-grey'
	});
</script>