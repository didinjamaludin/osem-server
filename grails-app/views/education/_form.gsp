<%@ page import="com.osem.Education" %>



<div class="form-group fieldcontain ${hasErrors(bean: educationInstance, field: 'eduLevel', 'error')} ">
	<label class="col-lg-2 control-label" for="eduLevel">
		<g:message code="education.eduLevel.label" default="Jenjang" />
		
	</label>
	<div class="col-lg-10"><g:textField class="form-control" name="eduLevel" value="${educationInstance?.eduLevel}" /></div>

</div>

<div class="form-group fieldcontain ${hasErrors(bean: educationInstance, field: 'institution', 'error')} ">
	<label class="col-lg-2 control-label" for="institution">
		<g:message code="education.institution.label" default="Sekolah/Institusi" />
		
	</label>
	<div class="col-lg-10"><g:textField class="form-control" name="institution" value="${educationInstance?.institution}" /></div>

</div>

<div class="form-group fieldcontain ${hasErrors(bean: educationInstance, field: 'yearIn', 'error')} ">
	<label class="col-lg-2 control-label" for="yearIn">
		<g:message code="education.yearIn.label" default="Tahun Masuk" />
		
	</label>
	<div class="col-lg-10"><g:field class="form-control" type="number" name="yearIn" value="${educationInstance.yearIn}" /></div>

</div>

<div class="form-group fieldcontain ${hasErrors(bean: educationInstance, field: 'yearOut', 'error')} ">
	<label class="col-lg-2 control-label" for="yearOut">
		<g:message code="education.yearOut.label" default="Tahun Keluar" />
		
	</label>
	<div class="col-lg-10"><g:field class="form-control" type="number" name="yearOut" value="${educationInstance.yearOut}" /></div>

</div>

<div class="form-group fieldcontain ${hasErrors(bean: educationInstance, field: 'faculty', 'error')} ">
	<label class="col-lg-2 control-label" for="faculty">
		<g:message code="education.faculty.label" default="Fakultas" />
		
	</label>
	<div class="col-lg-10"><g:textField class="form-control" name="faculty" value="${educationInstance?.faculty}" /></div>

</div>



<div class="form-group fieldcontain ${hasErrors(bean: educationInstance, field: 'majors', 'error')} ">
	<label class="col-lg-2 control-label" for="majors">
		<g:message code="education.majors.label" default="Jurusan" />
		
	</label>
	<div class="col-lg-10"><g:textField class="form-control" name="majors" value="${educationInstance?.majors}" /></div>

</div>

<div class="form-group fieldcontain ${hasErrors(bean: educationInstance, field: 'person', 'error')} " style="display:none;">
	<label class="col-lg-2 control-label" for="person">
		<g:message code="education.person.label" default="Person" />
		
	</label>
	<div class="col-lg-10"><g:select class="form-control" id="person" name="person.id" from="${com.osem.Person.list()}" optionKey="id" required="" value="${educationInstance?.person?.id}"/></div>

</div>

<div class="form-group fieldcontain ${hasErrors(bean: educationInstance, field: 'title', 'error')} ">
	<label class="col-lg-2 control-label" for="title">
		<g:message code="education.title.label" default="Gelar" />
		
	</label>
	<div class="col-lg-10"><g:textField class="form-control" name="title" value="${educationInstance?.title}" /></div>

</div>



