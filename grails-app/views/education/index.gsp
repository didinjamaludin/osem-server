

<%@ page import="com.osem.Education" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<g:set var="entityName"
	value="${message(code: 'education.label', default: 'Education')}" />
<title><g:message code="default.list.label" args="[entityName]" /></title>
</head>
<body>
	<div class="col-md-2 bootstrap-admin-col-left">
		<ul class="nav navbar-collapse collapse bootstrap-admin-navbar-side">
			<li><g:link action="create">
					<i class="glyphicon glyphicon-plus"></i>
					<g:message code="default.new.label" args="[entityName]" />
				</g:link></li>
		</ul>
	</div>

	<div class="col-md-10">
		<div class="row">
			<div class="col-lg-12">
				<div class="page-header">
					<h1>
						<g:message code="default.list.label" args="[entityName]" />
					</h1>
				</div>
				<g:if test="${flash.message}">
					<div class="page-body">
						<div class="alert alert-success" role="status">
							<a class="close" data-dismiss="alert" href="#">&times;</a>${flash.message}
						</div>
					</div>
				</g:if>
			</div>
		</div>

		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="bootstrap-admin-panel-content">
						<g:if test="${educationInstanceList}">
							<table class="table bootstrap-admin-table-with-actions">
								<thead>
									<tr>
										
						<g:sortableColumn property="eduLevel" title="${message(code: 'education.eduLevel.label', default: 'Edu Level')}" />
					
						<g:sortableColumn property="faculty" title="${message(code: 'education.faculty.label', default: 'Faculty')}" />
					
						<g:sortableColumn property="institution" title="${message(code: 'education.institution.label', default: 'Institution')}" />
					
						<g:sortableColumn property="majors" title="${message(code: 'education.majors.label', default: 'Majors')}" />
					
						<th><g:message code="education.person.label" default="Person" /></th>
					
						<g:sortableColumn property="title" title="${message(code: 'education.title.label', default: 'Title')}" />
					
									</tr>
								</thead>
								<tbody>
									<g:each in="${educationInstanceList}" status="i"
										var="educationInstance">
										<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
											
						<td><g:link action="show" id="${educationInstance.id}">${fieldValue(bean: educationInstance, field: "eduLevel")}</g:link></td>
					
						<td>${fieldValue(bean: educationInstance, field: "faculty")}</td>
					
						<td>${fieldValue(bean: educationInstance, field: "institution")}</td>
					
						<td>${fieldValue(bean: educationInstance, field: "majors")}</td>
					
						<td>${fieldValue(bean: educationInstance, field: "person")}</td>
					
						<td>${fieldValue(bean: educationInstance, field: "title")}</td>
					
										</tr>
									</g:each>
								</tbody>
							</table>
							<div class="pagination">
								<g:paginate total="${educationInstanceCount ?: 0}" />
							</div>
						</g:if>
						<g:else>
							No education found
						</g:else>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
