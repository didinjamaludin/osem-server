

<%@ page import="com.osem.Education" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<g:set var="entityName"
	value="${message(code: 'education.label', default: 'Education')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
</head>
<body>
	<div class="col-md-2 bootstrap-admin-col-left">
		<ul class="nav navbar-collapse collapse bootstrap-admin-navbar-side">
			<li><g:link action="index">
					<i class="glyphicon glyphicon-list-alt"></i>
					<g:message code="default.list.label" args="[entityName]" />
				</g:link></li>
			<li><g:link action="create">
					<i class="glyphicon glyphicon-plus"></i>
					<g:message code="default.new.label" args="[entityName]" />
				</g:link></li>
		</ul>
	</div>

	<div class="col-md-10">
		<div class="row">
			<div class="col-lg-12">
				<div class="page-header">
					<h1>
						<g:message code="default.show.label" args="[entityName]" />
					</h1>
				</div>
				<g:if test="${flash.message}">
					<div class="page-body">
						<div class="alert alert-success" role="status">
							<a class="close" data-dismiss="alert" href="#">&times;</a>${flash.message}
						</div>
					</div>
				</g:if>
				<g:hasErrors bean="${educationInstance}">
					<div class="page-body">
						<ul class="alert alert-danger" role="alert">
							<g:eachError bean="${educationInstance}" var="error">
								<li
									<g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message
										error="${error}" /></li>
							</g:eachError>
						</ul>
					</div>
				</g:hasErrors>
			</div>
		</div>

		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default bootstrap-admin-no-table-panel">
					<div
						class="bootstrap-admin-no-table-panel-content bootstrap-admin-panel-content collapse in">
						<dl class="dl-horizontal">
							
				<g:if test="${educationInstance?.eduLevel}">
					<dt><g:message code="education.eduLevel.label" default="Edu Level" /></dt>
					
						<dd><g:fieldValue bean="${educationInstance}" field="eduLevel"/></dd>
					
				</g:if>
			
				<g:if test="${educationInstance?.faculty}">
					<dt><g:message code="education.faculty.label" default="Faculty" /></dt>
					
						<dd><g:fieldValue bean="${educationInstance}" field="faculty"/></dd>
					
				</g:if>
			
				<g:if test="${educationInstance?.institution}">
					<dt><g:message code="education.institution.label" default="Institution" /></dt>
					
						<dd><g:fieldValue bean="${educationInstance}" field="institution"/></dd>
					
				</g:if>
			
				<g:if test="${educationInstance?.majors}">
					<dt><g:message code="education.majors.label" default="Majors" /></dt>
					
						<dd><g:fieldValue bean="${educationInstance}" field="majors"/></dd>
					
				</g:if>
			
				<g:if test="${educationInstance?.person}">
					<dt><g:message code="education.person.label" default="Person" /></dt>
					
						<dd><g:link controller="person" action="show" id="${educationInstance?.person?.id}">${educationInstance?.person?.encodeAsHTML()}</g:link></dd>
					
				</g:if>
			
				<g:if test="${educationInstance?.title}">
					<dt><g:message code="education.title.label" default="Title" /></dt>
					
						<dd><g:fieldValue bean="${educationInstance}" field="title"/></dd>
					
				</g:if>
			
				<g:if test="${educationInstance?.yearIn}">
					<dt><g:message code="education.yearIn.label" default="Year In" /></dt>
					
						<dd><g:fieldValue bean="${educationInstance}" field="yearIn"/></dd>
					
				</g:if>
			
				<g:if test="${educationInstance?.yearOut}">
					<dt><g:message code="education.yearOut.label" default="Year Out" /></dt>
					
						<dd><g:fieldValue bean="${educationInstance}" field="yearOut"/></dd>
					
				</g:if>
			
						</dl>
						<g:form url="[resource:educationInstance, action:'delete']"
							method="DELETE">
							<fieldset class="buttons">
								<g:link class="btn btn-primary" action="edit"
									resource="${educationInstance}">
									<g:message code="default.button.edit.label" default="Edit" />
								</g:link>
								<g:actionSubmit class="btn btn-default" action="delete"
									value="${message(code: 'default.button.delete.label', default: 'Delete')}"
									onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
							</fieldset>
						</g:form>
					</div>
				</div>
			</div>
		</div>

	</div>
</body>
</html>
