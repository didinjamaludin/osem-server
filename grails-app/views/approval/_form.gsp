<%@ page import="com.osem.Approval"%>

<div
	class="form-group fieldcontain ${hasErrors(bean: approvalInstance, field: 'person', 'error')} ">
	<label class="col-lg-2 control-label" for="person"> <g:message
			code="approval.person.label" default="Karyawan" />

	</label>
	<div class="col-lg-10">
		<g:if test="${params?.action?.equals('create')}">
			<g:select class="form-control" id="person" name="person.id"
				from="${com.osem.Person.list()}" optionKey="id" required=""
				value="${params?.personid}" />
		</g:if>
		<g:else>
			<g:select class="form-control" id="person" name="person.id"
				from="${com.osem.Person.list()}" optionKey="id" required=""
				value="${approvalInstance?.person?.id}" />
		</g:else>
	</div>

</div>

<div
	class="form-group fieldcontain ${hasErrors(bean: approvalInstance, field: 'appCode', 'error')} ">
	<label class="col-lg-2 control-label" for="appCode"> <g:message
			code="approval.appCode.label" default="Nomor Approval" />

	</label>
	<div class="col-lg-10">
		<g:if test="${params?.action?.equals('create')}">
			<g:textField class="form-control" name="appCode" value="${apprNbr}" />
		</g:if>
		<g:else>
			<g:textField class="form-control" name="appCode"
				value="${approvalInstance?.appCode}" />
		</g:else>
	</div>
</div>

<div
	class="form-group fieldcontain ${hasErrors(bean: approvalInstance, field: 'testScore', 'error')} ">
	<label class="col-lg-2 control-label" for="testScore"> <g:message
			code="approval.testScore.label" default="Nilai Tes" />

	</label>
	<div class="col-lg-10">
		<g:field type="number" class="form-control" name="testScore"
			value="${approvalInstance?.testScore}" />
	</div>

</div>

<div
	class="form-group fieldcontain ${hasErrors(bean: approvalInstance, field: 'appNotes', 'error')} ">
	<label class="col-lg-2 control-label" for="appNotes"> <g:message
			code="approval.appNotes.label" default="Catatan" />

	</label>
	<div class="col-lg-10">
		<g:textField class="form-control" name="appNotes"
			value="${approvalInstance?.appNotes}" />
	</div>

</div>