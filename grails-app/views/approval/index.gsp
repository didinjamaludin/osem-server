

<%@ page import="com.osem.Approval" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<g:set var="entityName"
	value="${message(code: 'approval.label', default: 'Approval')}" />
<title><g:message code="default.list.label" args="[entityName]" /></title>
</head>
<body>
	<div class="col-md-2 bootstrap-admin-col-left">
		<ul class="nav navbar-collapse collapse bootstrap-admin-navbar-side">
			<li><g:link action="create">
					<i class="glyphicon glyphicon-plus"></i>
					<g:message code="default.new.label" args="[entityName]" />
				</g:link></li>
		</ul>
	</div>

	<div class="col-md-10">
		<div class="row">
			<div class="col-lg-12">
				<div class="page-header">
					<h1>
						<g:message code="default.list.label" args="[entityName]" />
					</h1>
				</div>
				<g:if test="${flash.message}">
					<div class="page-body">
						<div class="alert alert-success" role="status">
							<a class="close" data-dismiss="alert" href="#">&times;</a>${flash.message}
						</div>
					</div>
				</g:if>
			</div>
		</div>

		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="bootstrap-admin-panel-content">
						<g:if test="${approvalInstanceList}">
							<table class="table bootstrap-admin-table-with-actions">
								<thead>
									<tr>
										
						<g:sortableColumn property="appCode" title="${message(code: 'approval.appCode.label', default: 'App Code')}" />
					
						<g:sortableColumn property="appDate" title="${message(code: 'approval.appDate.label', default: 'App Date')}" />
					
						<g:sortableColumn property="appNotes" title="${message(code: 'approval.appNotes.label', default: 'App Notes')}" />
					
						<th><g:message code="approval.person.label" default="Person" /></th>
					
						<g:sortableColumn property="testScore" title="${message(code: 'approval.testScore.label', default: 'Test Score')}" />
					
									</tr>
								</thead>
								<tbody>
									<g:each in="${approvalInstanceList}" status="i"
										var="approvalInstance">
										<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
											
						<td><g:link action="show" id="${approvalInstance.id}">${fieldValue(bean: approvalInstance, field: "appCode")}</g:link></td>
					
						<td><g:formatDate date="${approvalInstance.appDate}" /></td>
					
						<td>${fieldValue(bean: approvalInstance, field: "appNotes")}</td>
					
						<td>${fieldValue(bean: approvalInstance, field: "person")}</td>
					
						<td>${fieldValue(bean: approvalInstance, field: "testScore")}</td>
					
										</tr>
									</g:each>
								</tbody>
							</table>
							<div class="pagination">
								<g:paginate total="${approvalInstanceCount ?: 0}" />
							</div>
						</g:if>
						<g:else>
							No approval found
						</g:else>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
