<%@ page import="com.osem.Resignation" %>



<div class="form-group fieldcontain ${hasErrors(bean: resignationInstance, field: 'createDate', 'error')} ">
	<label class="col-lg-2 control-label" for="createDate">
		<g:message code="resignation.createDate.label" default="Create Date" />
		
	</label>
	<div class="col-lg-10"><g:datePicker class="form-control" name="createDate" precision="day" value="${resignationInstance?.createDate}" /></div>

</div>

<div class="form-group fieldcontain ${hasErrors(bean: resignationInstance, field: 'person', 'error')} ">
	<label class="col-lg-2 control-label" for="person">
		<g:message code="resignation.person.label" default="Person" />
		
	</label>
	<div class="col-lg-10"><g:select class="form-control" id="person" name="person.id" from="${com.osem.Person.list()}" optionKey="id" required="" value="${resignationInstance?.person?.id}"/></div>

</div>

<div class="form-group fieldcontain ${hasErrors(bean: resignationInstance, field: 'resCode', 'error')} ">
	<label class="col-lg-2 control-label" for="resCode">
		<g:message code="resignation.resCode.label" default="Res Code" />
		
	</label>
	<div class="col-lg-10"><g:textField class="form-control" name="resCode" value="${resignationInstance?.resCode}" /></div>

</div>

<div class="form-group fieldcontain ${hasErrors(bean: resignationInstance, field: 'resDate', 'error')} ">
	<label class="col-lg-2 control-label" for="resDate">
		<g:message code="resignation.resDate.label" default="Res Date" />
		
	</label>
	<div class="col-lg-10"><g:datePicker class="form-control" name="resDate" precision="day" value="${resignationInstance?.resDate}" /></div>

</div>

<div class="form-group fieldcontain ${hasErrors(bean: resignationInstance, field: 'resNotes', 'error')} ">
	<label class="col-lg-2 control-label" for="resNotes">
		<g:message code="resignation.resNotes.label" default="Res Notes" />
		
	</label>
	<div class="col-lg-10"><g:textField class="form-control" name="resNotes" value="${resignationInstance?.resNotes}" /></div>

</div>

<div class="form-group fieldcontain ${hasErrors(bean: resignationInstance, field: 'resReason', 'error')} ">
	<label class="col-lg-2 control-label" for="resReason">
		<g:message code="resignation.resReason.label" default="Res Reason" />
		
	</label>
	<div class="col-lg-10"><g:textField class="form-control" name="resReason" value="${resignationInstance?.resReason}" /></div>

</div>

