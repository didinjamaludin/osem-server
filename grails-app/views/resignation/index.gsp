

<%@ page import="com.osem.Resignation" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<g:set var="entityName"
	value="${message(code: 'resignation.label', default: 'Resignation')}" />
<title><g:message code="default.list.label" args="[entityName]" /></title>
</head>
<body>
	<div class="col-md-2 bootstrap-admin-col-left">
		<ul class="nav navbar-collapse collapse bootstrap-admin-navbar-side">
			<li><g:link action="create">
					<i class="glyphicon glyphicon-plus"></i>
					<g:message code="default.new.label" args="[entityName]" />
				</g:link></li>
		</ul>
	</div>

	<div class="col-md-10">
		<div class="row">
			<div class="col-lg-12">
				<div class="page-header">
					<h1>
						<g:message code="default.list.label" args="[entityName]" />
					</h1>
				</div>
				<g:if test="${flash.message}">
					<div class="page-body">
						<div class="alert alert-success" role="status">
							<a class="close" data-dismiss="alert" href="#">&times;</a>${flash.message}
						</div>
					</div>
				</g:if>
			</div>
		</div>

		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="bootstrap-admin-panel-content">
						<g:if test="${resignationInstanceList}">
							<table class="table bootstrap-admin-table-with-actions">
								<thead>
									<tr>
										
						<g:sortableColumn property="createDate" title="${message(code: 'resignation.createDate.label', default: 'Create Date')}" />
					
						<th><g:message code="resignation.person.label" default="Person" /></th>
					
						<g:sortableColumn property="resCode" title="${message(code: 'resignation.resCode.label', default: 'Res Code')}" />
					
						<g:sortableColumn property="resDate" title="${message(code: 'resignation.resDate.label', default: 'Res Date')}" />
					
						<g:sortableColumn property="resNotes" title="${message(code: 'resignation.resNotes.label', default: 'Res Notes')}" />
					
						<g:sortableColumn property="resReason" title="${message(code: 'resignation.resReason.label', default: 'Res Reason')}" />
					
									</tr>
								</thead>
								<tbody>
									<g:each in="${resignationInstanceList}" status="i"
										var="resignationInstance">
										<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
											
						<td><g:link action="show" id="${resignationInstance.id}">${fieldValue(bean: resignationInstance, field: "createDate")}</g:link></td>
					
						<td>${fieldValue(bean: resignationInstance, field: "person")}</td>
					
						<td>${fieldValue(bean: resignationInstance, field: "resCode")}</td>
					
						<td><g:formatDate date="${resignationInstance.resDate}" /></td>
					
						<td>${fieldValue(bean: resignationInstance, field: "resNotes")}</td>
					
						<td>${fieldValue(bean: resignationInstance, field: "resReason")}</td>
					
										</tr>
									</g:each>
								</tbody>
							</table>
							<div class="pagination">
								<g:paginate total="${resignationInstanceCount ?: 0}" />
							</div>
						</g:if>
						<g:else>
							No resignation found
						</g:else>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
