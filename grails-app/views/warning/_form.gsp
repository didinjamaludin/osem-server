<%@ page import="com.osem.Warning" %>



<div class="form-group fieldcontain ${hasErrors(bean: warningInstance, field: 'createDate', 'error')} ">
	<label class="col-lg-2 control-label" for="createDate">
		<g:message code="warning.createDate.label" default="Create Date" />
		
	</label>
	<div class="col-lg-10"><g:datePicker class="form-control" name="createDate" precision="day" value="${warningInstance?.createDate}" /></div>

</div>

<div class="form-group fieldcontain ${hasErrors(bean: warningInstance, field: 'investPosition', 'error')} ">
	<label class="col-lg-2 control-label" for="investPosition">
		<g:message code="warning.investPosition.label" default="Invest Position" />
		
	</label>
	<div class="col-lg-10"><g:textField class="form-control" name="investPosition" value="${warningInstance?.investPosition}" /></div>

</div>

<div class="form-group fieldcontain ${hasErrors(bean: warningInstance, field: 'investigator', 'error')} ">
	<label class="col-lg-2 control-label" for="investigator">
		<g:message code="warning.investigator.label" default="Investigator" />
		
	</label>
	<div class="col-lg-10"><g:textField class="form-control" name="investigator" value="${warningInstance?.investigator}" /></div>

</div>

<div class="form-group fieldcontain ${hasErrors(bean: warningInstance, field: 'person', 'error')} ">
	<label class="col-lg-2 control-label" for="person">
		<g:message code="warning.person.label" default="Person" />
		
	</label>
	<div class="col-lg-10"><g:select class="form-control" id="person" name="person.id" from="${com.osem.Person.list()}" optionKey="id" required="" value="${warningInstance?.person?.id}"/></div>

</div>

<div class="form-group fieldcontain ${hasErrors(bean: warningInstance, field: 'warnCode', 'error')} ">
	<label class="col-lg-2 control-label" for="warnCode">
		<g:message code="warning.warnCode.label" default="Warn Code" />
		
	</label>
	<div class="col-lg-10"><g:textField class="form-control" name="warnCode" value="${warningInstance?.warnCode}" /></div>

</div>

<div class="form-group fieldcontain ${hasErrors(bean: warningInstance, field: 'warnEnd', 'error')} ">
	<label class="col-lg-2 control-label" for="warnEnd">
		<g:message code="warning.warnEnd.label" default="Warn End" />
		
	</label>
	<div class="col-lg-10"><g:datePicker class="form-control" name="warnEnd" precision="day" value="${warningInstance?.warnEnd}" /></div>

</div>

<div class="form-group fieldcontain ${hasErrors(bean: warningInstance, field: 'warnNotes', 'error')} ">
	<label class="col-lg-2 control-label" for="warnNotes">
		<g:message code="warning.warnNotes.label" default="Warn Notes" />
		
	</label>
	<div class="col-lg-10"><g:textField class="form-control" name="warnNotes" value="${warningInstance?.warnNotes}" /></div>

</div>

<div class="form-group fieldcontain ${hasErrors(bean: warningInstance, field: 'warnStart', 'error')} ">
	<label class="col-lg-2 control-label" for="warnStart">
		<g:message code="warning.warnStart.label" default="Warn Start" />
		
	</label>
	<div class="col-lg-10"><g:datePicker class="form-control" name="warnStart" precision="day" value="${warningInstance?.warnStart}" /></div>

</div>

<div class="form-group fieldcontain ${hasErrors(bean: warningInstance, field: 'warnType', 'error')} ">
	<label class="col-lg-2 control-label" for="warnType">
		<g:message code="warning.warnType.label" default="Warn Type" />
		
	</label>
	<div class="col-lg-10"><g:select class="form-control" id="warnType" name="warnType.id" from="${com.osem.WarningType.list()}" optionKey="id" required="" value="${warningInstance?.warnType?.id}"/></div>

</div>

