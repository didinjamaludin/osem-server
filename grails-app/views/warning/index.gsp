

<%@ page import="com.osem.Warning" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<g:set var="entityName"
	value="${message(code: 'warning.label', default: 'Warning')}" />
<title><g:message code="default.list.label" args="[entityName]" /></title>
</head>
<body>
	<div class="col-md-2 bootstrap-admin-col-left">
		<ul class="nav navbar-collapse collapse bootstrap-admin-navbar-side">
			<li><g:link action="create">
					<i class="glyphicon glyphicon-plus"></i>
					<g:message code="default.new.label" args="[entityName]" />
				</g:link></li>
		</ul>
	</div>

	<div class="col-md-10">
		<div class="row">
			<div class="col-lg-12">
				<div class="page-header">
					<h1>
						<g:message code="default.list.label" args="[entityName]" />
					</h1>
				</div>
				<g:if test="${flash.message}">
					<div class="page-body">
						<div class="alert alert-success" role="status">
							<a class="close" data-dismiss="alert" href="#">&times;</a>${flash.message}
						</div>
					</div>
				</g:if>
			</div>
		</div>

		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="bootstrap-admin-panel-content">
						<g:if test="${warningInstanceList}">
							<table class="table bootstrap-admin-table-with-actions">
								<thead>
									<tr>
										
						<g:sortableColumn property="createDate" title="${message(code: 'warning.createDate.label', default: 'Create Date')}" />
					
						<g:sortableColumn property="investPosition" title="${message(code: 'warning.investPosition.label', default: 'Invest Position')}" />
					
						<g:sortableColumn property="investigator" title="${message(code: 'warning.investigator.label', default: 'Investigator')}" />
					
						<th><g:message code="warning.person.label" default="Person" /></th>
					
						<g:sortableColumn property="warnCode" title="${message(code: 'warning.warnCode.label', default: 'Warn Code')}" />
					
						<g:sortableColumn property="warnEnd" title="${message(code: 'warning.warnEnd.label', default: 'Warn End')}" />
					
									</tr>
								</thead>
								<tbody>
									<g:each in="${warningInstanceList}" status="i"
										var="warningInstance">
										<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
											
						<td><g:link action="show" id="${warningInstance.id}">${fieldValue(bean: warningInstance, field: "createDate")}</g:link></td>
					
						<td>${fieldValue(bean: warningInstance, field: "investPosition")}</td>
					
						<td>${fieldValue(bean: warningInstance, field: "investigator")}</td>
					
						<td>${fieldValue(bean: warningInstance, field: "person")}</td>
					
						<td>${fieldValue(bean: warningInstance, field: "warnCode")}</td>
					
						<td><g:formatDate date="${warningInstance.warnEnd}" /></td>
					
										</tr>
									</g:each>
								</tbody>
							</table>
							<div class="pagination">
								<g:paginate total="${warningInstanceCount ?: 0}" />
							</div>
						</g:if>
						<g:else>
							No warning found
						</g:else>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
