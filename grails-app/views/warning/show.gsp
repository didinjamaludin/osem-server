

<%@ page import="com.osem.Warning" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<g:set var="entityName"
	value="${message(code: 'warning.label', default: 'Warning')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
</head>
<body>
	<div class="col-md-2 bootstrap-admin-col-left">
		<ul class="nav navbar-collapse collapse bootstrap-admin-navbar-side">
			<li><g:link action="index">
					<i class="glyphicon glyphicon-list-alt"></i>
					<g:message code="default.list.label" args="[entityName]" />
				</g:link></li>
			<li><g:link action="create">
					<i class="glyphicon glyphicon-plus"></i>
					<g:message code="default.new.label" args="[entityName]" />
				</g:link></li>
		</ul>
	</div>

	<div class="col-md-10">
		<div class="row">
			<div class="col-lg-12">
				<div class="page-header">
					<h1>
						<g:message code="default.show.label" args="[entityName]" />
					</h1>
				</div>
				<g:if test="${flash.message}">
					<div class="page-body">
						<div class="alert alert-success" role="status">
							<a class="close" data-dismiss="alert" href="#">&times;</a>${flash.message}
						</div>
					</div>
				</g:if>
				<g:hasErrors bean="${warningInstance}">
					<div class="page-body">
						<ul class="alert alert-danger" role="alert">
							<g:eachError bean="${warningInstance}" var="error">
								<li
									<g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message
										error="${error}" /></li>
							</g:eachError>
						</ul>
					</div>
				</g:hasErrors>
			</div>
		</div>

		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default bootstrap-admin-no-table-panel">
					<div
						class="bootstrap-admin-no-table-panel-content bootstrap-admin-panel-content collapse in">
						<dl class="dl-horizontal">
							
				<g:if test="${warningInstance?.createDate}">
					<dt><g:message code="warning.createDate.label" default="Create Date" /></dt>
					
						<dd><g:formatDate date="${warningInstance?.createDate}" /></dd>
					
				</g:if>
			
				<g:if test="${warningInstance?.investPosition}">
					<dt><g:message code="warning.investPosition.label" default="Invest Position" /></dt>
					
						<dd><g:fieldValue bean="${warningInstance}" field="investPosition"/></dd>
					
				</g:if>
			
				<g:if test="${warningInstance?.investigator}">
					<dt><g:message code="warning.investigator.label" default="Investigator" /></dt>
					
						<dd><g:fieldValue bean="${warningInstance}" field="investigator"/></dd>
					
				</g:if>
			
				<g:if test="${warningInstance?.person}">
					<dt><g:message code="warning.person.label" default="Person" /></dt>
					
						<dd><g:link controller="person" action="show" id="${warningInstance?.person?.id}">${warningInstance?.person?.encodeAsHTML()}</g:link></dd>
					
				</g:if>
			
				<g:if test="${warningInstance?.warnCode}">
					<dt><g:message code="warning.warnCode.label" default="Warn Code" /></dt>
					
						<dd><g:fieldValue bean="${warningInstance}" field="warnCode"/></dd>
					
				</g:if>
			
				<g:if test="${warningInstance?.warnEnd}">
					<dt><g:message code="warning.warnEnd.label" default="Warn End" /></dt>
					
						<dd><g:formatDate date="${warningInstance?.warnEnd}" /></dd>
					
				</g:if>
			
				<g:if test="${warningInstance?.warnNotes}">
					<dt><g:message code="warning.warnNotes.label" default="Warn Notes" /></dt>
					
						<dd><g:fieldValue bean="${warningInstance}" field="warnNotes"/></dd>
					
				</g:if>
			
				<g:if test="${warningInstance?.warnStart}">
					<dt><g:message code="warning.warnStart.label" default="Warn Start" /></dt>
					
						<dd><g:formatDate date="${warningInstance?.warnStart}" /></dd>
					
				</g:if>
			
				<g:if test="${warningInstance?.warnType}">
					<dt><g:message code="warning.warnType.label" default="Warn Type" /></dt>
					
						<dd><g:link controller="warningType" action="show" id="${warningInstance?.warnType?.id}">${warningInstance?.warnType?.encodeAsHTML()}</g:link></dd>
					
				</g:if>
			
						</dl>
						<g:form url="[resource:warningInstance, action:'delete']"
							method="DELETE">
							<fieldset class="buttons">
								<g:link class="btn btn-primary" action="edit"
									resource="${warningInstance}">
									<g:message code="default.button.edit.label" default="Edit" />
								</g:link>
								<g:actionSubmit class="btn btn-default" action="delete"
									value="${message(code: 'default.button.delete.label', default: 'Delete')}"
									onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
							</fieldset>
						</g:form>
					</div>
				</div>
			</div>
		</div>

	</div>
</body>
</html>
