

<%@ page import="com.osem.Prohibition" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<g:set var="entityName"
	value="${message(code: 'prohibition.label', default: 'Prohibition')}" />
<title><g:message code="default.list.label" args="[entityName]" /></title>
</head>
<body>
	<div class="col-md-2 bootstrap-admin-col-left">
		<ul class="nav navbar-collapse collapse bootstrap-admin-navbar-side">
			<li><g:link action="create">
					<i class="glyphicon glyphicon-plus"></i>
					<g:message code="default.new.label" args="[entityName]" />
				</g:link></li>
		</ul>
	</div>

	<div class="col-md-10">
		<div class="row">
			<div class="col-lg-12">
				<div class="page-header">
					<h1>
						<g:message code="default.list.label" args="[entityName]" />
					</h1>
				</div>
				<g:if test="${flash.message}">
					<div class="page-body">
						<div class="alert alert-success" role="status">
							<a class="close" data-dismiss="alert" href="#">&times;</a>${flash.message}
						</div>
					</div>
				</g:if>
			</div>
		</div>

		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="bootstrap-admin-panel-content">
						<g:if test="${prohibitionInstanceList}">
							<table class="table bootstrap-admin-table-with-actions">
								<thead>
									<tr>
										
						<g:sortableColumn property="createDate" title="${message(code: 'prohibition.createDate.label', default: 'Create Date')}" />
					
						<th><g:message code="prohibition.person.label" default="Person" /></th>
					
						<g:sortableColumn property="probCode" title="${message(code: 'prohibition.probCode.label', default: 'Prob Code')}" />
					
						<g:sortableColumn property="probEnd" title="${message(code: 'prohibition.probEnd.label', default: 'Prob End')}" />
					
						<g:sortableColumn property="probLength" title="${message(code: 'prohibition.probLength.label', default: 'Prob Length')}" />
					
						<g:sortableColumn property="probNotes" title="${message(code: 'prohibition.probNotes.label', default: 'Prob Notes')}" />
					
									</tr>
								</thead>
								<tbody>
									<g:each in="${prohibitionInstanceList}" status="i"
										var="prohibitionInstance">
										<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
											
						<td><g:link action="show" id="${prohibitionInstance.id}">${fieldValue(bean: prohibitionInstance, field: "createDate")}</g:link></td>
					
						<td>${fieldValue(bean: prohibitionInstance, field: "person")}</td>
					
						<td>${fieldValue(bean: prohibitionInstance, field: "probCode")}</td>
					
						<td><g:formatDate date="${prohibitionInstance.probEnd}" /></td>
					
						<td>${fieldValue(bean: prohibitionInstance, field: "probLength")}</td>
					
						<td>${fieldValue(bean: prohibitionInstance, field: "probNotes")}</td>
					
										</tr>
									</g:each>
								</tbody>
							</table>
							<div class="pagination">
								<g:paginate total="${prohibitionInstanceCount ?: 0}" />
							</div>
						</g:if>
						<g:else>
							No prohibition found
						</g:else>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
