<%@ page import="com.osem.Prohibition"%>


<div
	class="form-group fieldcontain ${hasErrors(bean: prohibitionInstance, field: 'person', 'error')} ">
	<label class="col-lg-2 control-label" for="person"> <g:message
			code="prohibition.person.label" default="Person" />

	</label>
	<div class="col-lg-10">
		<g:if test="${params?.action?.equals('create')}">
			<g:select class="form-control" id="person" name="person.id"
				from="${com.osem.Person.list()}" optionKey="id" required=""
				value="${params?.personid}" />
		</g:if>
		<g:else>
			<g:select class="form-control" id="person" name="person.id"
				from="${com.osem.Person.list()}" optionKey="id" required=""
				value="${prohibitionInstance?.person?.id}" />
		</g:else>
	</div>

</div>

<div
	class="form-group fieldcontain ${hasErrors(bean: prohibitionInstance, field: 'probCode', 'error')} ">
	<label class="col-lg-2 control-label" for="probCode"> <g:message
			code="prohibition.probCode.label" default="Nomor" />

	</label>
	<div class="col-lg-10">
		<g:if test="${params?.action?.equals('create')}">
			<g:textField class="form-control" name="probCode" value="${probNbr}" />
		</g:if>
		<g:else>
			<g:textField class="form-control" name="probCode"
				value="${prohibitionInstance?.probCode}" />
		</g:else>
	</div>

</div>

<div
	class="form-group fieldcontain ${hasErrors(bean: prohibitionInstance, field: 'probStart', 'error')} ">
	<label class="col-lg-2 control-label" for="probStart"> <g:message
			code="prohibition.probStart.label" default="Tgl Mulai" />

	</label>
	<div class="col-lg-10">
		<div class="form-control">
			<g:datePicker name="probStart" precision="day"
				value="${prohibitionInstance?.probStart}" />
		</div>
	</div>

</div>

<div
	class="form-group fieldcontain ${hasErrors(bean: prohibitionInstance, field: 'probEnd', 'error')} ">
	<label class="col-lg-2 control-label" for="probEnd"> <g:message
			code="prohibition.probEnd.label" default="Tgl Berakhir" />

	</label>
	<div class="col-lg-10">
		<div class="form-control">
			<g:datePicker name="probEnd" precision="day"
				value="${prohibitionInstance?.probEnd}" />
		</div>
	</div>

</div>

<div
	class="form-group fieldcontain ${hasErrors(bean: prohibitionInstance, field: 'probLength', 'error')} ">
	<label class="col-lg-2 control-label" for="probLength"> <g:message
			code="prohibition.probLength.label" default="Lama (Bulan)" />

	</label>
	<div class="col-lg-10">
		<g:field class="form-control" type="number" name="probLength"
			value="${prohibitionInstance.probLength}" />
	</div>

</div>

<div
	class="form-group fieldcontain ${hasErrors(bean: prohibitionInstance, field: 'probNotes', 'error')} ">
	<label class="col-lg-2 control-label" for="probNotes"> <g:message
			code="prohibition.probNotes.label" default="Catatan" />

	</label>
	<div class="col-lg-10">
		<g:textField class="form-control" name="probNotes"
			value="${prohibitionInstance?.probNotes}" />
	</div>

</div>

