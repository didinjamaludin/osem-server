

<%@ page import="com.osem.Prohibition" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<g:set var="entityName"
	value="${message(code: 'prohibition.label', default: 'Prohibition')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
</head>
<body>
	<div class="col-md-2 bootstrap-admin-col-left">
		<ul class="nav navbar-collapse collapse bootstrap-admin-navbar-side">
			<li><g:link action="index">
					<i class="glyphicon glyphicon-list-alt"></i>
					<g:message code="default.list.label" args="[entityName]" />
				</g:link></li>
			<li><g:link action="create">
					<i class="glyphicon glyphicon-plus"></i>
					<g:message code="default.new.label" args="[entityName]" />
				</g:link></li>
		</ul>
	</div>

	<div class="col-md-10">
		<div class="row">
			<div class="col-lg-12">
				<div class="page-header">
					<h1>
						<g:message code="default.show.label" args="[entityName]" />
					</h1>
				</div>
				<g:if test="${flash.message}">
					<div class="page-body">
						<div class="alert alert-success" role="status">
							<a class="close" data-dismiss="alert" href="#">&times;</a>${flash.message}
						</div>
					</div>
				</g:if>
				<g:hasErrors bean="${prohibitionInstance}">
					<div class="page-body">
						<ul class="alert alert-danger" role="alert">
							<g:eachError bean="${prohibitionInstance}" var="error">
								<li
									<g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message
										error="${error}" /></li>
							</g:eachError>
						</ul>
					</div>
				</g:hasErrors>
			</div>
		</div>

		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default bootstrap-admin-no-table-panel">
					<div
						class="bootstrap-admin-no-table-panel-content bootstrap-admin-panel-content collapse in">
						<dl class="dl-horizontal">
							
				<g:if test="${prohibitionInstance?.createDate}">
					<dt><g:message code="prohibition.createDate.label" default="Create Date" /></dt>
					
						<dd><g:formatDate date="${prohibitionInstance?.createDate}" /></dd>
					
				</g:if>
			
				<g:if test="${prohibitionInstance?.person}">
					<dt><g:message code="prohibition.person.label" default="Person" /></dt>
					
						<dd><g:link controller="person" action="show" id="${prohibitionInstance?.person?.id}">${prohibitionInstance?.person?.encodeAsHTML()}</g:link></dd>
					
				</g:if>
			
				<g:if test="${prohibitionInstance?.probCode}">
					<dt><g:message code="prohibition.probCode.label" default="Prob Code" /></dt>
					
						<dd><g:fieldValue bean="${prohibitionInstance}" field="probCode"/></dd>
					
				</g:if>
			
				<g:if test="${prohibitionInstance?.probEnd}">
					<dt><g:message code="prohibition.probEnd.label" default="Prob End" /></dt>
					
						<dd><g:formatDate date="${prohibitionInstance?.probEnd}" /></dd>
					
				</g:if>
			
				<g:if test="${prohibitionInstance?.probLength}">
					<dt><g:message code="prohibition.probLength.label" default="Prob Length" /></dt>
					
						<dd><g:fieldValue bean="${prohibitionInstance}" field="probLength"/></dd>
					
				</g:if>
			
				<g:if test="${prohibitionInstance?.probNotes}">
					<dt><g:message code="prohibition.probNotes.label" default="Prob Notes" /></dt>
					
						<dd><g:fieldValue bean="${prohibitionInstance}" field="probNotes"/></dd>
					
				</g:if>
			
				<g:if test="${prohibitionInstance?.probStart}">
					<dt><g:message code="prohibition.probStart.label" default="Prob Start" /></dt>
					
						<dd><g:formatDate date="${prohibitionInstance?.probStart}" /></dd>
					
				</g:if>
			
						</dl>
						<g:form url="[resource:prohibitionInstance, action:'delete']"
							method="DELETE">
							<fieldset class="buttons">
								<g:link class="btn btn-primary" action="edit"
									resource="${prohibitionInstance}">
									<g:message code="default.button.edit.label" default="Edit" />
								</g:link>
								<g:actionSubmit class="btn btn-default" action="delete"
									value="${message(code: 'default.button.delete.label', default: 'Delete')}"
									onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
							</fieldset>
						</g:form>
					</div>
				</div>
			</div>
		</div>

	</div>
</body>
</html>
