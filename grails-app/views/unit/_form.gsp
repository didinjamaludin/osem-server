<%@ page import="com.osem.Unit"%>



<div
	class="form-group fieldcontain ${hasErrors(bean: unitInstance, field: 'company', 'error')} " style="display:none">
	<label class="col-lg-2 control-label" for="company"> <g:message
			code="unit.company.label" default="Company" />

	</label>
	<div class="col-lg-10">
		<g:if test="${params?.action?.equals('create')}">
			<g:select class="form-control" id="company" name="company.id"
				from="${com.osem.Company.list()}" optionKey="id" required=""
				value="${company?.id}" />
		</g:if>
		<g:else>
			<g:select class="form-control" id="company" name="company.id"
				from="${com.osem.Company.list()}" optionKey="id" required=""
				value="${unitInstance?.company?.id}" />
		</g:else>
	</div>

</div>

<div
	class="form-group fieldcontain ${hasErrors(bean: unitInstance, field: 'unitCode', 'error')} ">
	<label class="col-lg-2 control-label" for="unitCode"> <g:message
			code="unit.unitCode.label" default="Kode Unit" />

	</label>
	<div class="col-lg-10">
		<g:textField class="form-control" name="unitCode"
			value="${unitInstance?.unitCode}" />
	</div>

</div>

<div
	class="form-group fieldcontain ${hasErrors(bean: unitInstance, field: 'unitName', 'error')} ">
	<label class="col-lg-2 control-label" for="unitName"> <g:message
			code="unit.unitName.label" default="Nama Unit" />

	</label>
	<div class="col-lg-10">
		<g:textField class="form-control" name="unitName"
			value="${unitInstance?.unitName}" />
	</div>

</div>

<div
	class="form-group fieldcontain ${hasErrors(bean: unitInstance, field: 'unitHead', 'error')} ">
	<label class="col-lg-2 control-label" for="unitHead"> <g:message
			code="unit.unitHead.label" default="Kepala Unit" />

	</label>
	<div class="col-lg-10">
		<g:textField class="form-control" name="unitHead"
			value="${unitInstance?.unitHead}" />
	</div>

</div>

