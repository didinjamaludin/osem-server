

<%@ page import="com.osem.Unit"%>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<g:set var="entityName"
	value="${message(code: 'unit.label', default: 'Unit')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
</head>
<body>
	<div class="col-md-2 bootstrap-admin-col-left">
		<ul class="nav navbar-collapse collapse bootstrap-admin-navbar-side">
			<li><g:link action="index">
					<i class="glyphicon glyphicon-list-alt"></i>
					<g:message code="default.list.label" args="[entityName]" />
				</g:link></li>
			<li><g:link action="create">
					<i class="glyphicon glyphicon-plus"></i>
					<g:message code="default.new.label" args="[entityName]" />
				</g:link></li>
		</ul>
	</div>

	<div class="col-md-10">
		<div class="row">
			<div class="col-lg-12">
				<div class="page-header">
					<h1>
						<g:message code="default.show.label" args="[entityName]" />
					</h1>
				</div>
				<g:if test="${flash.message}">
					<div class="page-body">
						<div class="alert alert-success" role="status">
							<a class="close" data-dismiss="alert" href="#">&times;</a>
							${flash.message}
						</div>
					</div>
				</g:if>
				<g:hasErrors bean="${unitInstance}">
					<div class="page-body">
						<ul class="alert alert-danger" role="alert">
							<g:eachError bean="${unitInstance}" var="error">
								<li
									<g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message
										error="${error}" /></li>
							</g:eachError>
						</ul>
					</div>
				</g:hasErrors>
			</div>
		</div>

		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default bootstrap-admin-no-table-panel">
					<div
						class="bootstrap-admin-no-table-panel-content bootstrap-admin-panel-content collapse in">
						<dl class="dl-horizontal">

							<g:if test="${unitInstance?.unitCode}">
								<dt>
									<g:message code="unit.unitCode.label" default="Kode Unit" />
								</dt>

								<dd>
									<g:fieldValue bean="${unitInstance}" field="unitCode" />
								</dd>

							</g:if>

							<g:if test="${unitInstance?.unitName}">
								<dt>
									<g:message code="unit.unitName.label" default="Nama Unit" />
								</dt>

								<dd>
									<g:fieldValue bean="${unitInstance}" field="unitName" />
								</dd>

							</g:if>

							<g:if test="${unitInstance?.unitHead}">
								<dt>
									<g:message code="unit.unitHead.label" default="Kepala Unit" />
								</dt>

								<dd>
									<g:fieldValue bean="${unitInstance}" field="unitHead" />
								</dd>

							</g:if>

							<g:if test="${unitInstance?.subUnits}">
								<dt>
									<g:message code="unit.subUnits.label" default="Sub Unit" />
								</dt>
								<g:each in="${unitInstance.subUnits}" var="s">
									<dd>
										<g:link controller="subUnit" action="show" id="${s.id}">
											${s?.encodeAsHTML()}
										</g:link>
									</dd>
								</g:each>

							</g:if>

						</dl>
						<g:form url="[resource:unitInstance, action:'delete']"
							method="DELETE">
							<fieldset class="buttons">
								<g:link class="btn btn-primary" action="edit"
									resource="${unitInstance}">
									<g:message code="default.button.edit.label" default="Edit" />
								</g:link>
								<g:actionSubmit class="btn btn-default" action="delete"
									value="${message(code: 'default.button.delete.label', default: 'Delete')}"
									onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
							</fieldset>
						</g:form>
					</div>
				</div>
			</div>
		</div>

	</div>
</body>
</html>
