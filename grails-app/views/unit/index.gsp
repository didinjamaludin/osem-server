

<%@ page import="com.osem.Unit"%>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<g:set var="entityName"
	value="${message(code: 'unit.label', default: 'Unit')}" />
<title><g:message code="default.list.label" args="[entityName]" /></title>
</head>
<body>
	<div class="col-md-2 bootstrap-admin-col-left">
		<ul class="nav navbar-collapse collapse bootstrap-admin-navbar-side">
			<li><g:link action="create">
					<i class="glyphicon glyphicon-plus"></i>
					<g:message code="default.new.label" args="[entityName]" />
				</g:link></li>
		</ul>
	</div>

	<div class="col-md-10">
		<div class="row">
			<div class="col-lg-12">
				<div class="page-header">
					<h1>
						<g:message code="default.list.label" args="[entityName]" />
					</h1>
				</div>
				<g:if test="${flash.message}">
					<div class="page-body">
						<div class="alert alert-success" role="status">
							<a class="close" data-dismiss="alert" href="#">&times;</a>
							${flash.message}
						</div>
					</div>
				</g:if>
			</div>
		</div>

		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="bootstrap-admin-panel-content">
						<g:if test="${unitInstanceList}">
							<table class="table bootstrap-admin-table-with-actions">
								<thead>
									<tr>
										<g:sortableColumn property="company"
											title="${message(code: 'unit.company.label', default: 'Company')}" />

										<g:sortableColumn property="unitCode"
											title="${message(code: 'unit.unitCode.label', default: 'Unit Code')}" />


										<g:sortableColumn property="unitName"
											title="${message(code: 'unit.unitName.label', default: 'Unit Name')}" />

										<g:sortableColumn property="unitHead"
											title="${message(code: 'unit.unitHead.label', default: 'Unit Head')}" />


									</tr>
								</thead>
								<tbody>
									<g:each in="${unitInstanceList}" status="i" var="unitInstance">
										<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">

											<td>
												${fieldValue(bean: unitInstance, field: "company.compName")}
											</td>

											<td><g:link action="show" id="${unitInstance.id}">
													${fieldValue(bean: unitInstance, field: "unitCode")}
												</g:link></td>

											<td>
												${fieldValue(bean: unitInstance, field: "unitName")}
											</td>

											<td>
												${fieldValue(bean: unitInstance, field: "unitHead")}
											</td>

										</tr>
									</g:each>
								</tbody>
							</table>
							<div class="pagination">
								<g:paginate total="${unitInstanceCount ?: 0}" />
							</div>
						</g:if>
						<g:else>
							No Unit found
						</g:else>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
