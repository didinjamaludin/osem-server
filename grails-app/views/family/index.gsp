

<%@ page import="com.osem.Family" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<g:set var="entityName"
	value="${message(code: 'family.label', default: 'Family')}" />
<title><g:message code="default.list.label" args="[entityName]" /></title>
</head>
<body>
	<div class="col-md-2 bootstrap-admin-col-left">
		<ul class="nav navbar-collapse collapse bootstrap-admin-navbar-side">
			<li><g:link action="create">
					<i class="glyphicon glyphicon-plus"></i>
					<g:message code="default.new.label" args="[entityName]" />
				</g:link></li>
		</ul>
	</div>

	<div class="col-md-10">
		<div class="row">
			<div class="col-lg-12">
				<div class="page-header">
					<h1>
						<g:message code="default.list.label" args="[entityName]" />
					</h1>
				</div>
				<g:if test="${flash.message}">
					<div class="page-body">
						<div class="alert alert-success" role="status">
							<a class="close" data-dismiss="alert" href="#">&times;</a>${flash.message}
						</div>
					</div>
				</g:if>
			</div>
		</div>

		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="bootstrap-admin-panel-content">
						<g:if test="${familyInstanceList}">
							<table class="table bootstrap-admin-table-with-actions">
								<thead>
									<tr>
										
						<g:sortableColumn property="famAge" title="${message(code: 'family.famAge.label', default: 'Fam Age')}" />
					
						<g:sortableColumn property="famEducation" title="${message(code: 'family.famEducation.label', default: 'Fam Education')}" />
					
						<g:sortableColumn property="famGender" title="${message(code: 'family.famGender.label', default: 'Fam Gender')}" />
					
						<g:sortableColumn property="famJob" title="${message(code: 'family.famJob.label', default: 'Fam Job')}" />
					
						<g:sortableColumn property="famName" title="${message(code: 'family.famName.label', default: 'Fam Name')}" />
					
						<g:sortableColumn property="famPhone" title="${message(code: 'family.famPhone.label', default: 'Fam Phone')}" />
					
									</tr>
								</thead>
								<tbody>
									<g:each in="${familyInstanceList}" status="i"
										var="familyInstance">
										<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
											
						<td><g:link action="show" id="${familyInstance.id}">${fieldValue(bean: familyInstance, field: "famAge")}</g:link></td>
					
						<td>${fieldValue(bean: familyInstance, field: "famEducation")}</td>
					
						<td>${fieldValue(bean: familyInstance, field: "famGender")}</td>
					
						<td>${fieldValue(bean: familyInstance, field: "famJob")}</td>
					
						<td>${fieldValue(bean: familyInstance, field: "famName")}</td>
					
						<td>${fieldValue(bean: familyInstance, field: "famPhone")}</td>
					
										</tr>
									</g:each>
								</tbody>
							</table>
							<div class="pagination">
								<g:paginate total="${familyInstanceCount ?: 0}" />
							</div>
						</g:if>
						<g:else>
							No family found
						</g:else>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
