<%@ page import="com.osem.Family"%>

<div
	class="form-group fieldcontain ${hasErrors(bean: familyInstance, field: 'famName', 'error')} ">
	<label class="col-lg-2 control-label" for="famName"> <g:message
			code="family.famName.label" default="Nama Lengkap" />

	</label>
	<div class="col-lg-10">
		<g:textField class="form-control" name="famName"
			value="${familyInstance?.famName}" />
	</div>

</div>

<div
	class="form-group fieldcontain ${hasErrors(bean: familyInstance, field: 'famRelationship', 'error')} ">
	<label class="col-lg-2 control-label" for="famRelationship"> <g:message
			code="family.famRelationship.label" default="Hubungan" />

	</label>
	<div class="col-lg-10">
		<g:textField class="form-control" name="famRelationship"
			value="${familyInstance?.famRelationship}" />
	</div>

</div>

<div
	class="form-group fieldcontain ${hasErrors(bean: familyInstance, field: 'famGender', 'error')} ">
	<label class="col-lg-2 control-label" for="famGender"> <g:message
			code="family.famGender.label" default="Jenis Kelamin" />

	</label>
	<div class="col-lg-10">
		<g:select class="form-control" from="${['Laki-laki','Perempuan']}"
			name="famGender" value="${familyInstance?.famGender}" />
	</div>

</div>

<div
	class="form-group fieldcontain ${hasErrors(bean: familyInstance, field: 'famAge', 'error')} ">
	<label class="col-lg-2 control-label" for="famAge"> <g:message
			code="family.famAge.label" default="Usia" />

	</label>
	<div class="col-lg-10">
		<g:field type="number" class="form-control" name="famAge"
			value="${familyInstance?.famAge}" />
	</div>

</div>

<div
	class="form-group fieldcontain ${hasErrors(bean: familyInstance, field: 'famEducation', 'error')} ">
	<label class="col-lg-2 control-label" for="famEducation"> <g:message
			code="family.famEducation.label" default="Pendidikan" />

	</label>
	<div class="col-lg-10">
		<g:textField class="form-control" name="famEducation"
			value="${familyInstance?.famEducation}" />
	</div>

</div>



<div
	class="form-group fieldcontain ${hasErrors(bean: familyInstance, field: 'famJob', 'error')} ">
	<label class="col-lg-2 control-label" for="famJob"> <g:message
			code="family.famJob.label" default="Pekerjaan" />

	</label>
	<div class="col-lg-10">
		<g:textField class="form-control" name="famJob"
			value="${familyInstance?.famJob}" />
	</div>

</div>



<div
	class="form-group fieldcontain ${hasErrors(bean: familyInstance, field: 'famPhone', 'error')} ">
	<label class="col-lg-2 control-label" for="famPhone"> <g:message
			code="family.famPhone.label" default="Nomor HP" />

	</label>
	<div class="col-lg-10">
		<g:textField class="form-control" name="famPhone"
			value="${familyInstance?.famPhone}" />
	</div>

</div>



<div
	class="form-group fieldcontain ${hasErrors(bean: familyInstance, field: 'person', 'error')} "
	style="display: none;">
	<label class="col-lg-2 control-label" for="person"> <g:message
			code="family.person.label" default="Person" />

	</label>
	<div class="col-lg-10">
		<g:select class="form-control" id="person" name="person.id"
			from="${com.osem.Person.list()}" optionKey="id" required=""
			value="${familyInstance?.person?.id}" />
	</div>

</div>

