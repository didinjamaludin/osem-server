

<%@ page import="com.osem.Family" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<g:set var="entityName"
	value="${message(code: 'family.label', default: 'Family')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
</head>
<body>
	<div class="col-md-2 bootstrap-admin-col-left">
		<ul class="nav navbar-collapse collapse bootstrap-admin-navbar-side">
			<li><g:link action="index">
					<i class="glyphicon glyphicon-list-alt"></i>
					<g:message code="default.list.label" args="[entityName]" />
				</g:link></li>
			<li><g:link action="create">
					<i class="glyphicon glyphicon-plus"></i>
					<g:message code="default.new.label" args="[entityName]" />
				</g:link></li>
		</ul>
	</div>

	<div class="col-md-10">
		<div class="row">
			<div class="col-lg-12">
				<div class="page-header">
					<h1>
						<g:message code="default.show.label" args="[entityName]" />
					</h1>
				</div>
				<g:if test="${flash.message}">
					<div class="page-body">
						<div class="alert alert-success" role="status">
							<a class="close" data-dismiss="alert" href="#">&times;</a>${flash.message}
						</div>
					</div>
				</g:if>
				<g:hasErrors bean="${familyInstance}">
					<div class="page-body">
						<ul class="alert alert-danger" role="alert">
							<g:eachError bean="${familyInstance}" var="error">
								<li
									<g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message
										error="${error}" /></li>
							</g:eachError>
						</ul>
					</div>
				</g:hasErrors>
			</div>
		</div>

		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default bootstrap-admin-no-table-panel">
					<div
						class="bootstrap-admin-no-table-panel-content bootstrap-admin-panel-content collapse in">
						<dl class="dl-horizontal">
							
				<g:if test="${familyInstance?.famAge}">
					<dt><g:message code="family.famAge.label" default="Fam Age" /></dt>
					
						<dd><g:fieldValue bean="${familyInstance}" field="famAge"/></dd>
					
				</g:if>
			
				<g:if test="${familyInstance?.famEducation}">
					<dt><g:message code="family.famEducation.label" default="Fam Education" /></dt>
					
						<dd><g:fieldValue bean="${familyInstance}" field="famEducation"/></dd>
					
				</g:if>
			
				<g:if test="${familyInstance?.famGender}">
					<dt><g:message code="family.famGender.label" default="Fam Gender" /></dt>
					
						<dd><g:fieldValue bean="${familyInstance}" field="famGender"/></dd>
					
				</g:if>
			
				<g:if test="${familyInstance?.famJob}">
					<dt><g:message code="family.famJob.label" default="Fam Job" /></dt>
					
						<dd><g:fieldValue bean="${familyInstance}" field="famJob"/></dd>
					
				</g:if>
			
				<g:if test="${familyInstance?.famName}">
					<dt><g:message code="family.famName.label" default="Fam Name" /></dt>
					
						<dd><g:fieldValue bean="${familyInstance}" field="famName"/></dd>
					
				</g:if>
			
				<g:if test="${familyInstance?.famPhone}">
					<dt><g:message code="family.famPhone.label" default="Fam Phone" /></dt>
					
						<dd><g:fieldValue bean="${familyInstance}" field="famPhone"/></dd>
					
				</g:if>
			
				<g:if test="${familyInstance?.famRelationship}">
					<dt><g:message code="family.famRelationship.label" default="Fam Relationship" /></dt>
					
						<dd><g:fieldValue bean="${familyInstance}" field="famRelationship"/></dd>
					
				</g:if>
			
				<g:if test="${familyInstance?.person}">
					<dt><g:message code="family.person.label" default="Person" /></dt>
					
						<dd><g:link controller="person" action="show" id="${familyInstance?.person?.id}">${familyInstance?.person?.encodeAsHTML()}</g:link></dd>
					
				</g:if>
			
						</dl>
						<g:form url="[resource:familyInstance, action:'delete']"
							method="DELETE">
							<fieldset class="buttons">
								<g:link class="btn btn-primary" action="edit"
									resource="${familyInstance}">
									<g:message code="default.button.edit.label" default="Edit" />
								</g:link>
								<g:actionSubmit class="btn btn-default" action="delete"
									value="${message(code: 'default.button.delete.label', default: 'Delete')}"
									onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
							</fieldset>
						</g:form>
					</div>
				</div>
			</div>
		</div>

	</div>
</body>
</html>
