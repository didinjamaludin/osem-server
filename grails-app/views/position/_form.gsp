<%@ page import="com.osem.Position" %>



<div class="form-group fieldcontain ${hasErrors(bean: positionInstance, field: 'posName', 'error')} ">
	<label class="col-lg-2 control-label" for="posName">
		<g:message code="position.posName.label" default="Pos Name" />
		
	</label>
	<div class="col-lg-10"><g:textField class="form-control" name="posName" value="${positionInstance?.posName}" /></div>

</div>

