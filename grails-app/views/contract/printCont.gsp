<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Surat Kontrak</title>
        <link rel="license"
        href="http://www.opensource.org/licenses/mit-license/">
        <style>
            /* reset */
            * {
            border: 0;
            box-sizing: content-box;
            color: inherit;
            font-family: inherit;
            font-size: inherit;
            font-style: inherit;
            font-weight: inherit;
            line-height: inherit;
            margin: 0;
            padding: 0;
            text-decoration: none;
            vertical-align: top;
            }

            /* content editable */
            *[contenteditable] {
            border-radius: 0.25em;
            min-width: 1em;
            outline: 0;
            }

            *[contenteditable] {
            cursor: pointer;
            }

            *[contenteditable]:hover, *[contenteditable]:focus, td:hover *[contenteditable],
            td:focus *[contenteditable], img.hover {
            background: #DEF;
            box-shadow: 0 0 1em 0.5em #DEF;
            }

            span[contenteditable] {
            display: inline-block;
            }

            /* heading */
            h1 {
            font: bold 36px sans-serif;
            letter-spacing: 0.05em;
            text-align: center;
            }

            /* page */
            html {
            font: 16px/1 'Open Sans', sans-serif;
            overflow: auto;
            padding: 0.5in;
            }

            html {
            background: #999;
            cursor: default;
            }

            body {
            box-sizing: border-box;
            /*height: 11.69in;*/
            margin: 0 auto;
            overflow: hidden;
            padding: 0.5in 1in;
            width: 8.27in;
            }

            body {
            background: #FFF;
            border-radius: 1px;
            box-shadow: 0 0 1in -0.25in rgba(0, 0, 0, 0.5);
            }

            /* header */
            header {
            margin: 0;
            padding-bottom:5px;
            border-bottom: solid 1px #999;
            }

            header:after {
            clear: both;
            content: "";
            display: table;
            }

            header .header-logo {
            float:left;
            width:100%;
            text-align:center;
            }

            /* article */
            article, article address, table.meta, table.inventory {
            margin: 0.2em 0 6em;
            padding-top: 1em;
            border-top: solid 3px #999;
            }

            article:after {
            clear: both;
            content: "";
            display: table;
            }

            article  {
            font-size:12px;	
            line-height: 15px;

            }

            article h1 {
            font-size: 18px;
            margin-bottom: 20px;
            }

            article h3 {
            text-align:center;
            font-weight: bold;
            }

            article p {
            padding: 10px 0 0 0;
            }

            article table {
            border: 0;
            }

            article table td {
            padding: 1px 5px;
            }

            ol {
            margin-bottom: 10px;
            }

            ol li {
            padding: 5px 10px;
            margin-left: 20px;
            }


            footer {
            position: relative;
            bottom: 0;
            font-size: 10px;
            border-bottom: solid 20px red;
            }

            /* javascript */
            .add, .cut {
            border-width: 1px;
            display: block;
            font-size: .8rem;
            padding: 0.25em 0.5em;
            float: left;
            text-align: center;
            width: 0.6em;
            }

            .add, .cut {
            background: #9AF;
            box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
            background-image: -moz-linear-gradient(#00ADEE 5%, #0078A5 100%);
            background-image: -webkit-linear-gradient(#00ADEE 5%, #0078A5 100%);
            border-radius: 0.5em;
            border-color: #0076A3;
            color: #FFF;
            cursor: pointer;
            font-weight: bold;
            text-shadow: 0 -1px 2px rgba(0, 0, 0, 0.333);
            }

            .add {
            margin: -2.5em 0 0;
            }

            .add:hover {
            background: #00ADEE;
            }

            .cut {
            opacity: 0;
            position: absolute;
            top: 0;
            left: -1.5em;
            }

            .cut {
            -webkit-transition: opacity 100ms ease-in;
            }

            tr:hover .cut {
            opacity: 1;
            }

            .control {
            position:absolute;
            top: 10px;
            left: 2.3in;
            }

            .control a {
            padding: 3px 10px;
            background: #ccc;
            font-size: 12px;
            font-weight: bold;
            margin: 5px;
            }

            .control a:hover {
            background: #fff;
            }

            .uline {
            text-decoration: underline;
            }

            .bold {
            font-weight:bold;
            }

            @media all {
            .page-break,.spacer {
            display: none;
            }
            }

            @media print {
            * {
            -webkit-print-color-adjust: exact;
            }
            html {
            background: none;
            padding: 0;
            }
            body {
            box-shadow: none;
            margin: 0;
            }
            span:empty {
            display: none;
            }
            .add, .cut {
            display: none;
            }
            .control {
            display:none;
            }
            .page-break {
            display: block;
            page-break-before: always;
            }
            .spacer {
            display:block;
            height:0.5in;
            }
            }

            @page {
            margin: 0;
            }
        </style>
    </head>
    <body>
        <div class="control"><a href="#" onclick="PrintWebPage()">Print</a><g:link controller="hr">Close</g:link></div>
            <header>
                <div class="header-logo">
                    <img alt="" src="https://s3-ap-southeast-1.amazonaws.com/static.mitragardamandiri/${userInstance?.company?.compLogo}" />
            </div>
        </header>
        <article>
            <h1 style="margin-top: 20px;">Perjanjian Perpanjangan Jangka Waktu Kerja</h1>
            <p>Perjanjian perpanjangan jangka waktu kerja ini ditandatangani di ${userInstance?.company?.compCity} pada hari ini, 
                <span class="uline bold"><g:formatDate date="${contractInstance?.createDate}" format="dd MMMMM yyyy"/></span>  oleh dan antara yang bertandatangan di bawah ini :</p>

            <table>
                <tr>
                    <td>1.</td>
                    <td colspan="3">${userInstance?.company?.compName}</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td width="25%">Alamat</td>
                    <td>:</td>
                    <td><span class="uline">${userInstance?.company?.compAddress} - ${userInstance?.company?.compCity} Telp. ${userInstance?.company?.compPhone}</span></td>
                </tr>
                <tr>
                    <td>2.</td>
                    <td>Nama Karyawan</td>
                    <td>:</td>
                    <td><span class="uline bold">${contractInstance?.person?.fullname}</span></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>Jenis Kelamin</td>
                    <td>:</td>
                    <td><span class="uline">${contractInstance?.person?.gender}</span></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>Umur</td>
                    <td>:</td>
                    <td><span class="uline">${new Date()[java.util.Calendar.YEAR]-contractInstance?.person?.dob[java.util.Calendar.YEAR]} tahun</span></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>Alamat</td>
                    <td>:</td>
                    <td><span class="uline">${contractInstance?.person?.currentAddr} ${contractInstance?.person?.currentCity}</span></td>
                </tr>
            </table>
            <p>(Pihak Pertama dan Pihak Kedua secara bersama-sama selanjutnya disebut sebagai “Para Pihak”).
                Kedua belah Pihak terlebih dahulu menerangkan bahwa :
            </p>
            <table>
                <tr>
                    <td>3.</td>
                    <td colspan="3"><span class="bold uline">${userInstance?.company?.compName}</span> adalah sebuah perusahaan yang bergerak dibidang 
                        pengembangan sumber daya manusia dan usaha-usaha lain (outsourcing) yang dalam menjalankan 
                        kegiatan usahanya membutuhkan pekerja yang memiliki pengalaman dan keterampilan yang dibutuhkan 
                        serta memenuhi kualifikasi yang ditentukan.</td>
                </tr>
                <tr>
                    <td>4.</td>
                    <td colspan="3">Pihak Kedua memenuhi kualifikasi yang ditentukan serta memiliki pengalaman 
                        dan keterampilan kerja untuk melakukan pekerjaan dibidang <span class="bold uline">${contractInstance?.occupation}</span> selama <span class="bold uline">${contractInstance?.contractLength}
                            bulan</span> dan akan diposisikan sebagai <span class="bold uline">${contractInstance?.position?.posName}</span> Pihak Kedua bersedia ditempatkan di semua unit usaha ${userInstance?.company?.compInitial}.
                    </td>
                </tr>
                <tr>
                    <td>5.</td>
                    <td colspan="3">Pihak Kedua bersedia mengikuti peraturan ${userInstance?.company?.compInitial} dan/atau peraturan perusahaan pengguna jasa serta melaksanakan tugas yang dipercayakan oleh pimpinan.
                    </td>
                </tr>
                <tr>
                    <td>6.</td>
                    <td colspan="3">Pihak Pertama menawarkan pekerjaan tersebut dengan hubungan kerja untuk jangka waktu tertentu dan Pihak Kedua menerima 
                        tawaran Pihak Pertama untuk bekerja dalam jangka waktu tertentu dengan segala konsekuensi hukumnya.
                    </td>
                </tr>
            </table>    

            <p>Selanjutnya, berdasarkan hal-hal tersebut diatas, Para Pihak sepakat untuk menuangkan kesepakatan diantara 
                mereka di atas dalam Perjanjian Kerja ini dengan syarat-syarat dan kententuan-ketentuan sebagai berikut :</p>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <h3>Pasal 1<br>
                Jangka Waktu Perjanjian Kerja
            </h3>
            <p>&nbsp;</p>
            <table>
                <tr>
                    <td>1.</td>
                    <td colspan="3">Perjanjian Kerja ini berlaku paling lama ${contractInstance?.contractLength} bulan 
                        dimulai pada <g:formatDate date="${contractInstance?.startDate}" format="dd MMMMM yyyy" /> dan akan 
                        berakhir pada <g:formatDate date="${contractInstance?.endDate}" format="dd MMMMM yyyy" />
                    </td>
                </tr>
                <tr>
                    <td>2.</td>
                    <td colspan="3">Selama masa ikatan dinas tersebut, Pihak Pertama akan melakukan regular evaluasi terhadap kinerja Pihak Kedua.
                    </td>
                </tr>
                <tr>
                    <td>3.</td>
                    <td colspan="3">Apabila Pihak Pertama bermaksud untuk memperpanjang jangka waktu, maka Pihak Pertama akan memberitahukan keinginannya tersebut kepada Pihak Kedua secara tertulis selambat-lambatnya 7 (tujuh) hari kerja sebelum tanggal berakhir Perjanjian Kerja.
                    </td>
                </tr>
                <tr>
                    <td>4.</td>
                    <td colspan="3">Apabila Pihak Pertama tidak memberitahukan keinginannya untuk memperpanjang jangka waktu secara tertulis sebagaimana diatur dalam ayat 3 diatas, maka Perjanjian Kerja ini dianggap tidak akan diperpanjang dan akan berakhir dengan sendirinya pada tanggal berakhir Perjanjian Kerja.
                    </td>
                </tr>
            </table>
        </article>
        <footer>
            <p style="text-align:center"><strong>${userInstance?.company?.compName}</strong><br>
                ${userInstance?.company?.compAddress}, ${userInstance?.company?.compCity}, Indonesia<br>
                Phone : ${userInstance?.company?.compPhone}<br>
                Email : ${userInstance?.company?.compEmail}<br>
                Website : ${userInstance?.company?.compWebsite}</p>
        </footer>
        <div class="page-break"></div>
        <div class="spacer">&nbsp;</div>
        <header>
            <div class="header-logo">
                <img alt="" src="https://s3-ap-southeast-1.amazonaws.com/static.mitragardamandiri/${userInstance?.company?.compLogo}" />
            </div>
        </header>
        <article>
            <h3 style="margin-top: 20px;">Pasal 2<br>
                Tugas dan Tempat Bekerja
            </h3>
            <p>&nbsp;</p>
            <table>
                <tr>
                    <td>1.</td>
                    <td colspan="3">Pihak Pertama akan memperkerjakan Pihak Kedua sebagai <span class="uline bold">${contractInstance?.occupation}</span> yang akan ditempatkan di unit kerja yang ditunjuk oleh Pihak Pertama dan Pihak Kedua dengan ini menerima keputusan Pihak Pertama tersebut.
                    </td>
                </tr>
                <tr>
                    <td>2.</td>
                    <g:if test="${userInstance?.company?.compName=='PT. MITRA GARDA MANDIRI'}">
                        <td colspan="3">Dalam pelaksanaan tugas sehari-hari, Pihak Kedua wajib bertanggung jawab dan melapor kepada 
                            <span class="uline bold">Dan Ru (Komandan Regu), Dan Unit (Komandan Unit) Dan Sektor ( Komandan Sektor )</span> sebagai pimpinan langsung Pihak Kedua.
                        </td>
                    </g:if>
                    <g:else>
                        <td colspan="3">Dalam pelaksanaan tugas sehari-hari, Pihak Kedua wajib bertanggung jawab dan melapor kepada 
                            <span class="uline bold">Supervisor, Manajer</span> sebagai pimpinan langsung Pihak Kedua.
                        </td>
                    </g:else>
                </tr>
                <tr>
                    <td>3.</td>
                    <td colspan="3">Tugas dan tanggung jawab Pihak Kedua sebagai  <span class="uline bold">${contractInstance?.occupation}</span>  diatur dalam deskripsi pekerjaan yang ditentukan dan untuk pekerjaan yang belum diatur dalam deskripsi pekerjaan selanjutnya akan ditetapkan tersendiri oleh pimpinan.
                    </td>
                </tr>
            </table>
            <p>&nbsp;</p>
            <h3>Pasal 3<br>
                Hari, Jam Kerja dan Cuti
            </h3>
            <p>&nbsp;</p>
            <table>
                <tr>
                    <td>1.</td>
                    <td colspan="3">4 (empat) hari kerja 2 (hari) libur.
                    </td>
                </tr>
                <tr>
                    <td>2.</td>
                    <td colspan="3">12 (duabelas) jam dengan jam kerja sesuai dengan peraturan yang berlaku pada Pihak Pertama atau perusahaan pengguna jasa. Kecuali ditetapkan lain oleh Pihak Pertama atau perusahaan pengguna jasa. Pihak Kedua selama hari kerja sebagaimana yang dimaksud dalam ayat 1 diatas, diwajibkan untuk melaksanakan seluruh kewajibannya sesuai dengan jadwal kerja yang telah ditentukan dengan hak istirahat 1 (satu) jam yang diatur oleh pimpinan. Kecuali ditetapkan lain oleh Pihak Pertama atau perusahaan pengguna jasa.
                    </td>
                </tr>
                <tr>
                    <td>3.</td>
                    <td colspan="3">Setelah bekerja selama 12 (dua belas) bulan berturut-turut, Pihak Kedua berhak mendapat jatah cuti sebanyak 12 (dua belas) hari selama periode Perjanjian Kerja Untuk Jangka Waktu Tertentu.
                    </td>
                </tr>
            </table>
            <p>&nbsp;</p>
            <h3>Pasal 4<br>
                Ketidakhadiran
            </h3>
            <p>&nbsp;</p>
            <table>
                <tr>
                    <td>1.</td>
                    <td colspan="3">Jika Pihak Kedua tidak masuk kerja pada hari kerja lebih dari 1 (satu) hari karena sakit, maka hal itu harus dibuktikan dengan surat keterangan sakit dari dokter atau rumah sakit. Tanpa surat keterangan sakit tersebut, Pihak Kedua dianggap tidak masuk kerja tanpa seizin pimpinan (in discipliner) dan akan dikenakan sanksi.
                    </td>
                </tr>
                <tr>
                    <td>2.</td>
                    <td colspan="3">Adapun pemberian dan bentuk sanksi yang dikenakan kepada Pihak Kedua akan ditentukan oleh pimpinan yang bersangkutan.
                    </td>
                </tr>
            </table>
            <p>&nbsp;</p>
            <h3>Pasal 5<br>
                Upah Bulanan
            </h3>
            <p>&nbsp;</p>
            <table>
                <tr>
                    <td>1.</td>
                    <td colspan="3">Selama Perjanjian Kerja ini berlangsung, 
                        Pihak Pertama wajib membayar upah pokok bulanan kepada Pihak Kedua sebesar <span class="uline bold"><g:formatNumber number="${salaryInstance?.basicSalary}" format="Rp###,##0" />,-</span>.
                    </td>
                </tr>
                <tr>
                    <td>2.</td>
                    <td colspan="3">Pembayaran upah akan dilakukan pada setiap tanggal 3 (tiga) dan dilakukan dengan cara pembayaran transfer/cash (tunai) oleh Pihak Pertama kepada Pihak Kedua.
                    </td>
                </tr>
            </table>
        </article>
        <footer>
            <p style="text-align:center"><strong>${userInstance?.company?.compName}</strong><br>
                ${userInstance?.company?.compAddress}, ${userInstance?.company?.compCity}, Indonesia<br>
                Phone : ${userInstance?.company?.compPhone}<br>
                Email : ${userInstance?.company?.compEmail}<br>
                Website : ${userInstance?.company?.compWebsite}</p>
        </footer>
        <div class="page-break"></div>
        <div class="spacer">&nbsp;</div>
        <header>
            <div class="header-logo">
                <img alt="" src="https://s3-ap-southeast-1.amazonaws.com/static.mitragardamandiri/${userInstance?.company?.compLogo}" />
            </div>
        </header>
        <article>
            <h3 style="margin-top: 20px;">Pasal 6<br>
                Uang Transport dan Uang Makan
            </h3>
            <p>&nbsp;</p>
            <table>
                <tr>
                    <td>1.</td>
                    <td colspan="3">Pihak Kedua berhak atas penggantian uang transport untuk biaya angkutan datang ke dan pulang dari kantor Pihak Pertama, yang besarnya sesuai dengan ketentuan yang berlaku 
                        di lingkup perusahaan Pihak Pertama yaitu sebesar <span class="uline bold">Rp<g:if test="${salaryInstance?.transAllow}"><g:formatNumber number="${salaryInstance?.transAllow}" format="###,##0" /></g:if>,-</span> /bulan dan dibayarkan bersamaan dengan upah bulanan.
                        </td>
                    </tr>
                </table>
                <p>&nbsp;</p>
                <h3>Pasal 7<br>
                Tunjangan Hari Raya
            </h3>
            <p>&nbsp;</p>
            <table>
                <tr>
                    <td>1.</td>
                    <td colspan="3">Selama Perjanjian ini berlangsung, Pihak Pertama akan memberikan Tunjangan Hari Raya (THR) sesuai dengan peraturan perusahaan yang berlaku yaitu sebesar 1x (satu kali) upah pokok bulanan sebagaimana tersebut pada pasal 5 ayat 1 perjanjian ini, jika sampai hari raya itu Pihak Kedua telah bekerja genap 1 (satu) tahun.
                    </td>
                </tr>
                <tr>
                    <td>2.</td>
                    <td colspan="3">Seandainya Pihak Kedua belum genap bekerja selama 1 (satu) tahun, tetapi sudah bekerja selama 3 (tiga) bulan pada saat hari raya keagamaan itu tiba, maka Pihak Pertama akan membayar Tunjangan Hari Raya (THR) secara prorata kepada Pihak Kedua.
                    </td>
                </tr>
            </table>    
            <p>&nbsp;</p>
            <h3>Pasal 8<br>
                Kewajiban Pihak Kedua
            </h3>
            <p>Pihak Kedua wajib untuk :</p>
            <table>
                <tr>
                    <td>1.</td>
                    <td colspan="3">Hadir dan menjalankan tugas selambat-lambatnya pada saat jam kerja dimulai dengan menggunakan waktu untuk bekerja dengan sebaik-baiknya sampai jam bekerja berakhir.
                    </td>
                </tr>
                <tr>
                    <td>2.</td>
                    <td colspan="3">Melaksanakan semua tugas yang dipercayakan kepadanya dan menjadi tanggung jawabnya sebaik mungkin dan menggunakan pengetahuan dan keterampilannya semaksimal mungkin dengan memperhatikan petunjuk dan arahan pimpinannya.
                    </td>
                </tr>
                <tr>
                    <td>3.</td>
                    <td colspan="3">Melaksanakan dan mematuhi semua peraturan, petunjuk, arahan, dan instruksi yang diberikan, baik yang dibuat khusus oleh Pihak Pertama dan/atau perusahaan pengguna jasa maupun yang termuat dalam peraturan perundang-undangan yang berlaku di Negara Indonesia.
                    </td>
                </tr>
                <tr>
                    <td>4.</td>
                    <td colspan="3">Menolak setiap pemberian atau janji perlakuan yang diberikan oleh siapa pun juga atau melakukan perbuatan yang berakibat langsung ataupun tidak langsung yang dapat merugikan perusahaan Pihak Pertama, baik materiil maupun non-materiil.
                    </td>
                </tr>
                <tr>
                    <td>5.</td>
                    <td colspan="3">Menolak ataupun tidak melakukan pekerjaan di luar perusahaan yang sama atau sejenis kegiatan usahanya dengan perusahaan Pihak Pertama, yang dapat merugikan usaha Pihak Pertama baik langsung maupun tidak langsung dan tidak menjadikan Pihak Pertama sebagai ajang politik.
                    </td>
                </tr>
                <tr>
                    <td>6.</td>
                    <td colspan="3">Memegang teguh rahasia perusahaan Pihak Pertama, baik yang dipercayakan secara khusus kepadanya maupun yang harus dipegang teguh oleh karyawan pada umumnya.
                    </td>
                </tr>
                <tr>
                    <td>7.</td>
                    <td colspan="3">Menjaga, menyimpan dan memelihara barang-barang termasuk surat-surat dan dokumen-dokumen milik atau berada dalam penguasaan perusahaan Pihak Pertama yang digunakannya atau dipercayakan kepadanya sehingga selalu dalam keadaan aman dan/atau berfungsi baik.
                    </td>
                </tr>
                <tr>
                    <td>8.</td>
                    <td colspan="3">Menjaga, memelihara dan meningkatkan nama baik Pihak Pertama baik didalam maupun diluar perusahaan.
                    </td>
                </tr>
                <tr>
                    <td>9.</td>
                    <td colspan="3">Menghormati, bersikap ramah dan menjaga sopan santun dalam berhubungan dengan karyawan/tamu Pihak Pertama mapupun karyawan/tamu perusahaan pengguna jasa yang lain.
                    </td>
                </tr>
            </table>
        </article>
        <footer>
            <p style="text-align:center"><strong>${userInstance?.company?.compName}</strong><br>
                ${userInstance?.company?.compAddress}, ${userInstance?.company?.compCity}, Indonesia<br>
                Phone : ${userInstance?.company?.compPhone}<br>
                Email : ${userInstance?.company?.compEmail}<br>
                Website : ${userInstance?.company?.compWebsite}</p>
        </footer>
        <div class="page-break"></div>
        <div class="spacer">&nbsp;</div>
        <header>
            <div class="header-logo">
                <img alt="" src="https://s3-ap-southeast-1.amazonaws.com/static.mitragardamandiri/${userInstance?.company?.compLogo}" />
            </div>
        </header>
        <article>
            <table style="margin-top: 20px;">   
                <tr>
                    <td>10.</td>
                    <td colspan="3">Menghindari perbuatan tercela, seperti membuat keributan, keonaran, pertengkaran/perkelahian, terlibat dalam penyalahgunaan narkoba atau zat adiktif lainnya, melakukan atau terlibat tindakan kriminal atau tindakan yang bertentangan dengan hukum, dan lain-lain yang berakibat mengganggu ketertiban, kelancaran, serta ketenangan bekerja dalam perusahaan Pihak Pertama maupun perusahaan pengguna jasa.
                    </td>
                </tr>
                <tr>
                    <td>11.</td>
                    <td colspan="3">Tidak menyebar gosip yang berbau SARA atau hal yang merugikan Pihak Pertama maupun perusahaan pengguna jasa.
                    </td>
                </tr>
                <tr>
                    <td>12.</td>
                    <td colspan="3">Tidak melakukan sogok/suap untuk dapat bergabung dengan Pihak Pertama.
                    </td>
                </tr>
            </table>  
            <p>&nbsp;</p>
            <h3>Pasal 9<br>
                Berakhirnya Perjanjian Kerja Untuk Jangka Waktu Tertentu
            </h3>
            <p>Perjanjian Kerja Untuk Jangka Waktu Tertentu ini akan berakhir demi hukum dan tanpa syarat dan Pihak Pertama tidak berkewajiban untuk memberikan pembayaran dalam bentuk dan/atau atas dasar apapun kepada Pihak Kedua pada saat berakhirnya jangka waktu apabila terjadi hal-hal sebagai berikut:</p>
            <p>Jangka waktu Perjanjian Kerja ini berakhir :</p>
            <p>&nbsp;</p>
            <table>
                <tr>
                    <td>1.</td>
                    <td colspan="3">Pihak Kedua meninggal dunia.
                    </td>
                </tr>
                <tr>
                    <td>2.</td>
                    <td colspan="3">Pihak Kedua tidak cakap atau lalai dalam menjalankan tugas dan kewajibannya yang telah dipercayakan oleh Pihak Pertama walaupun telah mendapat teguran/peringatan dari Pihak Pertama baik secara tertulis maupun lisan.
                    </td>
                </tr>
                <tr>
                    <td>3.</td>
                    <td colspan="3">Pihak Kedua melakukan kesalahan berdasarkan peraturan Pihak Pertama maupun peraturan perundang-undangan yang berlaku, yang memungkinkan untuk dilakukannya pengakhiran Perjanjian Kerja ini sebelum tanggal berakhir.
                    </td>
                </tr>
                <tr>
                    <td>4.</td>
                    <td colspan="3">Pihak Kedua terlibat dengan tindakan kriminal atau terlibat dalam penyalahgunaan narkoba atau zat adiktif lainnya, yang memungkinkan untuk dilakukannya pengakhiran Perjanjian Kerja ini secara langsung sebelum tanggal berakhir Perjanjian Kerja ini.
                    </td>
                </tr>
                <tr>
                    <td>5.</td>
                    <td colspan="3">Dalam hal Perjanjian Kerja ini berakhir pada tanggal berakhir atau berakhir karena sebab apapun, maka Pihak Kedua wajib menyerahkan seluruh dokumen dan informasi dalam bentuk lainnya kepada Pihak Pertama dan tidak diperkenankan menghapus data-data yang terdapat di dalam sistem komputer Pihak Pertama, kecuali untuk data-data pribadi. Pihak Kedua wajib mengembalikan seluruh barang-barang atau fix asset dan tidak diperkenankan membawa untuk keperluan pribadi.
                    </td>
                </tr>
                <tr>
                    <td>6.</td>
                    <td colspan="3">Seluruh hasil pekerjaan Pihak Kedua merupakan milik Pihak Pertama. Pihak Pertama berhak untuk melakukan tindakan-tindakan hukum yang dianggap perlu terhadap Pihak Kedua, apabila Pihak Kedua melanggar ketentuan ayat 6 ini.
                    </td>
                </tr>
            </table>
            <p>&nbsp;</p>
            <h3>Pasal 10<br>
                Kerahasiaan
            </h3>
            <p>&nbsp;</p>
            <table>
                <tr>
                    <td>1.</td>
                    <td colspan="3">Selama dan setelah berakhirnya Perjanjian Kerja ini, Pihak Kedua wajib menjaga kerahasiaan seluruh inforamasi dan dokumen berkenaan dengan usaha Pihak Pertama yang diberikan dan diterima dari Pihak Pertama dan/atau pihak terkait lainnya yang mempunyai hubungan kerja dengan Pihak Pertama selama Pihak Kedua bekerja pada Pihak Pertama yang diberikan dalam rangka pelaksanaan tugas dan kewajiban Pihak Kedua.
                    </td>
                </tr>
            </table>
            <br>
        </article>
        <footer>
            <p style="text-align:center"><strong>${userInstance?.company?.compName}</strong><br>
                ${userInstance?.company?.compAddress}, ${userInstance?.company?.compCity}, Indonesia<br>
                Phone : ${userInstance?.company?.compPhone}<br>
                Email : ${userInstance?.company?.compEmail}<br>
                Website : ${userInstance?.company?.compWebsite}</p>
        </footer>
        <div class="page-break"></div>
        <div class="spacer">&nbsp;</div>
        <header>
            <div class="header-logo">
                <img alt="" src="https://s3-ap-southeast-1.amazonaws.com/static.mitragardamandiri/${userInstance?.company?.compLogo}" />
            </div>
        </header>
        <article>
            <table style="margin-top: 20px;">
                <tr>
                    <td>2.</td>
                    <td colspan="3">Karena sebab dan alasan apapun, Pihak Kedua dilarang untuk membeberkan, membocorkan, menyerahkan atau memberitahukan dalam bentuk apapun, atau mengizinkan informasi dan dokumen rahasia tersebut diberitahukan atau diserahkan kepada Pihak Ketiga manapun. Dikecualikan dari larangan tersebut adalah dalam hal pemberitahuan atau penyerahan tersebut adalah atas permintaan resmi tertulis dari instansi pemerintah yang berwenang. Pemberitahuan atau penyerahan tersebut hanya boleh dilakukan setelah Pihak Kedua menyerahkan surat permintaan dari instansi pemerintah yang berwenang tersebut kepada Pihak Pertama.
                    </td>
                </tr>
            </table>
            <p>&nbsp;</p>
            <h3>Pasal 11<br>
                Penutup
            </h3>
            <p>&nbsp;</p>
            <table>
                <tr>
                    <td>1.</td>
                    <td colspan="3">Setiap perubahan atas Perjanjian Kerja ini wajib dibuat secara tertulis dan ditandatangani oleh Para Pihak dan perubahan tersebut menjadi kesatuan dan bagian yang tak terpisahkan dari Perjanjian Kerja ini.
                    </td>
                </tr>
                <tr>
                    <td>2.</td>
                    <td colspan="3">Perjanjian Kerja ini mencakup semua kesepakatan dan janji yang dibuat oleh Para Pihak dan karenanya menggantikan semua kesepakatan dan janji yang pernah dibuat oleh Para Pihak baik secara tertulis maupun lisan.
                    </td>
                </tr>
            </table>

            <p>Hal-hal lain yang belum atau tidak cukup diatur dalam Perjanjian Kerja ini akan diatur lebih lanjut dalam Perjanjian Kerja Bersama yang berlaku didalam perusahaan Pihak Pertama.</p>
            <p>Demikian Perjanjian Perpanjangan Jangka Waktu Kerja ini ditandatangani oleh Para Pihak pada tanggal sebagaimana tercantum pada awal naskah ini dalam rangkap dua (dua) yang sama bunyinya, bermaterai cukup dan setiap rangkapnya mempunyai dasar dan kekuatan hukum yang sama. Masing-masing Pihak memegang dan memiliki 1 (satu) surat setelah ditandatangani oleh Para Pihak untuk dipergunakan sebagaimana mestinya. Masing-masing eksemplar yang telah ditandatangani dianggap sebagai eksemplar yang asli.</p>
            <div class="spacer">&nbsp;</div>
            <div class="spacer">&nbsp;</div>
            <p>&nbsp;</p>
            <p style="text-align:center;">${userInstance?.company?.compCity}, <span class="uline bold"><g:formatDate date="${new Date()}" format="dd MMMMM yyyy" /></span></p>
            <p>&nbsp;</p>
            <table width="100%" class="sign-table">
                <tr>
                    <td style="text-align: center" width="50%">Pihak Pertama<br>${userInstance?.company?.compName}</td>
                    <td style="text-align: center" width="50%">Pihak Kedua</td>
                </tr>
                <tr>
                    <td style="height:80px;">&nbsp;</td>
                    <td style="height:80px;">&nbsp;</td>
                </tr>
                <tr>
                    <td style="text-align: center;"><span class="uline bold">${userInstance?.fullname}</span></td>
                    <td style="text-align: center;"><span class="uline bold">${contractInstance?.person?.fullname}</span></td>
                </tr>
                <tr>
                    <td style="text-align: center"><span class="bold">${userInstance?.position?.posName} ${userInstance?.dept.deptName}</span></td>
                    <td style="text-align: center"><span class="bold">${contractInstance?.person?.pid}</span></td>
                </tr>
            </table>
            <p>&nbsp;<br>&nbsp;</p>
        </article>
        <footer>
            <p style="text-align:center"><strong>${userInstance?.company?.compName}</strong><br>
                ${userInstance?.company?.compAddress}, ${userInstance?.company?.compCity}, Indonesia<br>
                Phone : ${userInstance?.company?.compPhone}<br>
                Email : ${userInstance?.company?.compEmail}<br>
                Website : ${userInstance?.company?.compWebsite}</p>
        </footer>
        <script>
            function PrintWebPage() {
            window.print();
            }
        </script>
    </body>
</html>