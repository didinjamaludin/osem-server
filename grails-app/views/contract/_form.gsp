<%@ page import="com.osem.Contract" %>

<div class="form-group fieldcontain ${hasErrors(bean: contractInstance, field: 'person', 'error')} ">
	<label class="col-lg-2 control-label" for="person">
		<g:message code="contract.person.label" default="Karyawan" />
		
	</label>
	<div class="col-lg-10"><g:select class="form-control" id="person" name="person.id" from="${com.osem.Person.list()}" optionKey="id" required="" value="${contractInstance?.person?.id}"/></div>

</div>

<div class="form-group fieldcontain ${hasErrors(bean: contractInstance, field: 'contractLength', 'error')} ">
	<label class="col-lg-2 control-label" for="contractLength">
		<g:message code="contract.contractLength.label" default="Lama Kontrak (bulan)" />
		
	</label>
	<div class="col-lg-10"><g:field class="form-control" type="number" name="contractLength" value="${contractInstance.contractLength}" onkeyup="updateRange(this.value)" /></div>

</div>

<div class="form-group fieldcontain ${hasErrors(bean: contractInstance, field: 'startDate', 'error')} ">
	<label class="col-lg-2 control-label" for="startDate">
		<g:message code="contract.startDate.label" default="Tgl Mulai" />
		
	</label>
	<div class="col-lg-10"><g:datePicker class="form-control" name="startDate" precision="day" value="${contractInstance?.startDate}" /></div>

</div>

<div class="form-group fieldcontain ${hasErrors(bean: contractInstance, field: 'endDate', 'error')} ">
	<label class="col-lg-2 control-label" for="endDate">
		<g:message code="contract.endDate.label" default="Tgl Berakhir" />
		
	</label>
	<div class="col-lg-10"><g:datePicker class="form-control" name="endDate" precision="day" value="${contractInstance?.endDate}" /></div>

</div>

<div class="form-group fieldcontain ${hasErrors(bean: contractInstance, field: 'occupation', 'error')} ">
	<label class="col-lg-2 control-label" for="occupation">
		<g:message code="contract.occupation.label" default="Bidang Kerja" />
		
	</label>
	<div class="col-lg-10"><g:textField name="occupation" class="form-control" value="${contractInstance?.occupation}" /></div>

</div>

<div class="form-group fieldcontain ${hasErrors(bean: contractInstance, field: 'position', 'error')} ">
	<label class="col-lg-2 control-label" for="position"> <g:message
			code="contract.subunit.label" default="Posisi/Jabatan" />
	</label>
	<div class="col-lg-10">
		<g:select id="position" name="position.id" from="${com.osem.Position.list()}"
			optionKey="id" required="" value="${contractInstance?.position?.id}"
			class="form-control" />
	</div>
</div>

<div class="form-group fieldcontain ${hasErrors(bean: contractInstance, field: 'contractNotes', 'error')} ">
	<label class="col-lg-2 control-label" for="contractNotes">
		<g:message code="contract.contractNotes.label" default="Keterangan" />
		
	</label>
	<div class="col-lg-10"><g:textArea class="form-control" name="contractNotes" value="${contractInstance?.contractNotes}" /></div>

</div>

<div class="form-group">
	<label class="col-lg-2 control-label" for="printCont"> Print
		Kontrak </label>
	<div class="col-lg-10">
		<div class="form-control">
			<g:checkBox name="printCont" />
		</div>
	</div>
</div>

<script>
	$('#startDate_day').addClass('form-control');
	$('#startDate_day').css('width','70px');
	$('#startDate_day').css('float','left');
	$('#startDate_month').addClass('form-control');
	$('#startDate_month').css('width','200px');
	$('#startDate_month').css('float','left');
	$('#startDate_year').addClass('form-control');
	$('#startDate_year').css('width','100px');
	$('#startDate_year').css('float','left');

	$('#endDate_day').addClass('form-control');
	$('#endDate_day').css('width','70px');
	$('#endDate_day').css('float','left');
	$('#endDate_month').addClass('form-control');
	$('#endDate_month').css('width','200px');
	$('#endDate_month').css('float','left');
	$('#endDate_year').addClass('form-control');
	$('#endDate_year').css('width','100px');
	$('#endDate_year').css('float','left');

	$('input:checkbox').iCheck({
		checkboxClass : 'icheckbox_minimal-grey',
		radioClass : 'iradio_minimal-grey'
	});

	function updateRange(value) {
		var sdate = new Date($('#startDate_year').val(),$('#startDate_month').val()-1,$('#startDate_day').val());
		var edate = sdate.getTime() + 1000*60*60*24*30*parseInt(value);
		sdate.setTime(edate);
		$('#endDate_day').val(sdate.getDate());
		$('#endDate_month').val(sdate.getMonth()+1);
		$('#endDate_year').val(sdate.getFullYear());
	}
	
</script>

