

<%@ page import="com.osem.Contract"%>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<g:set var="entityName"
	value="${message(code: 'contract.label', default: 'Contract')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
</head>
<body>
	<div class="col-md-2 bootstrap-admin-col-left">
		<ul class="nav navbar-collapse collapse bootstrap-admin-navbar-side">
			<li><g:link action="index">
					<i class="glyphicon glyphicon-list-alt"></i>
					<g:message code="default.list.label" args="[entityName]" />
				</g:link></li>
			<li><g:link action="create">
					<i class="glyphicon glyphicon-plus"></i>
					<g:message code="default.new.label" args="[entityName]" />
				</g:link></li>
		</ul>
	</div>

	<div class="col-md-10">
		<div class="row">
			<div class="col-lg-12">
				<div class="page-header">
					<h1>
						<g:message code="default.show.label" args="[entityName]" />
					</h1>
				</div>
				<g:if test="${flash.message}">
					<div class="page-body">
						<div class="alert alert-success" role="status">
							<a class="close" data-dismiss="alert" href="#">&times;</a>
							${flash.message}
						</div>
					</div>
				</g:if>
				<g:hasErrors bean="${contractInstance}">
					<div class="page-body">
						<ul class="alert alert-danger" role="alert">
							<g:eachError bean="${contractInstance}" var="error">
								<li
									<g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message
										error="${error}" /></li>
							</g:eachError>
						</ul>
					</div>
				</g:hasErrors>
			</div>
		</div>

		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default bootstrap-admin-no-table-panel">
					<div
						class="bootstrap-admin-no-table-panel-content bootstrap-admin-panel-content collapse in">
						<dl class="dl-horizontal">

							<g:if test="${contractInstance?.person}">
								<dt>
									<g:message code="contract.person.label" default="Karyawan" />
								</dt>

								<dd>
									<g:link controller="person" action="show"
										id="${contractInstance?.person?.id}">
										${contractInstance?.person?.encodeAsHTML()}
									</g:link>
								</dd>

							</g:if>

							<g:if test="${contractInstance?.contractLength}">
								<dt>
									<g:message code="contract.contractLength.label"
										default="Lama Kontrak" />
								</dt>

								<dd>
									<g:fieldValue bean="${contractInstance}" field="contractLength" /> Bulan
								</dd>

							</g:if>							
							
							<g:if test="${contractInstance?.startDate}">
								<dt>
									<g:message code="contract.startDate.label" default="Tgl Mulai" />
								</dt>

								<dd>
									<g:formatDate date="${contractInstance?.startDate}" format="dd-MMM-yyyy" />
								</dd>

							</g:if>

							<g:if test="${contractInstance?.endDate}">
								<dt>
									<g:message code="contract.endDate.label" default="Tgl Berakhir" />
								</dt>

								<dd>
									<g:formatDate date="${contractInstance?.endDate}" format="dd-MMM-yyyy" />
								</dd>

							</g:if>
							
							
							<g:if test="${contractInstance?.contractNotes}">
								<dt>
									<g:message code="contract.contractNotes.label"
										default="Keterangan" />
								</dt>

								<dd>
									<g:fieldValue bean="${contractInstance}" field="contractNotes" />
								</dd>

							</g:if>


						</dl>
						<g:form url="[resource:contractInstance, action:'delete']"
							method="DELETE">
							<fieldset class="buttons">
								<g:link class="btn btn-primary" action="edit"
									resource="${contractInstance}">
									<g:message code="default.button.edit.label" default="Edit" />
								</g:link>
								<g:actionSubmit class="btn btn-default" action="delete"
									value="${message(code: 'default.button.delete.label', default: 'Delete')}"
									onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
							</fieldset>
						</g:form>
					</div>
				</div>
			</div>
		</div>

	</div>
</body>
</html>
