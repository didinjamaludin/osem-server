

<%@ page import="com.osem.Contract"%>
<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="main">
        <g:set var="entityName"
        value="${message(code: 'contract.label', default: 'Contract')}" />
        <title><g:message code="default.list.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="col-md-2 bootstrap-admin-col-left">
            <ul class="nav navbar-collapse collapse bootstrap-admin-navbar-side">
                <li><g:link action="create">
                        <i class="glyphicon glyphicon-plus"></i>
                        <g:message code="default.new.label" args="[entityName]" />
                    </g:link></li>
            </ul>
        </div>

        <div class="col-md-10">
            <div class="row">
                <div class="col-lg-12">
                    <div class="page-header">
                        <h1>
                            <g:message code="default.list.label" args="[entityName]" />
                        </h1>
                    </div>
                    <g:if test="${flash.message}">
                        <div class="page-body">
                            <div class="alert alert-success" role="status">
                                <a class="close" data-dismiss="alert" href="#">&times;</a>
                                ${flash.message}
                            </div>
                        </div>
                    </g:if>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="bootstrap-admin-panel-content">
                            <g:if test="${contractInstanceList}">
                                <table class="table bootstrap-admin-table-with-actions">
                                    <thead>
                                        <tr>

                                            <g:sortableColumn property="person"
                                            title="${message(code: 'contract.person.label', default: 'Karyawan')}" />

                                            <g:sortableColumn property="contractLength"
                                            title="${message(code: 'contract.contractLength.label', default: 'Lama Kontrak')}" style="text-align:center" />

                                            <g:sortableColumn property="startDate"
                                            title="${message(code: 'contract.startDate.label', default: 'Tgl Mulai')}" style="text-align:center" />

                                            <g:sortableColumn property="endDate"
                                            title="${message(code: 'contract.endDate.label', default: 'Tgl Berakhir')}" style="text-align:center" />
                                            
                                            <th>Action</th>

                                        </tr>
                                    </thead>
                                    <tbody>
                                        <g:each in="${contractInstanceList}" status="i"
                                            var="contractInstance">
                                            <tr class="${(i % 2) == 0 ? 'even' : 'odd'}">

                                                <td>
                                                    <g:link action="show" id="${contractInstance.id}">${fieldValue(bean: contractInstance, field: "person")}</g:link>
                                                    </td>

                                                <td style="text-align:center">
                                                    ${fieldValue(bean: contractInstance, field: "contractLength")} bulan
                                                </td>

                                                <td style="text-align:center"><g:formatDate date="${contractInstance.startDate}"
                                                        format="dd-MMM-yyyy" /></td>

                                                <td style="text-align:center"><g:formatDate date="${contractInstance.endDate}"
                                                        format="dd-MMM-yyyy" /></td>
                                                
                                                <td><g:link class="btn btn-primary" style="padding:2px 10px" action="printContract" id="${contractInstance?.id}">Print</g:link></td>
                                            </tr>
                                        </g:each>
                                    </tbody>
                                </table>
                                <div class="pagination">
                                    <g:paginate total="${contractInstanceCount ?: 0}" />
                                </div>
                            </g:if>
                            <g:else>
                                No contract found
                            </g:else>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
