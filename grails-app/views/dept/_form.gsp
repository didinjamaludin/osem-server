<%@ page import="com.osem.Dept" %>



<div class="form-group fieldcontain ${hasErrors(bean: deptInstance, field: 'deptName', 'error')} ">
	<label class="col-lg-2 control-label" for="deptName">
		<g:message code="dept.deptName.label" default="Dept Name" />
		
	</label>
	<div class="col-lg-10"><g:textField class="form-control" name="deptName" value="${deptInstance?.deptName}" /></div>

</div>

