

<%@ page import="com.osem.Mutation" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<g:set var="entityName"
	value="${message(code: 'mutation.label', default: 'Mutation')}" />
<title><g:message code="default.list.label" args="[entityName]" /></title>
</head>
<body>
	<div class="col-md-2 bootstrap-admin-col-left">
		<ul class="nav navbar-collapse collapse bootstrap-admin-navbar-side">
			<li><g:link action="create">
					<i class="glyphicon glyphicon-plus"></i>
					<g:message code="default.new.label" args="[entityName]" />
				</g:link></li>
		</ul>
	</div>

	<div class="col-md-10">
		<div class="row">
			<div class="col-lg-12">
				<div class="page-header">
					<h1>
						<g:message code="default.list.label" args="[entityName]" />
					</h1>
				</div>
				<g:if test="${flash.message}">
					<div class="page-body">
						<div class="alert alert-success" role="status">
							<a class="close" data-dismiss="alert" href="#">&times;</a>${flash.message}
						</div>
					</div>
				</g:if>
			</div>
		</div>

		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="bootstrap-admin-panel-content">
						<g:if test="${mutationInstanceList}">
							<table class="table bootstrap-admin-table-with-actions">
								<thead>
									<tr>
										
						<g:sortableColumn property="createDate" title="${message(code: 'mutation.createDate.label', default: 'Create Date')}" />
					
						<th><g:message code="mutation.fromSubUnit.label" default="From Sub Unit" /></th>
					
						<g:sortableColumn property="mutCode" title="${message(code: 'mutation.mutCode.label', default: 'Mut Code')}" />
					
						<g:sortableColumn property="mutDate" title="${message(code: 'mutation.mutDate.label', default: 'Mut Date')}" />
					
						<g:sortableColumn property="mutNotes" title="${message(code: 'mutation.mutNotes.label', default: 'Mut Notes')}" />
					
						<g:sortableColumn property="newDept" title="${message(code: 'mutation.newDept.label', default: 'New Dept')}" />
					
									</tr>
								</thead>
								<tbody>
									<g:each in="${mutationInstanceList}" status="i"
										var="mutationInstance">
										<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
											
						<td><g:link action="show" id="${mutationInstance.id}">${fieldValue(bean: mutationInstance, field: "createDate")}</g:link></td>
					
						<td>${fieldValue(bean: mutationInstance, field: "fromSubUnit")}</td>
					
						<td>${fieldValue(bean: mutationInstance, field: "mutCode")}</td>
					
						<td><g:formatDate date="${mutationInstance.mutDate}" /></td>
					
						<td>${fieldValue(bean: mutationInstance, field: "mutNotes")}</td>
					
						<td>${fieldValue(bean: mutationInstance, field: "newDept")}</td>
					
										</tr>
									</g:each>
								</tbody>
							</table>
							<div class="pagination">
								<g:paginate total="${mutationInstanceCount ?: 0}" />
							</div>
						</g:if>
						<g:else>
							No mutation found
						</g:else>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
