

<%@ page import="com.osem.Mutation" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<g:set var="entityName"
	value="${message(code: 'mutation.label', default: 'Mutation')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
</head>
<body>
	<div class="col-md-2 bootstrap-admin-col-left">
		<ul class="nav navbar-collapse collapse bootstrap-admin-navbar-side">
			<li><g:link action="index">
					<i class="glyphicon glyphicon-list-alt"></i>
					<g:message code="default.list.label" args="[entityName]" />
				</g:link></li>
			<li><g:link action="create">
					<i class="glyphicon glyphicon-plus"></i>
					<g:message code="default.new.label" args="[entityName]" />
				</g:link></li>
		</ul>
	</div>

	<div class="col-md-10">
		<div class="row">
			<div class="col-lg-12">
				<div class="page-header">
					<h1>
						<g:message code="default.show.label" args="[entityName]" />
					</h1>
				</div>
				<g:if test="${flash.message}">
					<div class="page-body">
						<div class="alert alert-success" role="status">
							<a class="close" data-dismiss="alert" href="#">&times;</a>${flash.message}
						</div>
					</div>
				</g:if>
				<g:hasErrors bean="${mutationInstance}">
					<div class="page-body">
						<ul class="alert alert-danger" role="alert">
							<g:eachError bean="${mutationInstance}" var="error">
								<li
									<g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message
										error="${error}" /></li>
							</g:eachError>
						</ul>
					</div>
				</g:hasErrors>
			</div>
		</div>

		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default bootstrap-admin-no-table-panel">
					<div
						class="bootstrap-admin-no-table-panel-content bootstrap-admin-panel-content collapse in">
						<dl class="dl-horizontal">
							
				<g:if test="${mutationInstance?.createDate}">
					<dt><g:message code="mutation.createDate.label" default="Create Date" /></dt>
					
						<dd><g:formatDate date="${mutationInstance?.createDate}" /></dd>
					
				</g:if>
			
				<g:if test="${mutationInstance?.fromSubUnit}">
					<dt><g:message code="mutation.fromSubUnit.label" default="From Sub Unit" /></dt>
					
						<dd><g:link controller="subUnit" action="show" id="${mutationInstance?.fromSubUnit?.id}">${mutationInstance?.fromSubUnit?.encodeAsHTML()}</g:link></dd>
					
				</g:if>
			
				<g:if test="${mutationInstance?.mutCode}">
					<dt><g:message code="mutation.mutCode.label" default="Mut Code" /></dt>
					
						<dd><g:fieldValue bean="${mutationInstance}" field="mutCode"/></dd>
					
				</g:if>
			
				<g:if test="${mutationInstance?.mutDate}">
					<dt><g:message code="mutation.mutDate.label" default="Mut Date" /></dt>
					
						<dd><g:formatDate date="${mutationInstance?.mutDate}" /></dd>
					
				</g:if>
			
				<g:if test="${mutationInstance?.mutNotes}">
					<dt><g:message code="mutation.mutNotes.label" default="Mut Notes" /></dt>
					
						<dd><g:fieldValue bean="${mutationInstance}" field="mutNotes"/></dd>
					
				</g:if>
			
				<g:if test="${mutationInstance?.newDept}">
					<dt><g:message code="mutation.newDept.label" default="New Dept" /></dt>
					
						<dd><g:fieldValue bean="${mutationInstance}" field="newDept"/></dd>
					
				</g:if>
			
				<g:if test="${mutationInstance?.oldDept}">
					<dt><g:message code="mutation.oldDept.label" default="Old Dept" /></dt>
					
						<dd><g:fieldValue bean="${mutationInstance}" field="oldDept"/></dd>
					
				</g:if>
			
				<g:if test="${mutationInstance?.person}">
					<dt><g:message code="mutation.person.label" default="Person" /></dt>
					
						<dd><g:link controller="person" action="show" id="${mutationInstance?.person?.id}">${mutationInstance?.person?.encodeAsHTML()}</g:link></dd>
					
				</g:if>
			
				<g:if test="${mutationInstance?.toSubUnit}">
					<dt><g:message code="mutation.toSubUnit.label" default="To Sub Unit" /></dt>
					
						<dd><g:link controller="subUnit" action="show" id="${mutationInstance?.toSubUnit?.id}">${mutationInstance?.toSubUnit?.encodeAsHTML()}</g:link></dd>
					
				</g:if>
			
						</dl>
						<g:form url="[resource:mutationInstance, action:'delete']"
							method="DELETE">
							<fieldset class="buttons">
								<g:link class="btn btn-primary" action="edit"
									resource="${mutationInstance}">
									<g:message code="default.button.edit.label" default="Edit" />
								</g:link>
								<g:actionSubmit class="btn btn-default" action="delete"
									value="${message(code: 'default.button.delete.label', default: 'Delete')}"
									onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
							</fieldset>
						</g:form>
					</div>
				</div>
			</div>
		</div>

	</div>
</body>
</html>
