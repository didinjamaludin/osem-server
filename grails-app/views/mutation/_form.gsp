<%@ page import="com.osem.Mutation" %>



<div class="form-group fieldcontain ${hasErrors(bean: mutationInstance, field: 'createDate', 'error')} ">
	<label class="col-lg-2 control-label" for="createDate">
		<g:message code="mutation.createDate.label" default="Create Date" />
		
	</label>
	<div class="col-lg-10"><g:datePicker class="form-control" name="createDate" precision="day" value="${mutationInstance?.createDate}" /></div>

</div>

<div class="form-group fieldcontain ${hasErrors(bean: mutationInstance, field: 'fromSubUnit', 'error')} ">
	<label class="col-lg-2 control-label" for="fromSubUnit">
		<g:message code="mutation.fromSubUnit.label" default="From Sub Unit" />
		
	</label>
	<div class="col-lg-10"><g:select class="form-control" id="fromSubUnit" name="fromSubUnit.id" from="${com.osem.SubUnit.list()}" optionKey="id" required="" value="${mutationInstance?.fromSubUnit?.id}"/></div>

</div>

<div class="form-group fieldcontain ${hasErrors(bean: mutationInstance, field: 'mutCode', 'error')} ">
	<label class="col-lg-2 control-label" for="mutCode">
		<g:message code="mutation.mutCode.label" default="Mut Code" />
		
	</label>
	<div class="col-lg-10"><g:textField class="form-control" name="mutCode" value="${mutationInstance?.mutCode}" /></div>

</div>

<div class="form-group fieldcontain ${hasErrors(bean: mutationInstance, field: 'mutDate', 'error')} ">
	<label class="col-lg-2 control-label" for="mutDate">
		<g:message code="mutation.mutDate.label" default="Mut Date" />
		
	</label>
	<div class="col-lg-10"><g:datePicker class="form-control" name="mutDate" precision="day" value="${mutationInstance?.mutDate}" /></div>

</div>

<div class="form-group fieldcontain ${hasErrors(bean: mutationInstance, field: 'mutNotes', 'error')} ">
	<label class="col-lg-2 control-label" for="mutNotes">
		<g:message code="mutation.mutNotes.label" default="Mut Notes" />
		
	</label>
	<div class="col-lg-10"><g:textField class="form-control" name="mutNotes" value="${mutationInstance?.mutNotes}" /></div>

</div>

<div class="form-group fieldcontain ${hasErrors(bean: mutationInstance, field: 'newDept', 'error')} ">
	<label class="col-lg-2 control-label" for="newDept">
		<g:message code="mutation.newDept.label" default="New Dept" />
		
	</label>
	<div class="col-lg-10"><g:textField class="form-control" name="newDept" value="${mutationInstance?.newDept}" /></div>

</div>

<div class="form-group fieldcontain ${hasErrors(bean: mutationInstance, field: 'oldDept', 'error')} ">
	<label class="col-lg-2 control-label" for="oldDept">
		<g:message code="mutation.oldDept.label" default="Old Dept" />
		
	</label>
	<div class="col-lg-10"><g:textField class="form-control" name="oldDept" value="${mutationInstance?.oldDept}" /></div>

</div>

<div class="form-group fieldcontain ${hasErrors(bean: mutationInstance, field: 'person', 'error')} ">
	<label class="col-lg-2 control-label" for="person">
		<g:message code="mutation.person.label" default="Person" />
		
	</label>
	<div class="col-lg-10"><g:select class="form-control" id="person" name="person.id" from="${com.osem.Person.list()}" optionKey="id" required="" value="${mutationInstance?.person?.id}"/></div>

</div>

<div class="form-group fieldcontain ${hasErrors(bean: mutationInstance, field: 'toSubUnit', 'error')} ">
	<label class="col-lg-2 control-label" for="toSubUnit">
		<g:message code="mutation.toSubUnit.label" default="To Sub Unit" />
		
	</label>
	<div class="col-lg-10"><g:select class="form-control" id="toSubUnit" name="toSubUnit.id" from="${com.osem.SubUnit.list()}" optionKey="id" required="" value="${mutationInstance?.toSubUnit?.id}"/></div>

</div>

