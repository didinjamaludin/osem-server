

<%@ page import="com.osem.Person"%>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<g:set var="entityName"
	value="${message(code: 'person.label', default: 'Person')}" />
<title><g:message code="default.list.label" args="[entityName]" /></title>
</head>
<body>
	<div class="col-md-2 bootstrap-admin-col-left">
		<ul class="nav navbar-collapse collapse bootstrap-admin-navbar-side">
			<li><g:link action="create">
					<i class="glyphicon glyphicon-plus"></i>
					<g:message code="default.new.label" args="[entityName]" />
				</g:link></li>
		</ul>
	</div>

	<div class="col-md-10">
		<div class="row">
			<div class="col-lg-12">
				<div class="page-header">
					<h1>
						<g:message code="default.list.label" args="[entityName]" />
					</h1>
				</div>
				<g:if test="${flash.message}">
					<div class="page-body">
						<div class="alert alert-success" role="status">
							<a class="close" data-dismiss="alert" href="#">&times;</a>
							${flash.message}
						</div>
					</div>
				</g:if>
			</div>
		</div>

		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="bootstrap-admin-panel-content">
						<g:if test="${personInstanceList}">
							<table class="table bootstrap-admin-table-with-actions">
								<thead>
									<tr>

										<th><g:message code="subUnit.unit.label" default="Unit" /></th>

										<g:sortableColumn property="subUnit"
											title="${message(code: 'person.subUnit.label', default: 'Sub Unit')}" />

										<g:sortableColumn property="pid"
											title="${message(code: 'person.pid.label', default: 'NIK')}" />

										<g:sortableColumn property="fullname"
											title="${message(code: 'person.fullname.label', default: 'Nama Lengkap')}" />

										<g:sortableColumn property="position"
											title="${message(code: 'person.position.label', default: 'Posisi')}" />

										<g:sortableColumn property="empStatus"
											title="${message(code: 'person.empStatus.label', default: 'Status')}" />

									</tr>
								</thead>
								<tbody>
									<g:each in="${personInstanceList}" status="i"
										var="personInstance">
										<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">

											<td>
												${com.osem.Unit.get(personInstance?.subUnit?.unit?.id)}
											</td>

											<td>
												${fieldValue(bean: personInstance, field: "subUnit")}
											</td>

											<td><g:link action="show" id="${personInstance.id}">
													${fieldValue(bean: personInstance, field: "pid")}
												</g:link></td>

											<td><g:link action="show" id="${personInstance.id}">
													${fieldValue(bean: personInstance, field: "fullname")}
												</g:link></td>

											<td>
												${fieldValue(bean: personInstance, field: "position")}
											</td>

											<td>
												${fieldValue(bean: personInstance, field: "empStatus")}
											</td>

										</tr>
									</g:each>
								</tbody>
							</table>
							<div class="pagination">
								<g:paginate total="${personInstanceCount ?: 0}" />
							</div>
						</g:if>
						<g:else>
							No person found
						</g:else>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
