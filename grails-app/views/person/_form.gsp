<%@ page import="com.osem.Person"%>

<div
	class="form-group fieldcontain ${hasErrors(bean: personInstance, field: 'company', 'error')} "
	style="display: none">
	<label class="col-lg-2 control-label" for="company"> <g:message
			code="person.company.label" default="Company" />

	</label>
	<div class="col-lg-10">
		<g:if test="${params.action.equals('create') }">
			<g:select class="form-control" id="company" name="company.id"
				from="${com.osem.Company.list()}" optionKey="id" required=""
				value="${company?.id}" />
		</g:if>
		<g:else>
			<g:select class="form-control" id="company" name="company.id"
				from="${com.osem.Company.list()}" optionKey="id" required=""
				value="${personInstance?.company?.id}" />
		</g:else>
	</div>

</div>

<div
	class="form-group fieldcontain ${hasErrors(bean: personInstance, field: 'fullname', 'error')} ">
	<label class="col-lg-3 control-label" for="fullname"> <g:message
			code="person.fullname.label" default="Nama Lengkap" />

	</label>
	<div class="col-lg-9">
		<g:textField class="form-control" name="fullname"
			value="${personInstance?.fullname}"
			placeholder="Nama Lengkap Sesuai Kartu Identitas" />
	</div>

</div>


<div
	class="form-group fieldcontain ${hasErrors(bean: personInstance, field: 'gender', 'error')} ">
	<label class="col-lg-3 control-label" for="gender"> <g:message
			code="person.gender.label" default="Jenis Kelamin" />

	</label>
	<div class="col-lg-9">
		<div class="form-control">
			<g:radioGroup name="gender" values="['Laki-laki','Perempuan']"
				value="${personInstance?.gender}" labels="['Laki-laki','Perempuan']">
				${it.radio}&nbsp;<g:message code="${it.label}" />&nbsp;&nbsp;
		</g:radioGroup>
		</div>
	</div>

</div>

<div class="form-group">
	<div
		class="fieldcontain ${hasErrors(bean: personInstance, field: 'pob', 'error')} ">
		<label class="col-lg-3 control-label" for="pob"> <g:message
				code="person.pob.label" default="Tempat/Tgl Lahir" />

		</label>
		<div class="col-lg-4">
			<g:textField class="form-control" name="pob"
				value="${personInstance?.pob}" placeholder="Kota Tempat Lahir" />
		</div>

	</div>

	<div
		class="fieldcontain ${hasErrors(bean: personInstance, field: 'dob', 'error')} ">
		<div class="col-lg-5">
			<div class="form-control">
				<g:datePicker name="dob" precision="day"
					value="${personInstance?.dob}" />
			</div>
		</div>

	</div>
</div>

<div class="form-group">
	<div
		class="fieldcontain ${hasErrors(bean: personInstance, field: 'idAddr', 'error')} ">
		<label class="col-lg-3 control-label" for="idAddr"> <g:message
				code="person.idAddr.label" default="Alamat di Identitas" />

		</label>
		<div class="col-lg-6">
			<g:textField class="form-control" name="idAddr"
				value="${personInstance?.idAddr}" placeholder="Alamat di Identitas" />
		</div>

	</div>

	<div
		class="fieldcontain ${hasErrors(bean: personInstance, field: 'idCity', 'error')} ">
		<div class="col-lg-3">
			<g:textField class="form-control" name="idCity"
				value="${personInstance?.idCity}" placeholder="Kota di Identitas" />
		</div>

	</div>
</div>
<div class="form-group">
	<div
		class="fieldcontain ${hasErrors(bean: personInstance, field: 'currentAddr', 'error')} ">
		<label class="col-lg-3 control-label" for="currentAddr"> <g:message
				code="person.currentAddr.label" default="Alamat Sekarang" />

		</label>
		<div class="col-lg-6">
			<g:textField class="form-control" name="currentAddr"
				value="${personInstance?.currentAddr}" placeholder="Alamat Sekarang" />
		</div>

	</div>

	<div
		class="fieldcontain ${hasErrors(bean: personInstance, field: 'currentCity', 'error')} ">
		<div class="col-lg-3">
			<g:textField class="form-control" name="currentCity"
				value="${personInstance?.currentCity}" placeholder="Kota Sekarang" />
		</div>

	</div>
</div>

<div class="form-group">
	<div
		class="fieldcontain ${hasErrors(bean: personInstance, field: 'postCode', 'error')} ">
		<label class="col-lg-3 control-label" for="postCode"> <g:message
				code="person.postCode.label" default="Kode POS/Telp/HP" />

		</label>
		<div class="col-lg-3">
			<g:textField class="form-control" name="postCode"
				value="${personInstance?.postCode}" placeholder="Kode POS" />
		</div>

	</div>

	<div
		class="fieldcontain ${hasErrors(bean: personInstance, field: 'phone', 'error')} ">
		<div class="col-lg-3">
			<g:textField class="form-control" name="phone"
				value="${personInstance?.phone}" placeholder="Telepon" />
		</div>

	</div>

	<div
		class="fieldcontain ${hasErrors(bean: personInstance, field: 'hp', 'error')} ">
		<div class="col-lg-3">
			<g:textField class="form-control" name="hp"
				value="${personInstance?.hp}" placeholder="HP" />
		</div>

	</div>
</div>

<div
	class="form-group fieldcontain ${hasErrors(bean: personInstance, field: 'email', 'error')} ">
	<label class="col-lg-3 control-label" for="email"> <g:message
			code="person.email.label" default="Email" />

	</label>
	<div class="col-lg-9">
		<g:field type="email" class="form-control" name="email"
			value="${personInstance?.email}" placeholder="alamat@domain.com" />
	</div>

</div>

<div class="form-group">
	<div
		class="fieldcontain ${hasErrors(bean: personInstance, field: 'idType', 'error')} ">
		<label class="col-lg-3 control-label" for="idType"> <g:message
				code="person.idType.label" default="Identitas" />

		</label>
		<div class="col-lg-4">
			<g:select from="${['KTP','SIM','PASSPORT']}" class="form-control"
				name="idType" value="${personInstance?.idType}" />
		</div>

	</div>

	<div
		class="fieldcontain ${hasErrors(bean: personInstance, field: 'idNumber', 'error')} ">
		<div class="col-lg-5">
			<g:textField class="form-control" name="idNumber"
				value="${personInstance?.idNumber}" placeholder="Nomor Identitas" />
		</div>

	</div>
</div>

<div class="form-group">
	<div
		class="fieldcontain ${hasErrors(bean: personInstance, field: 'bloodGroup', 'error')} ">
		<label class="col-lg-3 control-label" for="bloodGroup"> <g:message
				code="person.bloodGroup.label" default="Gol Darah/Tinggi/Berat" />

		</label>
		<div class="col-lg-3">
			<g:select from="${['A','B','O','AB']}" class="form-control"
				name="bloodGroup" value="${personInstance?.bloodGroup}" />
		</div>

	</div>



	<div
		class="fieldcontain ${hasErrors(bean: personInstance, field: 'height', 'error')} ">
		<div class="col-lg-3">
			<g:field class="form-control" type="number" name="height"
				value="${personInstance.height}" placeholder="Tinggi" />
		</div>

	</div>

	<div
		class="fieldcontain ${hasErrors(bean: personInstance, field: 'weight', 'error')} ">
		<div class="col-lg-3">
			<g:field class="form-control" type="number" name="weight"
				value="${personInstance.weight}" placeholder="Berat" />
		</div>

	</div>
</div>

<div class="form-group">
	<div
		class="fieldcontain ${hasErrors(bean: personInstance, field: 'nationality', 'error')} ">
		<label class="col-lg-3 control-label" for="nationality"> <g:message
				code="person.nationality.label" default="Kebangsaan/Suku/Agama" />

		</label>
		<div class="col-lg-3">
			<g:textField class="form-control" name="nationality"
				value="${personInstance?.nationality}" placeholder="Kebangsaan" />
		</div>

	</div>

	<div
		class="fieldcontain ${hasErrors(bean: personInstance, field: 'race', 'error')} ">
		<div class="col-lg-3">
			<g:textField class="form-control" name="race"
				value="${personInstance?.race}" placeholder="Suku/Ras" />
		</div>

	</div>

	<div
		class="fieldcontain ${hasErrors(bean: personInstance, field: 'religion', 'error')} ">
		<div class="col-lg-3">
			<g:select
				from="${['ISLAM','KRISTEN','KATOLIK','HINDU','BUDHA','KONGHUCU']}"
				class="form-control" name="religion"
				value="${personInstance?.religion}" />
		</div>

	</div>
</div>

<div
	class="form-group fieldcontain ${hasErrors(bean: personInstance, field: 'maritalStatus', 'error')} ">
	<label class="col-lg-3 control-label" for="maritalStatus"> <g:message
			code="person.maritalStatus.label" default="Status Perkawinan" />

	</label>
	<div class="col-lg-9">
		<div class="form-control">
			<g:radioGroup name="maritalStatus" values="['TK','K0','K1','K2','K3','K4']"
				value="${personInstance?.maritalStatus}" labels="['TK','K0','K1','K2','K3','K4']">
				${it.radio}&nbsp;<g:message code="${it.label}" />&nbsp;&nbsp;
		</g:radioGroup>
		</div>
	</div>

</div>

<div
	class="form-group fieldcontain ${hasErrors(bean: personInstance, field: 'empStatus', 'error')} " style="display:none">
	<label class="col-lg-3 control-label" for="empStatus"> <g:message
			code="person.empStatus.label" default="Status Kepegawaian" />

	</label>
	<div class="col-lg-9">
		<g:if test="${params?.action?.equals('create')}">
			<g:textField name="empStatus" value="PELAMAR" />
		</g:if>
		<g:else>
			<g:textField name="empStatus" value="${personInstance?.empStatus}" />
		</g:else>
	</div>

</div>

<script>
	$('input:radio').iCheck({
		checkboxClass : 'icheckbox_minimal-grey',
		radioClass : 'iradio_minimal-grey'
	});
</script>
