

<%@ page import="com.osem.Person"%>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<g:set var="entityName"
	value="${message(code: 'person.label', default: 'Person')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
</head>
<body>
	<div class="col-md-2 bootstrap-admin-col-left">
		<ul class="nav navbar-collapse collapse bootstrap-admin-navbar-side">
			<li><g:link action="index">
					<i class="glyphicon glyphicon-list-alt"></i>
					<g:message code="default.list.label" args="[entityName]" />
				</g:link></li>
			<li><g:link action="create">
					<i class="glyphicon glyphicon-plus"></i>
					<g:message code="default.new.label" args="[entityName]" />
				</g:link></li>
		</ul>
	</div>

	<div class="col-md-10">
		<div class="row">
			<div class="col-lg-12">
				<div class="page-header">
					<h1>
						<g:message code="default.show.label" args="[entityName]" />
					</h1>
				</div>
				<g:if test="${flash.message}">
					<div class="page-body">
						<div class="alert alert-success" role="status">
							<a class="close" data-dismiss="alert" href="#">&times;</a>
							${flash.message}
						</div>
					</div>
				</g:if>
				<g:hasErrors bean="${personInstance}">
					<div class="page-body">
						<ul class="alert alert-danger" role="alert">
							<g:eachError bean="${personInstance}" var="error">
								<li
									<g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message
										error="${error}" /></li>
							</g:eachError>
						</ul>
					</div>
				</g:hasErrors>
			</div>
		</div>

		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default bootstrap-admin-no-table-panel">
					<div
						class="bootstrap-admin-no-table-panel-content bootstrap-admin-panel-content collapse in">
						<dl class="dl-horizontal">

							<g:if test="${personInstance?.subUnit}">
								<dt>
									<g:message code="subUnit.unit.label" default="Unit" />
								</dt>

								<dd>
									<g:link controller="unit" action="show"
										id="${personInstance?.subUnit?.unit?.id}">
										${personInstance?.subUnit?.unit?.encodeAsHTML()}
									</g:link>
								</dd>

							</g:if>

							<g:if test="${personInstance?.subUnit}">
								<dt>
									<g:message code="person.subUnit.label" default="Sub Unit" />
								</dt>

								<dd>
									<g:link controller="subUnit" action="show"
										id="${personInstance?.subUnit?.id}">
										${personInstance?.subUnit?.encodeAsHTML()}
									</g:link>
								</dd>

							</g:if>
						</dl>
						<hr>
						<dl class="dl-horizontal">
							<g:if test="${personInstance?.pid}">
								<dt>
									<g:message code="person.pid.label" default="NIK" />
								</dt>

								<dd>
									<g:fieldValue bean="${personInstance}" field="pid" />
								</dd>

							</g:if>

							<g:if test="${personInstance?.fullname}">
								<dt>
									<g:message code="person.fullname.label" default="Nama Lengkap" />
								</dt>

								<dd>
									<g:fieldValue bean="${personInstance}" field="fullname" />
								</dd>

							</g:if>

							<g:if test="${personInstance?.position}">
								<dt>
									<g:message code="person.position.label" default="Position" />
								</dt>

								<dd>
									<g:link controller="position" action="show"
										id="${personInstance?.position?.id}">
										${personInstance?.position?.encodeAsHTML()}
									</g:link>
								</dd>

							</g:if>

							<g:if test="${personInstance?.dept}">
								<dt>
									<g:message code="person.dept.label" default="Dept" />
								</dt>

								<dd>
									<g:link controller="dept" action="show"
										id="${personInstance?.dept?.id}">
										${personInstance?.dept?.encodeAsHTML()}
									</g:link>
								</dd>

							</g:if>
						</dl>
						<hr>
						<dl class="dl-horizontal">
							<g:if test="${personInstance?.idAddr}">
								<dt>
									<g:message code="person.idAddr.label"
										default="Alamat di Identitas" />
								</dt>

								<dd>
									<g:fieldValue bean="${personInstance}" field="idAddr" />
								</dd>

							</g:if>

							<g:if test="${personInstance?.idCity}">
								<dt>
									<g:message code="person.idCity.label"
										default="Kota di Identitas" />
								</dt>

								<dd>
									<g:fieldValue bean="${personInstance}" field="idCity" />
								</dd>

							</g:if>

							<g:if test="${personInstance?.currentAddr}">
								<dt>
									<g:message code="person.currentAddr.label"
										default="Alamat Sekarang" />
								</dt>

								<dd>
									<g:fieldValue bean="${personInstance}" field="currentAddr" />
								</dd>

							</g:if>

							<g:if test="${personInstance?.currentCity}">
								<dt>
									<g:message code="person.currentCity.label"
										default="Kota Sekarang" />
								</dt>

								<dd>
									<g:fieldValue bean="${personInstance}" field="currentCity" />
								</dd>

							</g:if>

							<g:if test="${personInstance?.postCode}">
								<dt>
									<g:message code="person.postCode.label" default="Kode Pos" />
								</dt>

								<dd>
									<g:fieldValue bean="${personInstance}" field="postCode" />
								</dd>

							</g:if>

							<g:if test="${personInstance?.phone}">
								<dt>
									<g:message code="person.phone.label" default="Telepon" />
								</dt>

								<dd>
									<g:fieldValue bean="${personInstance}" field="phone" />
								</dd>

							</g:if>

							<g:if test="${personInstance?.hp}">
								<dt>
									<g:message code="person.hp.label" default="HP" />
								</dt>

								<dd>
									<g:fieldValue bean="${personInstance}" field="hp" />
								</dd>

							</g:if>

							<g:if test="${personInstance?.email}">
								<dt>
									<g:message code="person.email.label" default="Email" />
								</dt>

								<dd>
									<g:fieldValue bean="${personInstance}" field="email" />
								</dd>

							</g:if>

							<g:if test="${personInstance?.idType}">
								<dt>
									<g:message code="person.idType.label" default="Jenis Identitas" />
								</dt>

								<dd>
									<g:fieldValue bean="${personInstance}" field="idType" />
								</dd>

							</g:if>

							<g:if test="${personInstance?.idNumber}">
								<dt>
									<g:message code="person.idNumber.label"
										default="Nomor Identitas" />
								</dt>

								<dd>
									<g:fieldValue bean="${personInstance}" field="idNumber" />
								</dd>

							</g:if>

							<g:if test="${personInstance?.bloodGroup}">
								<dt>
									<g:message code="person.bloodGroup.label" default="Gol Darah" />
								</dt>

								<dd>
									<g:fieldValue bean="${personInstance}" field="bloodGroup" />
								</dd>

							</g:if>

							<g:if test="${personInstance?.pob}">
								<dt>
									<g:message code="person.pob.label" default="Tempat Lahir" />
								</dt>

								<dd>
									<g:fieldValue bean="${personInstance}" field="pob" />
								</dd>

							</g:if>

							<g:if test="${personInstance?.dob}">
								<dt>
									<g:message code="person.dob.label" default="Tanggal Lahir" />
								</dt>

								<dd>
									<g:formatDate date="${personInstance?.dob}" format="dd/MM/yyyy" />
								</dd>

							</g:if>

							<g:if test="${personInstance?.height}">
								<dt>
									<g:message code="person.height.label" default="Tinggi" />
								</dt>

								<dd>
									<g:fieldValue bean="${personInstance}" field="height" />
								</dd>

							</g:if>

							<g:if test="${personInstance?.weight}">
								<dt>
									<g:message code="person.weight.label" default="Berat" />
								</dt>

								<dd>
									<g:fieldValue bean="${personInstance}" field="weight" />
								</dd>

							</g:if>

							<g:if test="${personInstance?.nationality}">
								<dt>
									<g:message code="person.nationality.label"
										default="Kewarganegaraan" />
								</dt>

								<dd>
									<g:fieldValue bean="${personInstance}" field="nationality" />
								</dd>

							</g:if>

							<g:if test="${personInstance?.race}">
								<dt>
									<g:message code="person.race.label" default="Suku/Ras" />
								</dt>

								<dd>
									<g:fieldValue bean="${personInstance}" field="race" />
								</dd>

							</g:if>

							<g:if test="${personInstance?.religion}">
								<dt>
									<g:message code="person.religion.label" default="Agama" />
								</dt>

								<dd>
									<g:fieldValue bean="${personInstance}" field="religion" />
								</dd>

							</g:if>

							<g:if test="${personInstance?.gender}">
								<dt>
									<g:message code="person.gender.label" default="Jenis Kelamin" />
								</dt>

								<dd>
									<g:fieldValue bean="${personInstance}" field="gender" />
								</dd>

							</g:if>

							<g:if test="${personInstance?.maritalStatus}">
								<dt>
									<g:message code="person.maritalStatus.label"
										default="Status Pernikahan" />
								</dt>

								<dd>
									<g:fieldValue bean="${personInstance}" field="maritalStatus" />
								</dd>

							</g:if>
						</dl>
						<hr>
						<dl class="dl-horizontal">
							<g:if test="${personInstance?.empStatus}">
								<dt>
									<g:message code="person.empStatus.label"
										default="Status Kepegawaian" />
								</dt>

								<dd>
									<g:fieldValue bean="${personInstance}" field="empStatus" />
								</dd>

							</g:if>
						</dl>
						<hr>
						<dl class="dl-horizontal">
							<dt>
								<g:message code="person.languages.label" default="Pendidikan" />
							</dt>
							<dd>
								<g:link controller="education" action="create"
									params="['person.id': personInstance?.id]">
									<i class="glyphicon glyphicon-plus-sign"></i> Riwayat Pendidikan</g:link>
							</dd>

							<g:if test="${personInstance?.educations}">
								<dt>&nbsp;</dt>

								<g:each in="${personInstance.educations}" var="e">
									<dd>
										<g:link controller="education" action="show" id="${e.id}">
											<i class="glyphicon glyphicon-ok"></i>
											${e?.encodeAsHTML()}
										</g:link>
									</dd>
								</g:each>

							</g:if>

							<dt>
								<g:message code="person.languages.label" default="Languages" />
							</dt>
							<dd>
								<g:link controller="language" action="create"
									params="['person.id': personInstance?.id]">
									<i class="glyphicon glyphicon-plus-sign"></i> Penguasaan Bahasa</g:link>
							</dd>

							<g:if test="${personInstance?.languages}">
								<dt>&nbsp;</dt>

								<g:each in="${personInstance.languages}" var="l">
									<dd>
										<g:link controller="language" action="show" id="${l.id}">
											<i class="glyphicon glyphicon-ok"></i>
											${l?.encodeAsHTML()}
										</g:link>
									</dd>
								</g:each>

							</g:if>

							<dt>
								<g:message code="person.families.label" default="Keluarga" />
							</dt>
							<dd>
								<g:link controller="family" action="create"
									params="['person.id': personInstance?.id]">
									<i class="glyphicon glyphicon-plus-sign"></i> Anggota Keluarga</g:link>
							</dd>

							<g:if test="${personInstance?.families}">
								<dt>&nbsp;</dt>

								<g:each in="${personInstance.families}" var="f">
									<dd>
										<g:link controller="family" action="show" id="${f.id}">
											<i class="glyphicon glyphicon-ok"></i>
											${f?.encodeAsHTML()}
										</g:link>
									</dd>
								</g:each>

							</g:if>

							<dt>
								<g:message code="person.workHistories.label" default="Pekerjaan" />
							</dt>
							<dd>
								<g:link controller="workHistory" action="create"
									params="['person.id': personInstance?.id]">
									<i class="glyphicon glyphicon-plus-sign"></i> Riwayat Pekerjaan</g:link>
							</dd>

							<g:if test="${personInstance?.workHistories}">
								<dt>&nbsp;</dt>

								<g:each in="${personInstance.workHistories}" var="w">
									<dd>
										<g:link controller="workHistory" action="show" id="${w.id}">
											<i class="glyphicon glyphicon-ok"></i>
											${w?.encodeAsHTML()}
										</g:link>
									</dd>
								</g:each>

							</g:if>

						</dl>
						<g:form url="[resource:personInstance, action:'delete']"
							method="DELETE">
							<fieldset class="buttons">
								<g:link class="btn btn-primary" action="edit"
									resource="${personInstance}">
									<g:message code="default.button.edit.label" default="Edit" />
								</g:link>
								<g:actionSubmit class="btn btn-default" action="delete"
									value="${message(code: 'default.button.delete.label', default: 'Delete')}"
									onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
							</fieldset>
						</g:form>
					</div>
				</div>
			</div>
		</div>

	</div>
</body>
</html>
