<!DOCTYPE html>
<!--[if lt IE 7 ]> <html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>OSEM-Server</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="shortcut icon" href="${assetPath(src: 'favicon.ico')}"
	type="image/x-icon">
<asset:stylesheet src="login.css" />
<asset:javascript src="login.js" />
<style type="text/css">
.alert {
	margin: 0 auto 20px;
}
</style>
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
    <script type="text/javascript" src="${assetPath(src: 'html5shiv.js')}"></script>
    <script type="text/javascript" src="${assetPath(src: 'respond.min.js')}"></script>
<![endif]-->
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="col-sm-6 col-md-4 col-md-offset-4">
				<h1 class="text-center login-title">
					<img src="${assetPath(src: 'long-name.png')}" alt="" width="330">
				</h1>
				<h1 class="text-center login-title">Masuk untuk melanjutkan ke
					Aplikasi</h1>
				<g:if test="${flash.message}">
					<div class="alert alert-info" role="status">
						<a class="close" data-dismiss="alert" href="#">&times;</a>
						${flash.message}
					</div>
				</g:if>
				<div class="account-wall">
					<img class="profile-img" src="${assetPath(src: 'photo.jpg')}"
						alt="">
					<form method="post" action="${postUrl}" class="form-signin">
						<input type="text" name="j_username" class="form-control"
							placeholder="Email" required autofocus> <input
							type="password" class="form-control" name="j_password"
							placeholder="Password" required>
						<button class="btn btn-lg btn-primary btn-block" type="submit">
							Sign in</button>
						<label class="checkbox pull-left"> <input type="checkbox"
							value="remember-me" name='${rememberMeParameter}'
							<g:if test='${hasCookie}'>checked='checked'</g:if>>
							Remember me
						</label> <a href="#" class="pull-right need-help">Need help? </a><span
							class="clearfix"></span>
					</form>
				</div>
				<a href="#" class="text-center new-account">Create an account </a>
			</div>
		</div>
	</div>
</body>
</html>
