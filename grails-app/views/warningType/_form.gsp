<%@ page import="com.osem.WarningType" %>



<div class="form-group fieldcontain ${hasErrors(bean: warningTypeInstance, field: 'warningType', 'error')} ">
	<label class="col-lg-2 control-label" for="warningType">
		<g:message code="warningType.warningType.label" default="Warning Type" />
		
	</label>
	<div class="col-lg-10"><g:textField class="form-control" name="warningType" value="${warningTypeInstance?.warningType}" /></div>

</div>

