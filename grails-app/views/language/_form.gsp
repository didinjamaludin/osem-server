<%@ page import="com.osem.Language" %>



<div class="form-group fieldcontain ${hasErrors(bean: languageInstance, field: 'langName', 'error')} ">
	<label class="col-lg-2 control-label" for="langName">
		<g:message code="language.langName.label" default="Bahasa" />
		
	</label>
	<div class="col-lg-10"><g:textField class="form-control" name="langName" value="${languageInstance?.langName}" /></div>

</div>

<div class="form-group fieldcontain ${hasErrors(bean: languageInstance, field: 'listenLevel', 'error')} ">
	<label class="col-lg-2 control-label" for="listenLevel">
		<g:message code="language.listenLevel.label" default="Mendengar" />
		
	</label>
	<div class="col-lg-10">
	<g:select class="form-control" from="${['Kurang','Sedang','Baik']}" name="listenLevel" value="${languageInstance?.listenLevel}" /></div>

</div>

<div class="form-group fieldcontain ${hasErrors(bean: languageInstance, field: 'person', 'error')} " style="display:none;">
	<label class="col-lg-2 control-label" for="person">
		<g:message code="language.person.label" default="Person" />
		
	</label>
	<div class="col-lg-10"><g:select class="form-control" id="person" name="person.id" from="${com.osem.Person.list()}" optionKey="id" required="" value="${languageInstance?.person?.id}"/></div>

</div>

<div class="form-group fieldcontain ${hasErrors(bean: languageInstance, field: 'speakLevel', 'error')} ">
	<label class="col-lg-2 control-label" for="speakLevel">
		<g:message code="language.speakLevel.label" default="Bicara" />
		
	</label>
	<div class="col-lg-10"><g:select class="form-control" from="${['Kurang','Sedang','Baik']}" name="speakLevel" value="${languageInstance?.speakLevel}" /></div>

</div>

<div class="form-group fieldcontain ${hasErrors(bean: languageInstance, field: 'writeLevel', 'error')} ">
	<label class="col-lg-2 control-label" for="writeLevel">
		<g:message code="language.writeLevel.label" default="Menulis" />
		
	</label>
	<div class="col-lg-10"><g:select class="form-control" from="${['Kurang','Sedang','Baik']}" name="writeLevel" value="${languageInstance?.writeLevel}" /></div>

</div>

