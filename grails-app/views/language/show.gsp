

<%@ page import="com.osem.Language" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<g:set var="entityName"
	value="${message(code: 'language.label', default: 'Language')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
</head>
<body>
	<div class="col-md-2 bootstrap-admin-col-left">
		<ul class="nav navbar-collapse collapse bootstrap-admin-navbar-side">
			<li><g:link action="index">
					<i class="glyphicon glyphicon-list-alt"></i>
					<g:message code="default.list.label" args="[entityName]" />
				</g:link></li>
			<li><g:link action="create">
					<i class="glyphicon glyphicon-plus"></i>
					<g:message code="default.new.label" args="[entityName]" />
				</g:link></li>
		</ul>
	</div>

	<div class="col-md-10">
		<div class="row">
			<div class="col-lg-12">
				<div class="page-header">
					<h1>
						<g:message code="default.show.label" args="[entityName]" />
					</h1>
				</div>
				<g:if test="${flash.message}">
					<div class="page-body">
						<div class="alert alert-success" role="status">
							<a class="close" data-dismiss="alert" href="#">&times;</a>${flash.message}
						</div>
					</div>
				</g:if>
				<g:hasErrors bean="${languageInstance}">
					<div class="page-body">
						<ul class="alert alert-danger" role="alert">
							<g:eachError bean="${languageInstance}" var="error">
								<li
									<g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message
										error="${error}" /></li>
							</g:eachError>
						</ul>
					</div>
				</g:hasErrors>
			</div>
		</div>

		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default bootstrap-admin-no-table-panel">
					<div
						class="bootstrap-admin-no-table-panel-content bootstrap-admin-panel-content collapse in">
						<dl class="dl-horizontal">
							
				<g:if test="${languageInstance?.langName}">
					<dt><g:message code="language.langName.label" default="Lang Name" /></dt>
					
						<dd><g:fieldValue bean="${languageInstance}" field="langName"/></dd>
					
				</g:if>
			
				<g:if test="${languageInstance?.listenLevel}">
					<dt><g:message code="language.listenLevel.label" default="Listen Level" /></dt>
					
						<dd><g:fieldValue bean="${languageInstance}" field="listenLevel"/></dd>
					
				</g:if>
			
				<g:if test="${languageInstance?.person}">
					<dt><g:message code="language.person.label" default="Person" /></dt>
					
						<dd><g:link controller="person" action="show" id="${languageInstance?.person?.id}">${languageInstance?.person?.encodeAsHTML()}</g:link></dd>
					
				</g:if>
			
				<g:if test="${languageInstance?.speakLevel}">
					<dt><g:message code="language.speakLevel.label" default="Speak Level" /></dt>
					
						<dd><g:fieldValue bean="${languageInstance}" field="speakLevel"/></dd>
					
				</g:if>
			
				<g:if test="${languageInstance?.writeLevel}">
					<dt><g:message code="language.writeLevel.label" default="Write Level" /></dt>
					
						<dd><g:fieldValue bean="${languageInstance}" field="writeLevel"/></dd>
					
				</g:if>
			
						</dl>
						<g:form url="[resource:languageInstance, action:'delete']"
							method="DELETE">
							<fieldset class="buttons">
								<g:link class="btn btn-primary" action="edit"
									resource="${languageInstance}">
									<g:message code="default.button.edit.label" default="Edit" />
								</g:link>
								<g:actionSubmit class="btn btn-default" action="delete"
									value="${message(code: 'default.button.delete.label', default: 'Delete')}"
									onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
							</fieldset>
						</g:form>
					</div>
				</div>
			</div>
		</div>

	</div>
</body>
</html>
