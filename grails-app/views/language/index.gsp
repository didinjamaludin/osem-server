

<%@ page import="com.osem.Language" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<g:set var="entityName"
	value="${message(code: 'language.label', default: 'Language')}" />
<title><g:message code="default.list.label" args="[entityName]" /></title>
</head>
<body>
	<div class="col-md-2 bootstrap-admin-col-left">
		<ul class="nav navbar-collapse collapse bootstrap-admin-navbar-side">
			<li><g:link action="create">
					<i class="glyphicon glyphicon-plus"></i>
					<g:message code="default.new.label" args="[entityName]" />
				</g:link></li>
		</ul>
	</div>

	<div class="col-md-10">
		<div class="row">
			<div class="col-lg-12">
				<div class="page-header">
					<h1>
						<g:message code="default.list.label" args="[entityName]" />
					</h1>
				</div>
				<g:if test="${flash.message}">
					<div class="page-body">
						<div class="alert alert-success" role="status">
							<a class="close" data-dismiss="alert" href="#">&times;</a>${flash.message}
						</div>
					</div>
				</g:if>
			</div>
		</div>

		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="bootstrap-admin-panel-content">
						<g:if test="${languageInstanceList}">
							<table class="table bootstrap-admin-table-with-actions">
								<thead>
									<tr>
										
						<g:sortableColumn property="langName" title="${message(code: 'language.langName.label', default: 'Lang Name')}" />
					
						<g:sortableColumn property="listenLevel" title="${message(code: 'language.listenLevel.label', default: 'Listen Level')}" />
					
						<th><g:message code="language.person.label" default="Person" /></th>
					
						<g:sortableColumn property="speakLevel" title="${message(code: 'language.speakLevel.label', default: 'Speak Level')}" />
					
						<g:sortableColumn property="writeLevel" title="${message(code: 'language.writeLevel.label', default: 'Write Level')}" />
					
									</tr>
								</thead>
								<tbody>
									<g:each in="${languageInstanceList}" status="i"
										var="languageInstance">
										<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
											
						<td><g:link action="show" id="${languageInstance.id}">${fieldValue(bean: languageInstance, field: "langName")}</g:link></td>
					
						<td>${fieldValue(bean: languageInstance, field: "listenLevel")}</td>
					
						<td>${fieldValue(bean: languageInstance, field: "person")}</td>
					
						<td>${fieldValue(bean: languageInstance, field: "speakLevel")}</td>
					
						<td>${fieldValue(bean: languageInstance, field: "writeLevel")}</td>
					
										</tr>
									</g:each>
								</tbody>
							</table>
							<div class="pagination">
								<g:paginate total="${languageInstanceCount ?: 0}" />
							</div>
						</g:if>
						<g:else>
							No language found
						</g:else>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
