<!DOCTYPE html>
<!--[if lt IE 7 ]> <html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>OSEM</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="shortcut icon" href="${assetPath(src: 'favicon.ico')}"
	type="image/x-icon">
<asset:stylesheet src="application.css" />
<asset:javascript src="application.js" />
<link href="https://s3-ap-southeast-1.amazonaws.com/static.mitragardamandiri/all.css" rel="stylesheet">
<script src="https://s3-ap-southeast-1.amazonaws.com/static.mitragardamandiri/icheck.min.js"></script>
<g:layoutHead />
</head>
<body class="bootstrap-admin-with-small-navbar">
	<!-- small navbar -->
	<nav
		class="navbar navbar-default navbar-fixed-top bootstrap-admin-navbar-sm"
		role="navigation">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="collapse navbar-collapse">
						<ul
							class="nav navbar-nav navbar-left bootstrap-admin-theme-change-size">
						</ul>
						<ul class="nav navbar-nav navbar-right">
							<li class="dropdown"><a href="#" role="button"
								class="dropdown-toggle" data-hover="dropdown"> <i
									class="glyphicon glyphicon-user"></i> <sec:loggedInUserInfo
										field="fullname"></sec:loggedInUserInfo> <i class="caret"></i></a>
								<ul class="dropdown-menu">
									<li><g:link controller="user" action="changePassword">Action</g:link></li>
									<li role="presentation" class="divider"></li>
									<li><g:link controller="logout">Logout</g:link></li>
								</ul></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</nav>

	<!-- main / large navbar -->
	<nav
		class="navbar navbar-default navbar-fixed-top bootstrap-admin-navbar bootstrap-admin-navbar-under-small"
		role="navigation">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse"
							data-target=".main-navbar-collapse">
							<span class="sr-only">Toggle navigation</span> <span
								class="icon-bar"></span> <span class="icon-bar"></span> <span
								class="icon-bar"></span>
						</button>
						<g:link controller="hr" class="navbar-brand">Admin Panel</g:link>
					</div>
					<div class="collapse navbar-collapse main-navbar-collapse">
						<ul class="nav navbar-nav">
							<li class="dropdown"><a href="#" class="dropdown-toggle"
								data-hover="dropdown"><i class="glyphicon glyphicon-user"></i> Employee <b class="caret"></b></a>
								<ul class="dropdown-menu">
									<li role="presentation" class="dropdown-header">Basic Data</li>
									<li><g:link controller="person">Personal Data</g:link></li>
								</ul></li>
							<li class="dropdown"><a href="#" class="dropdown-toggle"
								data-hover="dropdown"><i class="glyphicon glyphicon-play-circle"></i> Activity <b class="caret"></b></a>
								<ul class="dropdown-menu">
									<li role="presentation" class="dropdown-header">Aktifitas Karyawan</li>
									<li><g:link controller="contract">Kontrak</g:link></li>
									<li><g:link controller="permanent">Karyawan Tetap</g:link></li>
									<li><g:link controller="promotion">Promosi</g:link></li>
									<li><g:link controller="mutation">Mutasi</g:link></li>
									<li><g:link controller="warning">Peringatan</g:link></li>
									<li><g:link controller="resignation">Pengunduran Diri</g:link></li>
									<li><g:link controller="termination">PHK</g:link></li>
									<li role="presentation" class="dropdown-header">Penggajian</li>
									<li><g:link controller="payroll" action="process">Proses Penggajian</g:link></li>
									<li><g:link controller="payroll" action="index">Daftar Penggajian</g:link></li>
									<li><g:link controller="payroll" action="printStruct">Print Struk Gaji</g:link></li>
								</ul></li>
							<li class="dropdown"><a href="#" class="dropdown-toggle"
								data-hover="dropdown"><i class="glyphicon glyphicon-hdd"></i> Master Data <b class="caret"></b></a>
								<ul class="dropdown-menu">
									<li role="presentation" class="dropdown-header">General</li>
									<li><g:link controller="unit">Unit</g:link></li>
									<li><g:link controller="subUnit">Sub Unit</g:link></li>
									<li><g:link controller="dept">Dept</g:link></li>
									<li><g:link controller="division">Divisi</g:link></li>
									<li><g:link controller="position">Posisi</g:link></li>
									<li><g:link controller="warningType">Jenis SP</g:link></li>
									<li role="presentation" class="divider"></li>
									<li role="presentation" class="dropdown-header">Payroll</li>
									<li><g:link controller="basicSalary">Skema Biaya</g:link></li>
								</ul></li>
						</ul>
					</div>
					<!-- /.navbar-collapse -->
				</div>
			</div>
		</div>
		<!-- /.container -->
	</nav>

	<div class="container">
		<div class="row">
			<g:layoutBody />
		</div>
	</div>
</body>
</html>
