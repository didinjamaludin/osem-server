

<%@ page import="com.osem.WorkHistory" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<g:set var="entityName"
	value="${message(code: 'workHistory.label', default: 'WorkHistory')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
</head>
<body>
	<div class="col-md-2 bootstrap-admin-col-left">
		<ul class="nav navbar-collapse collapse bootstrap-admin-navbar-side">
			<li><g:link action="index">
					<i class="glyphicon glyphicon-list-alt"></i>
					<g:message code="default.list.label" args="[entityName]" />
				</g:link></li>
			<li><g:link action="create">
					<i class="glyphicon glyphicon-plus"></i>
					<g:message code="default.new.label" args="[entityName]" />
				</g:link></li>
		</ul>
	</div>

	<div class="col-md-10">
		<div class="row">
			<div class="col-lg-12">
				<div class="page-header">
					<h1>
						<g:message code="default.show.label" args="[entityName]" />
					</h1>
				</div>
				<g:if test="${flash.message}">
					<div class="page-body">
						<div class="alert alert-success" role="status">
							<a class="close" data-dismiss="alert" href="#">&times;</a>${flash.message}
						</div>
					</div>
				</g:if>
				<g:hasErrors bean="${workHistoryInstance}">
					<div class="page-body">
						<ul class="alert alert-danger" role="alert">
							<g:eachError bean="${workHistoryInstance}" var="error">
								<li
									<g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message
										error="${error}" /></li>
							</g:eachError>
						</ul>
					</div>
				</g:hasErrors>
			</div>
		</div>

		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default bootstrap-admin-no-table-panel">
					<div
						class="bootstrap-admin-no-table-panel-content bootstrap-admin-panel-content collapse in">
						<dl class="dl-horizontal">
							
				<g:if test="${workHistoryInstance?.lastSalary}">
					<dt><g:message code="workHistory.lastSalary.label" default="Last Salary" /></dt>
					
						<dd><g:fieldValue bean="${workHistoryInstance}" field="lastSalary"/></dd>
					
				</g:if>
			
				<g:if test="${workHistoryInstance?.person}">
					<dt><g:message code="workHistory.person.label" default="Person" /></dt>
					
						<dd><g:link controller="person" action="show" id="${workHistoryInstance?.person?.id}">${workHistoryInstance?.person?.encodeAsHTML()}</g:link></dd>
					
				</g:if>
			
				<g:if test="${workHistoryInstance?.workAddress}">
					<dt><g:message code="workHistory.workAddress.label" default="Work Address" /></dt>
					
						<dd><g:fieldValue bean="${workHistoryInstance}" field="workAddress"/></dd>
					
				</g:if>
			
				<g:if test="${workHistoryInstance?.workIn}">
					<dt><g:message code="workHistory.workIn.label" default="Work In" /></dt>
					
						<dd><g:fieldValue bean="${workHistoryInstance}" field="workIn"/></dd>
					
				</g:if>
			
				<g:if test="${workHistoryInstance?.workInstitution}">
					<dt><g:message code="workHistory.workInstitution.label" default="Work Institution" /></dt>
					
						<dd><g:fieldValue bean="${workHistoryInstance}" field="workInstitution"/></dd>
					
				</g:if>
			
				<g:if test="${workHistoryInstance?.workOut}">
					<dt><g:message code="workHistory.workOut.label" default="Work Out" /></dt>
					
						<dd><g:fieldValue bean="${workHistoryInstance}" field="workOut"/></dd>
					
				</g:if>
			
				<g:if test="${workHistoryInstance?.workPosition}">
					<dt><g:message code="workHistory.workPosition.label" default="Work Position" /></dt>
					
						<dd><g:fieldValue bean="${workHistoryInstance}" field="workPosition"/></dd>
					
				</g:if>
			
						</dl>
						<g:form url="[resource:workHistoryInstance, action:'delete']"
							method="DELETE">
							<fieldset class="buttons">
								<g:link class="btn btn-primary" action="edit"
									resource="${workHistoryInstance}">
									<g:message code="default.button.edit.label" default="Edit" />
								</g:link>
								<g:actionSubmit class="btn btn-default" action="delete"
									value="${message(code: 'default.button.delete.label', default: 'Delete')}"
									onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
							</fieldset>
						</g:form>
					</div>
				</div>
			</div>
		</div>

	</div>
</body>
</html>
