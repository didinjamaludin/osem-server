

<%@ page import="com.osem.WorkHistory" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<g:set var="entityName"
	value="${message(code: 'workHistory.label', default: 'WorkHistory')}" />
<title><g:message code="default.list.label" args="[entityName]" /></title>
</head>
<body>
	<div class="col-md-2 bootstrap-admin-col-left">
		<ul class="nav navbar-collapse collapse bootstrap-admin-navbar-side">
			<li><g:link action="create">
					<i class="glyphicon glyphicon-plus"></i>
					<g:message code="default.new.label" args="[entityName]" />
				</g:link></li>
		</ul>
	</div>

	<div class="col-md-10">
		<div class="row">
			<div class="col-lg-12">
				<div class="page-header">
					<h1>
						<g:message code="default.list.label" args="[entityName]" />
					</h1>
				</div>
				<g:if test="${flash.message}">
					<div class="page-body">
						<div class="alert alert-success" role="status">
							<a class="close" data-dismiss="alert" href="#">&times;</a>${flash.message}
						</div>
					</div>
				</g:if>
			</div>
		</div>

		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="bootstrap-admin-panel-content">
						<g:if test="${workHistoryInstanceList}">
							<table class="table bootstrap-admin-table-with-actions">
								<thead>
									<tr>
										
						<g:sortableColumn property="lastSalary" title="${message(code: 'workHistory.lastSalary.label', default: 'Last Salary')}" />
					
						<th><g:message code="workHistory.person.label" default="Person" /></th>
					
						<g:sortableColumn property="workAddress" title="${message(code: 'workHistory.workAddress.label', default: 'Work Address')}" />
					
						<g:sortableColumn property="workIn" title="${message(code: 'workHistory.workIn.label', default: 'Work In')}" />
					
						<g:sortableColumn property="workInstitution" title="${message(code: 'workHistory.workInstitution.label', default: 'Work Institution')}" />
					
						<g:sortableColumn property="workOut" title="${message(code: 'workHistory.workOut.label', default: 'Work Out')}" />
					
									</tr>
								</thead>
								<tbody>
									<g:each in="${workHistoryInstanceList}" status="i"
										var="workHistoryInstance">
										<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
											
						<td><g:link action="show" id="${workHistoryInstance.id}">${fieldValue(bean: workHistoryInstance, field: "lastSalary")}</g:link></td>
					
						<td>${fieldValue(bean: workHistoryInstance, field: "person")}</td>
					
						<td>${fieldValue(bean: workHistoryInstance, field: "workAddress")}</td>
					
						<td>${fieldValue(bean: workHistoryInstance, field: "workIn")}</td>
					
						<td>${fieldValue(bean: workHistoryInstance, field: "workInstitution")}</td>
					
						<td>${fieldValue(bean: workHistoryInstance, field: "workOut")}</td>
					
										</tr>
									</g:each>
								</tbody>
							</table>
							<div class="pagination">
								<g:paginate total="${workHistoryInstanceCount ?: 0}" />
							</div>
						</g:if>
						<g:else>
							No workHistory found
						</g:else>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
