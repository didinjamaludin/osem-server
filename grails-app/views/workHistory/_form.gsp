<%@ page import="com.osem.WorkHistory" %>


<div class="form-group fieldcontain ${hasErrors(bean: workHistoryInstance, field: 'person', 'error')} " style="display: none;">
	<label class="col-lg-2 control-label" for="person">
		<g:message code="workHistory.person.label" default="Person" />
		
	</label>
	<div class="col-lg-10"><g:select class="form-control" id="person" name="person.id" from="${com.osem.Person.list()}" optionKey="id" required="" value="${workHistoryInstance?.person?.id}"/></div>

</div>

<div class="form-group fieldcontain ${hasErrors(bean: workHistoryInstance, field: 'workInstitution', 'error')} ">
	<label class="col-lg-2 control-label" for="workInstitution">
		<g:message code="workHistory.workInstitution.label" default="Nama Perusahaan" />
		
	</label>
	<div class="col-lg-10"><g:textField class="form-control" name="workInstitution" value="${workHistoryInstance?.workInstitution}" /></div>

</div>

<div class="form-group fieldcontain ${hasErrors(bean: workHistoryInstance, field: 'workAddress', 'error')} ">
	<label class="col-lg-2 control-label" for="workAddress">
		<g:message code="workHistory.workAddress.label" default="Alamat" />
		
	</label>
	<div class="col-lg-10"><g:textField class="form-control" name="workAddress" value="${workHistoryInstance?.workAddress}" /></div>

</div>

<div class="form-group fieldcontain ${hasErrors(bean: workHistoryInstance, field: 'workPosition', 'error')} ">
	<label class="col-lg-2 control-label" for="workPosition">
		<g:message code="workHistory.workPosition.label" default="Posisi/Jabatan" />
		
	</label>
	<div class="col-lg-10"><g:textField class="form-control" name="workPosition" value="${workHistoryInstance?.workPosition}" /></div>

</div>

<div class="form-group fieldcontain ${hasErrors(bean: workHistoryInstance, field: 'workIn', 'error')} ">
	<label class="col-lg-2 control-label" for="workIn">
		<g:message code="workHistory.workIn.label" default="Tahun Masuk" />
		
	</label>
	<div class="col-lg-10"><g:field type="number" class="form-control" name="workIn" value="${workHistoryInstance?.workIn}" /></div>

</div>

<div class="form-group fieldcontain ${hasErrors(bean: workHistoryInstance, field: 'workOut', 'error')} ">
	<label class="col-lg-2 control-label" for="workOut">
		<g:message code="workHistory.workOut.label" default="Tahun Keluar" />
		
	</label>
	<div class="col-lg-10"><g:field type="number" class="form-control" name="workOut" value="${workHistoryInstance?.workOut}" /></div>

</div>

<div class="form-group fieldcontain ${hasErrors(bean: workHistoryInstance, field: 'lastSalary', 'error')} ">
	<label class="col-lg-2 control-label" for="lastSalary">
		<g:message code="workHistory.lastSalary.label" default="Gaji Terakhir" />
		
	</label>
	<div class="col-lg-10"><g:field class="form-control" type="number" name="lastSalary" value="${workHistoryInstance.lastSalary}" /></div>

</div>

