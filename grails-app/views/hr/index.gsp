<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main" />
<title>HR Dashboard</title>
</head>
<body>
	<div class="col-md-2">
		<ul class="nav navbar-collapse collapse bootstrap-admin-navbar-side">
			<li><a href="#"><span class="badge pull-right">0</span>
					Jumlah Orang</a></li>
			<li><a href="#"><span class="badge pull-right">0</span>
					Pelamar</a></li>
			<li><a href="#"><span class="badge pull-right">0</span>
					Lulus</a></li>
			<li><a href="#"><span class="badge pull-right">0</span>
					Training</a></li>
			<li><a href="#"><span class="badge pull-right">0</span>
					Kontrak</a></li>
			<li><a href="#"><span class="badge pull-right">0</span>
					Tetap</a></li>
			<li><a href="#"><span class="badge pull-right">0</span> SP</a></li>
			<li><a href="#"><span class="badge pull-right">0</span>
					Mutasi</a></li>
			<li><a href="#"><span class="badge pull-right">0</span>
					Promosi</a></li>
			<li><a href="#"><span class="badge pull-right">0</span>
					Resign</a></li>
			<li><a href="#"><span class="badge pull-right">0</span> PHK</a></li>
		</ul>
	</div>

	<div class="col-md-10">
		<div class="row">
			<div class="col-lg-12">
				<div class="page-header">
					<h1>HR Dashboard</h1>
				</div>
				<g:if test="${flash.message}">
					<div class="page-body">
						<div class="alert alert-success" role="status">
							<a class="close" data-dismiss="alert" href="#">&times;</a>
							${flash.message}
						</div>
					</div>
				</g:if>
			</div>
		</div>

		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						<div class="row">
							<div class="col-lg-4">
								<input type="text" name="byunit" id="byunit" class="form-control"
									placeholder="Search By Unit" /><i
									class="fa fa-search"></i>
							</div>
							<div class="col-lg-4">
								<input type="text" name="bysubunit" id="bysubunit" class="form-control"
									placeholder="Search By SubUnit" onkeyup="filterlist()" /><i
									class="fa fa-search"></i>
							</div>
							<div class="col-lg-4">
								<input type="text" name="bynik" id="bynik" class="form-control"
									placeholder="Search By NIK" onkeyup="filterlist()" /><i
									class="fa fa-search"></i>
							</div>
							<div class="col-lg-4">
								<input type="text" name="byname" id="byname" class="form-control"
									placeholder="Search By Name" onkeyup="filterlist()" /><i
									class="fa fa-search"></i>
							</div>
							<div class="col-lg-4">
								<input type="text" name="byposisi" id="byposisi" class="form-control"
									placeholder="Search By Position" onkeyup="filterlist()" /><i
									class="fa fa-search"></i>
							</div>
							<div class="col-lg-4">
								<input type="text" name="bystatus" id="bystatus" class="form-control"
									placeholder="Search By Status" onkeyup="filterlist()" /><i
									class="fa fa-search"></i>
							</div>
						</div>
					</div>
					<div class="bootstrap-admin-panel-content" id="persongrid">
						<g:render template="persongrid" />
					</div>
				</div>
			</div>
		</div>
	</div>

	<script>
        function filterlist() {
           	$.ajax({
               	url: "${request.contextPath}/hr/index",
				data : 'bynik=' + $("#bynik").val() + '&byname='
						+ $("#byname").val() + '&byunit=' + $("#byunit").val()
						+ '&bysubunit=' + $("#bysubunit").val() + '&byposisi='
						+ $("#byposisi").val() + '&bystatus='
						+ $("#bystatus").val(),
				success : function(msg) {
					document.getElementById('persongrid').innerHTML = msg;
				}
			});
		}
	</script>
</body>
</html>
