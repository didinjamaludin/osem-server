<table class="table table-striped bootstrap-admin-table-with-actions">
	<thead>
		<tr>
			<th>Unit</th>
			<th>Sub Unit</th>
			<th>NIK</th>
			<th>Nama Lengkap</th>
			<th>Posisi</th>
			<th>Status</th>
			<th>Action</th>
		</tr>
	</thead>
	<tbody>
		<g:each in="${personInstanceList}" status="i" var="personInstance">
			<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
				<td>
					${com.osem.Unit.get(personInstance?.subUnit?.unit?.id)}
				</td>

				<td>
					${fieldValue(bean: personInstance, field: "subUnit")}
				</td>

				<td><g:link action="show" id="${personInstance.id}">
						${fieldValue(bean: personInstance, field: "pid")}
					</g:link></td>

				<td><g:link action="show" id="${personInstance.id}">
						${fieldValue(bean: personInstance, field: "fullname")}
					</g:link></td>

				<td>
					${fieldValue(bean: personInstance, field: "position")}
				</td>

				<td>
					${fieldValue(bean: personInstance, field: "empStatus")}
				</td>
				<td class="actions"><g:if
						test="${personInstance?.empStatus?.equals('PELAMAR')}">
						<g:link controller="approval" action="create"
							params="[personid:personInstance.id]">
							<button class="btn btn-sm btn-primary">
								<i class="glyphicon glyphicon-ok"></i> APPROVE
							</button>
						</g:link>
					</g:if>
					<g:if
						test="${personInstance?.empStatus?.equals('DITERIMA')}">
						<g:link controller="prohibition" action="create"
							params="[personid:personInstance.id]">
							<button class="btn btn-sm btn-success">
								<i class="glyphicon glyphicon-tower"></i> PERCOBAAN
							</button>
						</g:link>
					</g:if>
					<g:if
						test="${personInstance?.empStatus?.equals('PERCOBAAN')&&!personInstance?.unit}">
						<g:link controller="assigment" action="create"
							params="[personid:personInstance.id]">
							<button class="btn btn-sm btn-info">
								<i class="glyphicon glyphicon-tag"></i> PENUGASAN
							</button>
						</g:link>
					</g:if>
					</td>
			</tr>
		</g:each>
	</tbody>
</table>