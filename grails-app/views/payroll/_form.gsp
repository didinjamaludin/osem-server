<%@ page import="com.osem.Payroll" %>

<div class="form-group fieldcontain ${hasErrors(bean: payrollInstance, field: 'month', 'error')} ">
	<div class="col-lg-4"><h3>Bulan Penggajian ${payrollInstance?.month}/${payrollInstance?.year}</h3></div>
</div>

<div class="form-group fieldcontain ${hasErrors(bean: payrollInstance, field: 'person', 'error')} ">
	<label class="col-lg-2 control-label" for="person">
		<g:message code="payroll.person.label" default="Karyawan" />
		
	</label>
	<div class="col-lg-10"><span class="form-control" id="showNama">${payrollInstance?.person?.pid} - ${payrollInstance?.person?.fullname }</span></div>

</div>

<div class="form-group fieldcontain ${hasErrors(bean: payrollInstance, field: 'monthlyAtt', 'error')} ">
	<label class="col-lg-2 control-label" for="monthlyAtt">
		<g:message code="payroll.monthlyAtt.label" default="Hari Kerja" />
		
	</label>
	<div class="col-lg-10"><span class="form-control" id="showHari"><g:formatNumber number="${payrollInstance.monthlyAtt}" /></span></div>

</div>

<div class="form-group fieldcontain ${hasErrors(bean: payrollInstance, field: 'totalAtt', 'error')} ">
	<label class="col-lg-2 control-label" for="totalAtt">
		<g:message code="payroll.totalAtt.label" default="Tidak Masuk (hari)" />
		
	</label>
	<div class="col-lg-10"><g:field class="form-control" type="number" name="totalAtt" value="${payrollInstance.totalAtt}" onkeyup="countAtt(this.value)" /></div>

</div>

<div class="form-group fieldcontain ${hasErrors(bean: payrollInstance, field: 'attCut', 'error')} ">
	<label class="col-lg-2 control-label" for="attCut">
		<g:message code="payroll.attCut.label" default="Tidak Masuk" />
		<i class="glyphicon glyphicon-minus-sign"></i>
	</label>
	<div class="col-lg-10"><span class="form-control" id="showAttcut"></span><g:hiddenField name="attCut" value="${payrollInstance.attCut}" /></div>

</div>

<div class="form-group fieldcontain ${hasErrors(bean: payrollInstance, field: 'totalOt', 'error')} ">
	<label class="col-lg-2 control-label" for="totalOt">
		<g:message code="payroll.totalOt.label" default="Lembur (jam)" />
		
	</label>
	<div class="col-lg-10"><g:field class="form-control" type="number" name="totalOt" value="${payrollInstance.totalOt}" onkeyup="countOt(this.value)" /></div>

</div>

<div class="form-group fieldcontain ${hasErrors(bean: payrollInstance, field: 'amountOt', 'error')} ">
	<label class="col-lg-2 control-label" for="amountOt">
		<g:message code="payroll.amountOt.label" default="Lembur" />
		<i class="glyphicon glyphicon-plus-sign"></i>
	</label>
	<div class="col-lg-10"><span class="form-control" id="showAmountot"></span><g:hiddenField name="amountOt" value="${payrollInstance.amountOt}" /></div>

</div>

<div class="form-group fieldcontain ${hasErrors(bean: payrollInstance, field: 'bpjsCut', 'error')} ">
	<label class="col-lg-2 control-label" for="bpjsCut">
		<g:message code="payroll.bpjsCut.label" default="BPJS" />
		<i class="glyphicon glyphicon-minus-sign"></i>
	</label>
	<div class="col-lg-10"><span class="form-control" id="showBpjs"><g:formatNumber number="${payrollInstance.bpjsCut}" /></span></div>

</div>

<div class="form-group fieldcontain ${hasErrors(bean: payrollInstance, field: 'jamsostekCut', 'error')} ">
	<label class="col-lg-2 control-label" for="jamsostekCut">
		<g:message code="payroll.jamsostekCut.label" default="Jamsostek" />
		<i class="glyphicon glyphicon-minus-sign"></i>
	</label>
	<div class="col-lg-10"><span class="form-control" id="showJamsostek"><g:formatNumber number="${payrollInstance.jamsostekCut}" /></span></div>

</div>

<div class="form-group fieldcontain ${hasErrors(bean: payrollInstance, field: 'deposit', 'error')} ">
	<label class="col-lg-2 control-label" for="deposit">
		<g:message code="payroll.deposit.label" default="Deposit" />
		<i class="glyphicon glyphicon-minus-sign"></i>
	</label>
	<div class="col-lg-10"><span class="form-control" id="showDeposit"><g:formatNumber number="${payrollInstance.deposit}" /></span></div>

</div>


<div class="form-group fieldcontain ${hasErrors(bean: payrollInstance, field: 'otherFee', 'error')} ">
	<label class="col-lg-2 control-label" for="otherFee">
		<g:message code="payroll.otherFee.label" default="Biaya Lain-lain" />
		<i class="glyphicon glyphicon-minus-sign"></i>
	</label>
	<div class="col-lg-10"><g:field class="form-control" type="number" name="otherFee" value="${payrollInstance.otherFee}" /></div>

</div>

<div class="form-group fieldcontain ${hasErrors(bean: payrollInstance, field: 'pph21', 'error')} ">
	<label class="col-lg-2 control-label" for="pph21">
		<g:message code="payroll.pph21.label" default="PPH21" />
		<i class="glyphicon glyphicon-minus-sign"></i>
	</label>
	<div class="col-lg-10"><g:field class="form-control" type="number" name="pph21" value="${payrollInstance.pph21}" /></div>

</div>

<div class="form-group fieldcontain ${hasErrors(bean: payrollInstance, field: 'takeHomePay', 'error')} ">
	<label class="col-lg-2 control-label" for="takeHomePay">
		<g:message code="payroll.takeHomePay.label" default="Take Home Pay" />
		
	</label>
	<div class="col-lg-10"><span class="form-control" id="showThp"><g:formatNumber number="${payrollInstance.takeHomePay}" /></span><g:hiddenField name="takeHomePay" value="${payrollInstance.takeHomePay}" /></div>

</div>

<div class="form-group fieldcontain ${hasErrors(bean: payrollInstance, field: 'notes', 'error')} ">
	<label class="col-lg-2 control-label" for="notes">
		<g:message code="payroll.notes.label" default="Catatan" />
		
	</label>
	<div class="col-lg-10"><g:textField class="form-control" name="notes" value="${payrollInstance?.notes}" /></div>

</div>

<g:hiddenField name="oldThp" value="${payrollInstance.takeHomePay}" />

<script>
	function countThp() {
		var currentThp = parseInt($('#showThp').text());
		var attCut = parseInt($('#attCut').val());
		var amountOt = parseInt($('#amountOt').val());
		var pph21 = parseInt($('#pph21').val());
		var newthp = (currentThp+amountOt)-(attCut+pph21);
		$('#showThp').text(newthp);
		$('#takeHomePay').val(newthp);
	}

	function countAtt(value) {
		var day = 0;
		if(value) {
			day = parseInt(value);
		}
		var fullday = parseInt($('#showHari').text());
		var oldThp = parseInt($('#oldThp').val());
		var daythp = Math.round(oldThp/fullday);
		var attamount = day*daythp;

		$('#showAttcut').text(attamount);
		$('#attCut').val(attamount);

		countThp();
	}

	function countOt(value) {
		var hour = 0;
		if(value) {
			hour = parseInt(value);
		}
		var fullday = parseInt($('#showHari').text());
		var oldThp = parseInt($('#oldThp').val());
		var hourthp = Math.round((oldThp/fullday)/8);
		var otamount = hour*hourthp;

		$('#showAmountot').text(otamount);
		$('#amountOt').val(otamount);

		countThp();
	}
</script>
