

<%@ page import="com.osem.Payroll"%>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<g:set var="entityName"
	value="${message(code: 'payroll.label', default: 'Payroll')}" />
<title><g:message code="default.list.label" args="[entityName]" /></title>
</head>
<body>
	<div class="col-md-2 bootstrap-admin-col-left">
		<ul class="nav navbar-collapse collapse bootstrap-admin-navbar-side">
			<li><g:link action="create">
					<i class="glyphicon glyphicon-plus"></i>
					<g:message code="default.new.label" args="[entityName]" />
				</g:link></li>
		</ul>
	</div>

	<div class="col-md-10">
		<div class="row">
			<div class="col-lg-12">
				<div class="page-header">
					<h1>
						<g:message code="default.list.label" args="[entityName]" />
					</h1>
				</div>
				<g:if test="${flash.message}">
					<div class="page-body">
						<div class="alert alert-success" role="status">
							<a class="close" data-dismiss="alert" href="#">&times;</a>
							${flash.message}
						</div>
					</div>
				</g:if>
			</div>
		</div>

		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="bootstrap-admin-panel-content">
						<g:if test="${payrollInstanceList}">
							<table class="table bootstrap-admin-table-with-actions">
								<thead>
									<tr>

										<g:sortableColumn property="month"
											title="${message(code: 'payroll.month.label', default: 'Bulan')}" style="text-align: center" />

										<g:sortableColumn property="year"
											title="${message(code: 'payroll.year.label', default: 'Tahun')}" style="text-align: center" />

										<g:sortableColumn property="person"
											title="${message(code: 'payroll.person.label', default: 'Karyawan')}" />

										<g:sortableColumn property="takeHomePay"
											title="${message(code: 'payroll.takeHomePay.label', default: 'THP')}" />

										<th style="text-align: center">Action</th>

									</tr>
								</thead>
								<tbody>
									<g:each in="${payrollInstanceList}" status="i"
										var="payrollInstance">
										<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">

											<td style="text-align: center">
												${fieldValue(bean: payrollInstance, field: "month")}
											</td>

											<td style="text-align: center">
												${payrollInstance?.year}
											</td>

											<td><g:link action="show" id="${payrollInstance.id}">
													${fieldValue(bean: payrollInstance, field: "person")}
												</g:link></td>

											<td>
												${fieldValue(bean: payrollInstance, field: "takeHomePay")}
											</td>

											<td style="text-align: center"><g:link action="edit" id="${payrollInstance.id}">
													<i class="glyphicon glyphicon-plus-sign"></i>/<i
														class="glyphicon glyphicon-minus-sign"></i>
												</g:link></td>

										</tr>
									</g:each>
								</tbody>
							</table>
							<div class="pagination">
								<g:paginate total="${payrollInstanceCount ?: 0}" />
							</div>
						</g:if>
						<g:else>
							No payroll found
						</g:else>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
