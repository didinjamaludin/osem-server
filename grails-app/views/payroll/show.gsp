

<%@ page import="com.osem.Payroll" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<g:set var="entityName"
	value="${message(code: 'payroll.label', default: 'Payroll')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
</head>
<body>
	<div class="col-md-2 bootstrap-admin-col-left">
		<ul class="nav navbar-collapse collapse bootstrap-admin-navbar-side">
			<li><g:link action="index">
					<i class="glyphicon glyphicon-list-alt"></i>
					<g:message code="default.list.label" args="[entityName]" />
				</g:link></li>
			<li><g:link action="create">
					<i class="glyphicon glyphicon-plus"></i>
					<g:message code="default.new.label" args="[entityName]" />
				</g:link></li>
		</ul>
	</div>

	<div class="col-md-10">
		<div class="row">
			<div class="col-lg-12">
				<div class="page-header">
					<h1>
						<g:message code="default.show.label" args="[entityName]" />
					</h1>
				</div>
				<g:if test="${flash.message}">
					<div class="page-body">
						<div class="alert alert-success" role="status">
							<a class="close" data-dismiss="alert" href="#">&times;</a>${flash.message}
						</div>
					</div>
				</g:if>
				<g:hasErrors bean="${payrollInstance}">
					<div class="page-body">
						<ul class="alert alert-danger" role="alert">
							<g:eachError bean="${payrollInstance}" var="error">
								<li
									<g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message
										error="${error}" /></li>
							</g:eachError>
						</ul>
					</div>
				</g:hasErrors>
			</div>
		</div>

		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default bootstrap-admin-no-table-panel">
					<div
						class="bootstrap-admin-no-table-panel-content bootstrap-admin-panel-content collapse in">
						<dl class="dl-horizontal">
							
				<g:if test="${payrollInstance?.amountOt}">
					<dt><g:message code="payroll.amountOt.label" default="Amount Ot" /></dt>
					
						<dd><g:fieldValue bean="${payrollInstance}" field="amountOt"/></dd>
					
				</g:if>
			
				<g:if test="${payrollInstance?.attCut}">
					<dt><g:message code="payroll.attCut.label" default="Att Cut" /></dt>
					
						<dd><g:fieldValue bean="${payrollInstance}" field="attCut"/></dd>
					
				</g:if>
			
				<g:if test="${payrollInstance?.bpjsCut}">
					<dt><g:message code="payroll.bpjsCut.label" default="Bpjs Cut" /></dt>
					
						<dd><g:fieldValue bean="${payrollInstance}" field="bpjsCut"/></dd>
					
				</g:if>
			
				<g:if test="${payrollInstance?.deposit}">
					<dt><g:message code="payroll.deposit.label" default="Deposit" /></dt>
					
						<dd><g:fieldValue bean="${payrollInstance}" field="deposit"/></dd>
					
				</g:if>
			
				<g:if test="${payrollInstance?.jamsostekCut}">
					<dt><g:message code="payroll.jamsostekCut.label" default="Jamsostek Cut" /></dt>
					
						<dd><g:fieldValue bean="${payrollInstance}" field="jamsostekCut"/></dd>
					
				</g:if>
			
				<g:if test="${payrollInstance?.monthlyAtt}">
					<dt><g:message code="payroll.monthlyAtt.label" default="Monthly Att" /></dt>
					
						<dd><g:fieldValue bean="${payrollInstance}" field="monthlyAtt"/></dd>
					
				</g:if>
			
				<g:if test="${payrollInstance?.notes}">
					<dt><g:message code="payroll.notes.label" default="Notes" /></dt>
					
						<dd><g:fieldValue bean="${payrollInstance}" field="notes"/></dd>
					
				</g:if>
			
				<g:if test="${payrollInstance?.otherFee}">
					<dt><g:message code="payroll.otherFee.label" default="Other Fee" /></dt>
					
						<dd><g:fieldValue bean="${payrollInstance}" field="otherFee"/></dd>
					
				</g:if>
			
				<g:if test="${payrollInstance?.person}">
					<dt><g:message code="payroll.person.label" default="Person" /></dt>
					
						<dd><g:link controller="person" action="show" id="${payrollInstance?.person?.id}">${payrollInstance?.person?.encodeAsHTML()}</g:link></dd>
					
				</g:if>
			
				<g:if test="${payrollInstance?.pph21}">
					<dt><g:message code="payroll.pph21.label" default="Pph21" /></dt>
					
						<dd><g:fieldValue bean="${payrollInstance}" field="pph21"/></dd>
					
				</g:if>
			
				<g:if test="${payrollInstance?.takeHomePay}">
					<dt><g:message code="payroll.takeHomePay.label" default="Take Home Pay" /></dt>
					
						<dd><g:fieldValue bean="${payrollInstance}" field="takeHomePay"/></dd>
					
				</g:if>
			
				<g:if test="${payrollInstance?.totalAtt}">
					<dt><g:message code="payroll.totalAtt.label" default="Total Att" /></dt>
					
						<dd><g:fieldValue bean="${payrollInstance}" field="totalAtt"/></dd>
					
				</g:if>
			
				<g:if test="${payrollInstance?.totalOt}">
					<dt><g:message code="payroll.totalOt.label" default="Total Ot" /></dt>
					
						<dd><g:fieldValue bean="${payrollInstance}" field="totalOt"/></dd>
					
				</g:if>
			
						</dl>
						<g:form url="[resource:payrollInstance, action:'delete']"
							method="DELETE">
							<fieldset class="buttons">
								<g:link class="btn btn-primary" action="edit"
									resource="${payrollInstance}">
									<g:message code="default.button.edit.label" default="Edit" />
								</g:link>
								<g:actionSubmit class="btn btn-default" action="delete"
									value="${message(code: 'default.button.delete.label', default: 'Delete')}"
									onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
							</fieldset>
						</g:form>
					</div>
				</div>
			</div>
		</div>

	</div>
</body>
</html>
