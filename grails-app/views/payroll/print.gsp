<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Payroll Print</title>
<link rel="license"
	href="http://www.opensource.org/licenses/mit-license/">
<style>

/* heading */
h1 {
	font: bold 36px sans-serif;
	letter-spacing: 0.1em;
	text-align: center;
	text-transform: uppercase;
}

/* page */
html {
	font: 16px/1 'Open Sans', sans-serif;
	overflow: auto;
	padding: 0.5in;
}

html {
	background: #999;
	cursor: default;
}

body {
	box-sizing: border-box;
	margin: 0 auto;
	padding: 0.3in 0.5in;
	width: 8.27in;
}

body {
	background: #FFF;
	border-radius: 1px;
	box-shadow: 0 0 1in -0.25in rgba(0, 0, 0, 0.5);
}

/* header */
header {
	margin: 0 0 1em;
	padding-bottom: 5px;
	border-bottom: solid 1px #999;
}

header:after {
	clear: both;
	content: "";
	display: table;
}

header .header-logo {
	float: left;
	width: 32%;
	text-align: left;
	padding: 1%;
}

header .header-text {
	float: left;
	width: 30%;
	font-size: 28px;
	padding: 2% 2% 0 2%;
	text-align: center;
	padding-top: 46px;
}

header .header-right {
	float: left;
	width: 32%;
	padding-top: 18px;
}

/* article */
article, article address, table.meta, table.inventory {
	margin: 0 0 2em;
}

article:after {
	clear: both;
	content: "";
	display: table;
}

article {
	font-size: 12px;
	line-height: 15px;
}

footer {
	position: relative;
	bottom: 0;
	font-size: 10px;
	border-top: solid 3px #999;
}

/* javascript */
.add, .cut {
	border-width: 1px;
	display: block;
	font-size: .8rem;
	padding: 0.25em 0.5em;
	float: left;
	text-align: center;
	width: 0.6em;
}

.add, .cut {
	background: #9AF;
	box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
	background-image: -moz-linear-gradient(#00ADEE 5%, #0078A5 100%);
	background-image: -webkit-linear-gradient(#00ADEE 5%, #0078A5 100%);
	border-radius: 0.5em;
	border-color: #0076A3;
	color: #FFF;
	cursor: pointer;
	font-weight: bold;
	text-shadow: 0 -1px 2px rgba(0, 0, 0, 0.333);
}

.add {
	margin: -2.5em 0 0;
}

.add:hover {
	background: #00ADEE;
}

.cut {
	opacity: 0;
	position: absolute;
	top: 0;
	left: -1.5em;
}

.cut {
	-webkit-transition: opacity 100ms ease-in;
}

tr:hover .cut {
	opacity: 1;
}

.control {
	position: absolute;
	top: 10px;
	left: 2.3in;
}

.control a {
	padding: 3px 10px;
	background: #ccc;
	font-size: 12px;
	font-weight: bold;
	margin: 5px;
}

.control a:hover {
	background: #fff;
}

.compname {
	font-size: 18px;
	padding: 5px;
	vertical-align: middle
}

.compaddr {
	line-height: 20px;
	font-size: 12px;
}

.hr-left {float-left;
	line-height: 20px;
	font-size: 12px;
	width: 60%;
}

.hr-right {
	float: left;
	line-height: 20px;
	font-size: 12px;
	width: 40%;
}

.detail-table {
	width: 100%;
	border-spacing: 0;
	border-collapse: collapse;
}

.detail-table th {
	border-top: solid 1px #999;
	border-bottom: solid 2px #999;
	margin: 0;
	padding: 3px 6px;
	font-weight: bold;
}

@media all {
	.page-break,.spacer {
		display: none;
	}
}

@media print {
	* {
		-webkit-print-color-adjust: exact;
	}
	html {
		background: none;
		padding: 0;
	}
	body {
		box-shadow: none;
		margin: 0;
	}
	span:empty {
		display: none;
	}
	.add, .cut {
		display: none;
	}
	.control {
		display: none;
	}
	.page-break {
		display: block;
		page-break-before: always;
	}
	.spacer {
		display:block;
		height:0.3in;
	}
}

@page {
	margin: 0;
}
</style>
</head>
<body>
	<div class="control">
		<a href="#" onclick="PrintWebPage()">Print</a>
		<g:link controller="hr">Close</g:link>
	</div>
	<g:each in="${payrollInstanceList}" var="payrollInstance" status="p">
		<header>
			<div class="header-logo">
				<img alt="" src="${assetPath(src: 'mcm.jpg')}"
					style="width: 32px; vertical-align: middle" /> <span
					class="compname">PT.Mitra Citra Mandiri</span><br> <span
					class="compaddr">Ruko Blog Blue Dot Com Paramount Blok D,
					No.26/Type C, Tangerang-Banten</span><br> <span class="compaddr">Phone
					: 62-21 45768979, 081517722257, 0822 9886 9157</span>
			</div>
			<div class="header-text">SLIP GAJI</div>
			<div class="header-right">
				<span class="hr-right">Nomor Slip</span><span class="hr-left">:
					MCMPR${new java.text.DecimalFormat('0000').format(payrollInstance?.id)}
				</span><br> <span class="hr-right">Tgl Dicetak</span><span
					class="hr-left">: <g:formatDate date="${new Date()}"
						format="dd-MMM-yyyy" /></span><br> <span class="hr-right">Periode</span><span
					class="hr-left">: ${payrollInstance?.month}/${payrollInstance?.year}</span><br>
				<span class="hr-right">NIK</span><span class="hr-left">: ${payrollInstance?.person?.pid}</span><br>
				<span class="hr-right">Nama</span><span class="hr-left">: ${payrollInstance?.person?.fullname}</span><br>
			</div>
		</header>
		<article>
			<table class="detail-table">
				<thead>
					<tr>
						<th width="5%">No.</th>
						<th width="70%" style="text-align: left">KETERANGAN</th>
						<th width="25%" style="text-align: right">JUMLAH</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td style="text-align: center; padding: 3px 6px;">1</td>
						<td style="text-align: left; padding: 3px 6px;">Gaji Pokok</td>
						<td style="text-align: right; padding: 3px 6px;"><g:fieldValue
								bean="${payrollInstance}" field="basicSalary" /></td>
					</tr>
					<tr>
						<td style="text-align: center; padding: 3px 6px;">4</td>
						<td style="text-align: left; padding: 3px 6px;">Tunjangan</td>
						<td style="text-align: right; padding: 3px 6px;"><g:fieldValue
								bean="${payrollInstance}" field="toolsEquipment" /></td>
					</tr>
					<tr>
						<td
							style="text-align: center; padding: 3px 6px; border-top: solid 1px #999; border-bottom: solid 1px #999;"></td>
						<td
							style="text-align: left; padding: 3px 6px; border-top: solid 1px #999; border-bottom: solid 1px #999;">Sub
							Total</td>
						<td
							style="text-align: right; padding: 3px 6px; border-top: solid 1px #999; border-bottom: solid 1px #999;"><g:fieldValue
								bean="${payrollInstance}" field="empBenefit" /></td>
					</tr>
					<tr>
						<td style="text-align: center; padding: 3px 6px;">5</td>
						<td style="text-align: left; padding: 3px 6px;">Lembur</td>
						<td style="text-align: right; padding: 3px 6px;"><g:fieldValue
								bean="${payrollInstance}" field="amountOt" /></td>
					</tr>
					<tr>
						<td style="text-align: center; padding: 3px 6px;">6</td>
						<td style="text-align: left; padding: 3px 6px;">Potongan
							Tidak Hadir</td>
						<td style="text-align: right; padding: 3px 6px;"><g:fieldValue
								bean="${payrollInstance}" field="attCut" /></td>
					</tr>
					<tr>
						<td style="text-align: center; padding: 3px 6px;">7</td>
						<td style="text-align: left; padding: 3px 6px;">Potongan
							PPH21</td>
						<td style="text-align: right; padding: 3px 6px;"><g:fieldValue
								bean="${payrollInstance}" field="pph21" /></td>
					</tr>
					<tr>
						<td style="text-align: center; padding: 3px 6px;">8</td>
						<td style="text-align: left; padding: 3px 6px;">Potongan
							Jamsostek</td>
						<td style="text-align: right; padding: 3px 6px;"><g:fieldValue
								bean="${payrollInstance}" field="jamsostekCut" /></td>
					</tr>
					<tr>
						<td style="text-align: center; padding: 3px 6px;">9</td>
						<td style="text-align: left; padding: 3px 6px;">Potongan BPJS</td>
						<td style="text-align: right; padding: 3px 6px;"><g:fieldValue
								bean="${payrollInstance}" field="bpjsCut" /></td>
					</tr>
					<tr>
						<td style="text-align: center; padding: 3px 6px;">10</td>
						<td style="text-align: left; padding: 3px 6px;">Potongan
							Lain-lain</td>
						<td style="text-align: right; padding: 3px 6px;"><g:fieldValue
								bean="${payrollInstance}" field="otherFee" /></td>
					</tr>
					<tr>
						<td style="text-align: center; padding: 3px 6px;">11</td>
						<td style="text-align: left; padding: 3px 6px;">Deposit
							Seragam</td>
						<td style="text-align: right; padding: 3px 6px;"><g:fieldValue
								bean="${payrollInstance}" field="deposit" /></td>
					</tr>
					<tr>
						<td
							style="text-align: center; padding: 6px 6px; border-top: solid 2px #999;"></td>
						<td
							style="text-align: right; padding: 6px 6px; font-size: 14px; border-top: solid 2px #999; font-weight: bold;">TOTAL
							DITERIMA</td>
						<td
							style="text-align: right; padding: 6px 6px; font-size: 14px; border-top: solid 2px #999; font-weight: bold;"><g:fieldValue
								bean="${payrollInstance}" field="takeHomePay" /></td>
					</tr>
				</tbody>
			</table>
			<br>
			<table class="detail-table">
				<tr>
					<td width="50%" style="text-align: center">KARYAWAN</td>
					<td width="50%" style="text-align: center">TANGERANG, <g:formatDate
							date="${new Date()}" format="dd-MMM-yyyy" /></td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td style="text-align: center">
						${payrollInstance?.person?.fullname}
					</td>
					<td style="text-align: center">MANAGER HRD</td>
				</tr>
			</table>
		</article>
		<g:if test="${p%2==1}">
			<div class="page-break"></div>
			<div class="spacer">&nbsp;</div>
		</g:if>
	</g:each>
	<script>
		function PrintWebPage() {
			window.print();
		}
	</script>
</body>
</html>