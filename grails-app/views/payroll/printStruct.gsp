<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<title>Print Struk Gaji</title>
</head>
<body>
	<div class="col-md-2 bootstrap-admin-col-left">
		<ul class="nav navbar-collapse collapse bootstrap-admin-navbar-side">
			<li><g:link action="index">
					<i class="glyphicon glyphicon-list-alt"></i>
					Daftar Payroll
				</g:link></li>
		</ul>
	</div>

	<div class="col-md-10">
		<div class="row">
			<div class="col-lg-12">
				<div class="page-header">
					<h1>Print Struk Gaji</h1>
				</div>
				<g:if test="${flash.message}">
					<div class="page-body">
						<div class="alert alert-success" role="status">
							<a class="close" data-dismiss="alert" href="#">&times;</a>
							${flash.message}
						</div>
					</div>
				</g:if>
			</div>
		</div>

		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default bootstrap-admin-no-table-panel">
					<div
						class="bootstrap-admin-no-table-panel-content bootstrap-admin-panel-content collapse in">
						<g:form url="[resource:payrollInstance, action:'print']"
							class="form-horizontal">
							<fieldset class="form">
								<div class="form-group">
									<label class="col-lg-2 control-label" for="month">
										Pilih Bulan </label>
									<div class="col-lg-10">
										<g:datePicker name="bulanGaji" precision="month" />
									</div>

								</div>

								<div class="form-group">
									<label class="col-lg-2 control-label" for="unit"> <g:message
											code="basicSalary.unit.label" default="Unit" />

									</label>
									<div class="col-lg-10">
										<g:select class="form-control" id="unit" name="unit.id"
											from="${com.osem.Unit.list()}" optionKey="id" required="" />
									</div>

								</div>
							</fieldset>
							<fieldset class="buttons">
								<g:submitButton name="create" class="btn btn-primary"
									value="Print Struk" />
							</fieldset>
						</g:form>
					</div>
				</div>
			</div>
		</div>

	</div>

	<script>
		$('#bulanGaji_month').addClass('form-control');
		$('#bulanGaji_month').css('width', '20%');
		$('#bulanGaji_month').css('float', 'left');
		$('#bulanGaji_year').addClass('form-control');
		$('#bulanGaji_year').css('width', '12%');
		$('#bulanGaji_year').css('float', 'left');
	</script>
</body>
</html>
