<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<g:set var="entityName"
	value="${message(code: 'payroll.label', default: 'Payroll')}" />
<title>Process Payroll</title>
</head>
<body>
	<div class="col-md-2 bootstrap-admin-col-left">
		<ul class="nav navbar-collapse collapse bootstrap-admin-navbar-side">
			<li><g:link action="index">
					<i class="glyphicon glyphicon-list-alt"></i>
					<g:message code="default.list.label" args="[entityName]" />
				</g:link></li>
		</ul>
	</div>

	<div class="col-md-10">
		<div class="row">
			<div class="col-lg-12">
				<div class="page-header">
					<h1>Process Payroll</h1>
				</div>
				<g:if test="${flash.message}">
					<div class="page-body">
						<div class="alert alert-success" role="status">
							<a class="close" data-dismiss="alert" href="#">&times;</a>
							${flash.message}
						</div>
					</div>
				</g:if>
			</div>
		</div>

		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default bootstrap-admin-no-table-panel">
					<div
						class="bootstrap-admin-no-table-panel-content bootstrap-admin-panel-content collapse in">
						<g:form url="[action:'processPayroll']" class="form-horizontal">
							<fieldset class="form">
								<div class="form-group">
									<label class="col-lg-2 control-label" for="month">Bulan/Tahun</label>
									<div class="col-lg-10">
										<g:datePicker name="myDate" value="${new Date()}"
											precision="month" noSelection="['':'-Choose-']"
											relativeYears="[-2..7]" />
									</div>
								</div>

								<div class="form-group">
									<label class="col-lg-2 control-label" for="allUnit">All
										Unit</label>
									<div class="col-lg-10">
										<div class="form-control">
											<g:checkBox name="allUnit" />
										</div>
									</div>
								</div>

								<div class="form-group">
									<label class="col-lg-2 control-label" for="unit">Unit</label>
									<div class="col-lg-10">
										<g:select class="form-control" id="unit" name="unit.id"
											from="${com.osem.Unit.list()}" optionKey="id" />
									</div>
								</div>
								
								<div class="form-group">
									<label class="col-lg-2 control-label" for="unit">Total Hari Kerja</label>
									<div class="col-lg-10">
										<g:field type="number" name="monthlyAtt" class="form-control"/>
									</div>
								</div>
							</fieldset>
							<fieldset class="buttons">
								<g:submitButton name="create" class="btn btn-primary"
									value="${message(code: 'default.button.process.label', default: 'Process')}" />
							</fieldset>
						</g:form>
					</div>
				</div>
			</div>
		</div>

	</div>

	<script>
		$('input:checkbox').iCheck({
			checkboxClass : 'icheckbox_minimal-grey',
			radioClass : 'iradio_minimal-grey'
		});

		$('input:checkbox').on('ifChecked', function(event) {
			$('#unit').attr('disabled','disabled');
		});

		$('input:checkbox').on('ifUnchecked', function(event) {
			$('#unit').attr('disabled',false);
		});

		$('#myDate_month').addClass('form-control');
		$('#myDate_month').addClass('col-lg-2');
		$('#myDate_year').addClass('form-control');
		$('#myDate_year').addClass('col-lg-2');
	</script>
</body>
</html>
