<%@ page import="com.osem.Promotion" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<g:set var="entityName"
	value="${message(code: 'promotion.label', default: 'Promotion')}" />
<title><g:message code="default.edit.label" args="[entityName]" /></title>
</head>
<body>
	<div class="col-md-2 bootstrap-admin-col-left">
		<ul class="nav navbar-collapse collapse bootstrap-admin-navbar-side">
			<li><g:link action="index">
					<i class="glyphicon glyphicon-list-alt"></i>
					<g:message code="default.list.label" args="[entityName]" />
				</g:link></li>
			<li><g:link action="create">
					<i class="glyphicon glyphicon-plus"></i>
					<g:message code="default.new.label" args="[entityName]" />
				</g:link></li>
		</ul>
	</div>

	<div class="col-md-10">
		<div class="row">
			<div class="col-lg-12">
				<div class="page-header">
					<h1>
						<g:message code="default.edit.label" args="[entityName]" />
					</h1>
				</div>
				<g:if test="${flash.message}">
					<div class="page-body">
						<div class="alert alert-success" role="status">
							<a class="close" data-dismiss="alert" href="#">&times;</a>${flash.message}
						</div>
					</div>
				</g:if>
				<g:hasErrors bean="${promotionInstance}">
					<div class="page-body">
						<ul class="alert alert-danger" role="alert">
							<g:eachError bean="${promotionInstance}" var="error">
								<li
									<g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message
										error="${error}" /></li>
							</g:eachError>
						</ul>
					</div>
				</g:hasErrors>
			</div>
		</div>

		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default bootstrap-admin-no-table-panel">
					<div
						class="bootstrap-admin-no-table-panel-content bootstrap-admin-panel-content collapse in">
						<g:form url="[resource:promotionInstance, action:'update']"
							method="PUT" class="form-horizontal"
							>
							<g:hiddenField name="version"
								value="${promotionInstance?.version}" />
							<fieldset class="form">
								<g:render template="form" />
							</fieldset>
							<fieldset class="buttons">
								<g:actionSubmit class="btn btn-primary" action="update"
									value="${message(code: 'default.button.update.label', default: 'Update')}" />
							</fieldset>
						</g:form>
					</div>
				</div>
			</div>
		</div>

	</div>
</body>
</html>
