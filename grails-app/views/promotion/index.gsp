

<%@ page import="com.osem.Promotion" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<g:set var="entityName"
	value="${message(code: 'promotion.label', default: 'Promotion')}" />
<title><g:message code="default.list.label" args="[entityName]" /></title>
</head>
<body>
	<div class="col-md-2 bootstrap-admin-col-left">
		<ul class="nav navbar-collapse collapse bootstrap-admin-navbar-side">
			<li><g:link action="create">
					<i class="glyphicon glyphicon-plus"></i>
					<g:message code="default.new.label" args="[entityName]" />
				</g:link></li>
		</ul>
	</div>

	<div class="col-md-10">
		<div class="row">
			<div class="col-lg-12">
				<div class="page-header">
					<h1>
						<g:message code="default.list.label" args="[entityName]" />
					</h1>
				</div>
				<g:if test="${flash.message}">
					<div class="page-body">
						<div class="alert alert-success" role="status">
							<a class="close" data-dismiss="alert" href="#">&times;</a>${flash.message}
						</div>
					</div>
				</g:if>
			</div>
		</div>

		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="bootstrap-admin-panel-content">
						<g:if test="${promotionInstanceList}">
							<table class="table bootstrap-admin-table-with-actions">
								<thead>
									<tr>
										
						<g:sortableColumn property="createDate" title="${message(code: 'promotion.createDate.label', default: 'Create Date')}" />
					
						<g:sortableColumn property="newPosition" title="${message(code: 'promotion.newPosition.label', default: 'New Position')}" />
					
						<th><g:message code="promotion.newSubUnit.label" default="New Sub Unit" /></th>
					
						<g:sortableColumn property="oldPosition" title="${message(code: 'promotion.oldPosition.label', default: 'Old Position')}" />
					
						<th><g:message code="promotion.oldSubUnit.label" default="Old Sub Unit" /></th>
					
						<th><g:message code="promotion.person.label" default="Person" /></th>
					
									</tr>
								</thead>
								<tbody>
									<g:each in="${promotionInstanceList}" status="i"
										var="promotionInstance">
										<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
											
						<td><g:link action="show" id="${promotionInstance.id}">${fieldValue(bean: promotionInstance, field: "createDate")}</g:link></td>
					
						<td>${fieldValue(bean: promotionInstance, field: "newPosition")}</td>
					
						<td>${fieldValue(bean: promotionInstance, field: "newSubUnit")}</td>
					
						<td>${fieldValue(bean: promotionInstance, field: "oldPosition")}</td>
					
						<td>${fieldValue(bean: promotionInstance, field: "oldSubUnit")}</td>
					
						<td>${fieldValue(bean: promotionInstance, field: "person")}</td>
					
										</tr>
									</g:each>
								</tbody>
							</table>
							<div class="pagination">
								<g:paginate total="${promotionInstanceCount ?: 0}" />
							</div>
						</g:if>
						<g:else>
							No promotion found
						</g:else>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
