<%@ page import="com.osem.Promotion" %>



<div class="form-group fieldcontain ${hasErrors(bean: promotionInstance, field: 'createDate', 'error')} ">
	<label class="col-lg-2 control-label" for="createDate">
		<g:message code="promotion.createDate.label" default="Create Date" />
		
	</label>
	<div class="col-lg-10"><g:datePicker class="form-control" name="createDate" precision="day" value="${promotionInstance?.createDate}" /></div>

</div>

<div class="form-group fieldcontain ${hasErrors(bean: promotionInstance, field: 'newPosition', 'error')} ">
	<label class="col-lg-2 control-label" for="newPosition">
		<g:message code="promotion.newPosition.label" default="New Position" />
		
	</label>
	<div class="col-lg-10"><g:textField class="form-control" name="newPosition" value="${promotionInstance?.newPosition}" /></div>

</div>

<div class="form-group fieldcontain ${hasErrors(bean: promotionInstance, field: 'newSubUnit', 'error')} ">
	<label class="col-lg-2 control-label" for="newSubUnit">
		<g:message code="promotion.newSubUnit.label" default="New Sub Unit" />
		
	</label>
	<div class="col-lg-10"><g:select class="form-control" id="newSubUnit" name="newSubUnit.id" from="${com.osem.SubUnit.list()}" optionKey="id" required="" value="${promotionInstance?.newSubUnit?.id}"/></div>

</div>

<div class="form-group fieldcontain ${hasErrors(bean: promotionInstance, field: 'oldPosition', 'error')} ">
	<label class="col-lg-2 control-label" for="oldPosition">
		<g:message code="promotion.oldPosition.label" default="Old Position" />
		
	</label>
	<div class="col-lg-10"><g:textField class="form-control" name="oldPosition" value="${promotionInstance?.oldPosition}" /></div>

</div>

<div class="form-group fieldcontain ${hasErrors(bean: promotionInstance, field: 'oldSubUnit', 'error')} ">
	<label class="col-lg-2 control-label" for="oldSubUnit">
		<g:message code="promotion.oldSubUnit.label" default="Old Sub Unit" />
		
	</label>
	<div class="col-lg-10"><g:select class="form-control" id="oldSubUnit" name="oldSubUnit.id" from="${com.osem.SubUnit.list()}" optionKey="id" required="" value="${promotionInstance?.oldSubUnit?.id}"/></div>

</div>

<div class="form-group fieldcontain ${hasErrors(bean: promotionInstance, field: 'person', 'error')} ">
	<label class="col-lg-2 control-label" for="person">
		<g:message code="promotion.person.label" default="Person" />
		
	</label>
	<div class="col-lg-10"><g:select class="form-control" id="person" name="person.id" from="${com.osem.Person.list()}" optionKey="id" required="" value="${promotionInstance?.person?.id}"/></div>

</div>

<div class="form-group fieldcontain ${hasErrors(bean: promotionInstance, field: 'promCode', 'error')} ">
	<label class="col-lg-2 control-label" for="promCode">
		<g:message code="promotion.promCode.label" default="Prom Code" />
		
	</label>
	<div class="col-lg-10"><g:textField class="form-control" name="promCode" value="${promotionInstance?.promCode}" /></div>

</div>

<div class="form-group fieldcontain ${hasErrors(bean: promotionInstance, field: 'promDate', 'error')} ">
	<label class="col-lg-2 control-label" for="promDate">
		<g:message code="promotion.promDate.label" default="Prom Date" />
		
	</label>
	<div class="col-lg-10"><g:datePicker class="form-control" name="promDate" precision="day" value="${promotionInstance?.promDate}" /></div>

</div>

<div class="form-group fieldcontain ${hasErrors(bean: promotionInstance, field: 'promNotes', 'error')} ">
	<label class="col-lg-2 control-label" for="promNotes">
		<g:message code="promotion.promNotes.label" default="Prom Notes" />
		
	</label>
	<div class="col-lg-10"><g:textField class="form-control" name="promNotes" value="${promotionInstance?.promNotes}" /></div>

</div>

