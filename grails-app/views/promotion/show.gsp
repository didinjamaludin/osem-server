

<%@ page import="com.osem.Promotion" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<g:set var="entityName"
	value="${message(code: 'promotion.label', default: 'Promotion')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
</head>
<body>
	<div class="col-md-2 bootstrap-admin-col-left">
		<ul class="nav navbar-collapse collapse bootstrap-admin-navbar-side">
			<li><g:link action="index">
					<i class="glyphicon glyphicon-list-alt"></i>
					<g:message code="default.list.label" args="[entityName]" />
				</g:link></li>
			<li><g:link action="create">
					<i class="glyphicon glyphicon-plus"></i>
					<g:message code="default.new.label" args="[entityName]" />
				</g:link></li>
		</ul>
	</div>

	<div class="col-md-10">
		<div class="row">
			<div class="col-lg-12">
				<div class="page-header">
					<h1>
						<g:message code="default.show.label" args="[entityName]" />
					</h1>
				</div>
				<g:if test="${flash.message}">
					<div class="page-body">
						<div class="alert alert-success" role="status">
							<a class="close" data-dismiss="alert" href="#">&times;</a>${flash.message}
						</div>
					</div>
				</g:if>
				<g:hasErrors bean="${promotionInstance}">
					<div class="page-body">
						<ul class="alert alert-danger" role="alert">
							<g:eachError bean="${promotionInstance}" var="error">
								<li
									<g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message
										error="${error}" /></li>
							</g:eachError>
						</ul>
					</div>
				</g:hasErrors>
			</div>
		</div>

		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default bootstrap-admin-no-table-panel">
					<div
						class="bootstrap-admin-no-table-panel-content bootstrap-admin-panel-content collapse in">
						<dl class="dl-horizontal">
							
				<g:if test="${promotionInstance?.createDate}">
					<dt><g:message code="promotion.createDate.label" default="Create Date" /></dt>
					
						<dd><g:formatDate date="${promotionInstance?.createDate}" /></dd>
					
				</g:if>
			
				<g:if test="${promotionInstance?.newPosition}">
					<dt><g:message code="promotion.newPosition.label" default="New Position" /></dt>
					
						<dd><g:fieldValue bean="${promotionInstance}" field="newPosition"/></dd>
					
				</g:if>
			
				<g:if test="${promotionInstance?.newSubUnit}">
					<dt><g:message code="promotion.newSubUnit.label" default="New Sub Unit" /></dt>
					
						<dd><g:link controller="subUnit" action="show" id="${promotionInstance?.newSubUnit?.id}">${promotionInstance?.newSubUnit?.encodeAsHTML()}</g:link></dd>
					
				</g:if>
			
				<g:if test="${promotionInstance?.oldPosition}">
					<dt><g:message code="promotion.oldPosition.label" default="Old Position" /></dt>
					
						<dd><g:fieldValue bean="${promotionInstance}" field="oldPosition"/></dd>
					
				</g:if>
			
				<g:if test="${promotionInstance?.oldSubUnit}">
					<dt><g:message code="promotion.oldSubUnit.label" default="Old Sub Unit" /></dt>
					
						<dd><g:link controller="subUnit" action="show" id="${promotionInstance?.oldSubUnit?.id}">${promotionInstance?.oldSubUnit?.encodeAsHTML()}</g:link></dd>
					
				</g:if>
			
				<g:if test="${promotionInstance?.person}">
					<dt><g:message code="promotion.person.label" default="Person" /></dt>
					
						<dd><g:link controller="person" action="show" id="${promotionInstance?.person?.id}">${promotionInstance?.person?.encodeAsHTML()}</g:link></dd>
					
				</g:if>
			
				<g:if test="${promotionInstance?.promCode}">
					<dt><g:message code="promotion.promCode.label" default="Prom Code" /></dt>
					
						<dd><g:fieldValue bean="${promotionInstance}" field="promCode"/></dd>
					
				</g:if>
			
				<g:if test="${promotionInstance?.promDate}">
					<dt><g:message code="promotion.promDate.label" default="Prom Date" /></dt>
					
						<dd><g:formatDate date="${promotionInstance?.promDate}" /></dd>
					
				</g:if>
			
				<g:if test="${promotionInstance?.promNotes}">
					<dt><g:message code="promotion.promNotes.label" default="Prom Notes" /></dt>
					
						<dd><g:fieldValue bean="${promotionInstance}" field="promNotes"/></dd>
					
				</g:if>
			
						</dl>
						<g:form url="[resource:promotionInstance, action:'delete']"
							method="DELETE">
							<fieldset class="buttons">
								<g:link class="btn btn-primary" action="edit"
									resource="${promotionInstance}">
									<g:message code="default.button.edit.label" default="Edit" />
								</g:link>
								<g:actionSubmit class="btn btn-default" action="delete"
									value="${message(code: 'default.button.delete.label', default: 'Delete')}"
									onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
							</fieldset>
						</g:form>
					</div>
				</div>
			</div>
		</div>

	</div>
</body>
</html>
