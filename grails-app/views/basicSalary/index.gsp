

<%@ page import="com.osem.BasicSalary"%>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<g:set var="entityName"
	value="${message(code: 'basicSalary.label', default: 'BasicSalary')}" />
<title>Daftar Skema Biaya</title>
</head>
<body>
	<div class="col-md-2 bootstrap-admin-col-left">
		<ul class="nav navbar-collapse collapse bootstrap-admin-navbar-side">
			<li><g:link action="create">
					<i class="glyphicon glyphicon-plus"></i>
					<g:message code="default.new.label" args="[entityName]" />
				</g:link></li>
		</ul>
	</div>

	<div class="col-md-10">
		<div class="row">
			<div class="col-lg-12">
				<div class="page-header">
					<h1>Daftar Skema Biaya</h1>
				</div>
				<g:if test="${flash.message}">
					<div class="page-body">
						<div class="alert alert-success" role="status">
							<a class="close" data-dismiss="alert" href="#">&times;</a>
							${flash.message}
						</div>
					</div>
				</g:if>
			</div>
		</div>

		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="bootstrap-admin-panel-content">
						<g:if test="${basicSalaryInstanceList}">
							<table class="table bootstrap-admin-table-with-actions">
								<thead>
									<tr>

										<th>Unit</th>

										<th>Posisi</th>

										<th>Basic Salary</th>

										<th>Net Claimed</th>

									</tr>
								</thead>
								<tbody>
									<g:each in="${basicSalaryInstanceList}" status="i"
										var="basicSalaryInstance">
										<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">

											<td><g:link action="show" id="${basicSalaryInstance.id}">
													${fieldValue(bean: basicSalaryInstance, field: "unit")}
												</g:link></td>

											<td><g:link action="show" id="${basicSalaryInstance.id}">
													${fieldValue(bean: basicSalaryInstance, field: "position")}
												</g:link></td>

											<td>
												${fieldValue(bean: basicSalaryInstance, field: "basicSalary")}
											</td>

											<td>
												${fieldValue(bean: basicSalaryInstance, field: "netClaimed")}
											</td>

										</tr>
									</g:each>
								</tbody>
							</table>
							<div class="pagination">
								<g:paginate total="${basicSalaryInstanceCount ?: 0}" />
							</div>
						</g:if>
						<g:else>
							No basicSalary found
						</g:else>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
