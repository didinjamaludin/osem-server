

<%@ page import="com.osem.BasicSalary"%>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<g:set var="entityName"
	value="${message(code: 'basicSalary.label', default: 'BasicSalary')}" />
<title>Show Skema Basic Salary</title>
</head>
<body>
	<div class="col-md-2 bootstrap-admin-col-left">
		<ul class="nav navbar-collapse collapse bootstrap-admin-navbar-side">
			<li><g:link action="index">
					<i class="glyphicon glyphicon-list-alt"></i>
					<g:message code="default.list.label" args="[entityName]" />
				</g:link></li>
			<li><g:link action="create">
					<i class="glyphicon glyphicon-plus"></i>
					<g:message code="default.new.label" args="[entityName]" />
				</g:link></li>
		</ul>
	</div>

	<div class="col-md-10">
		<div class="row">
			<div class="col-lg-12">
				<div class="page-header">
					<h1>Show Skema Basic Salary</h1>
				</div>
				<g:if test="${flash.message}">
					<div class="page-body">
						<div class="alert alert-success" role="status">
							<a class="close" data-dismiss="alert" href="#">&times;</a>
							${flash.message}
						</div>
					</div>
				</g:if>
				<g:hasErrors bean="${basicSalaryInstance}">
					<div class="page-body">
						<ul class="alert alert-danger" role="alert">
							<g:eachError bean="${basicSalaryInstance}" var="error">
								<li
									<g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message
										error="${error}" /></li>
							</g:eachError>
						</ul>
					</div>
				</g:hasErrors>
			</div>
		</div>

		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default bootstrap-admin-no-table-panel">
					<div
						class="bootstrap-admin-no-table-panel-content bootstrap-admin-panel-content collapse in">
						<dl class="dl-horizontal">

							<g:if test="${basicSalaryInstance?.unit}">
								<dt>
									<g:message code="basicSalary.unit.label" default="Unit" />
								</dt>

								<dd>
									<g:link controller="unit" action="show"
										id="${basicSalaryInstance?.unit?.id}">
										${basicSalaryInstance?.unit?.encodeAsHTML()}
									</g:link>
								</dd>

							</g:if>

							<g:if test="${basicSalaryInstance?.position}">
								<dt>
									<g:message code="basicSalary.position.label" default="Position" />
								</dt>

								<dd>
									<g:link controller="position" action="show"
										id="${basicSalaryInstance?.position?.id}">
										${basicSalaryInstance?.position?.encodeAsHTML()}
									</g:link>
								</dd>

							</g:if>
						</dl>
						<hr>
						<dl class="dl-horizontal">
							<g:if test="${basicSalaryInstance?.basicSalary}">
								<dt>
									<g:message code="basicSalary.basicSalary.label"
										default="Basic Salary" />
								</dt>

								<dd>
									<g:fieldValue bean="${basicSalaryInstance}" field="basicSalary" />
								</dd>

							</g:if>

							<g:if test="${basicSalaryInstance?.mealAllow}">
								<dt>
									<g:message code="basicSalary.mealAllow.label"
										default="Meal Allow" />
								</dt>

								<dd>
									<g:fieldValue bean="${basicSalaryInstance}" field="mealAllow" />
								</dd>

							</g:if>

							<g:if test="${basicSalaryInstance?.transAllow}">
								<dt>
									<g:message code="basicSalary.transAllow.label"
										default="Trans Allow" />
								</dt>

								<dd>
									<g:fieldValue bean="${basicSalaryInstance}" field="transAllow" />
								</dd>

							</g:if>

							<g:if test="${basicSalaryInstance?.commAllow}">
								<dt>
									<g:message code="basicSalary.commAllow.label"
										default="Comm Allow" />
								</dt>

								<dd>
									<g:fieldValue bean="${basicSalaryInstance}" field="commAllow" />
								</dd>

							</g:if>

							<g:if test="${basicSalaryInstance?.uniformAllow}">
								<dt>
									<g:message code="basicSalary.uniformAllow.label"
										default="Uniform Allow" />
								</dt>

								<dd>
									<g:fieldValue bean="${basicSalaryInstance}"
										field="uniformAllow" />
								</dd>

							</g:if>

							<g:if test="${basicSalaryInstance?.toolsEquipment}">
								<dt>
									<g:message code="basicSalary.toolsEquipment.label"
										default="Tools Equipment" />
								</dt>

								<dd>
									<g:fieldValue bean="${basicSalaryInstance}"
										field="toolsEquipment" />
								</dd>

							</g:if>

							<g:if test="${basicSalaryInstance?.overtime}">
								<dt>
									<g:message code="basicSalary.overtime.label" default="Overtime" />
								</dt>

								<dd>
									<g:formatBoolean boolean="${basicSalaryInstance?.overtime}" />
								</dd>

							</g:if>


							<g:if test="${basicSalaryInstance?.empBenefit}">
								<dt>
									<g:message code="basicSalary.empBenefit.label"
										default="Emp Benefit" />
								</dt>

								<dd>
									<strong><g:fieldValue bean="${basicSalaryInstance}"
											field="empBenefit" /></strong>
								</dd>

							</g:if>
						</dl>
						<hr>
						<dl class="dl-horizontal">

							<g:if test="${basicSalaryInstance?.jamsostek}">
								<dt>
									<g:message code="basicSalary.jamsostek.label"
										default="Jamsostek" />
								</dt>

								<dd>
									<g:fieldValue bean="${basicSalaryInstance}" field="jamsostek" />
								</dd>

							</g:if>

							<g:if test="${basicSalaryInstance?.bpjsKesehatan}">
								<dt>
									<g:message code="basicSalary.bpjsKesehatan.label"
										default="Bpjs Kesehatan" />
								</dt>

								<dd>
									<g:fieldValue bean="${basicSalaryInstance}"
										field="bpjsKesehatan" />
								</dd>

							</g:if>

							<g:if test="${basicSalaryInstance?.thr}">
								<dt>
									<g:message code="basicSalary.thr.label" default="Thr" />
								</dt>

								<dd>
									<g:formatBoolean boolean="${basicSalaryInstance?.thr}" />
								</dd>

							</g:if>

							<g:if test="${basicSalaryInstance?.subTotal}">
								<dt>
									<g:message code="basicSalary.subTotal.label"
										default="Sub Total" />
								</dt>

								<dd>
									<Strong><g:fieldValue bean="${basicSalaryInstance}"
											field="subTotal" /></Strong>
								</dd>

							</g:if>
						</dl>
						<hr>
						<dl class="dl-horizontal">

							<g:if test="${basicSalaryInstance?.fee}">
								<dt>
									<g:message code="basicSalary.fee.label" default="Fee" />
								</dt>

								<dd>
									<g:fieldValue bean="${basicSalaryInstance}" field="fee" />
								</dd>

							</g:if>

							<g:if test="${basicSalaryInstance?.manFee}">
								<dt>
									<g:message code="basicSalary.manFee.label" default="Man Fee" />
								</dt>

								<dd>
									<g:fieldValue bean="${basicSalaryInstance}" field="manFee" />
								</dd>

							</g:if>

							<g:if test="${basicSalaryInstance?.totalClaim}">
								<dt>
									<g:message code="basicSalary.totalClaim.label"
										default="Total Claim" />
								</dt>

								<dd>
									<strong><g:fieldValue bean="${basicSalaryInstance}"
											field="totalClaim" /></strong>
								</dd>

							</g:if>

						</dl>
						<hr>
						<dl class="dl-horizontal">

							<g:if test="${basicSalaryInstance?.ppn10}">
								<dt>
									<g:message code="basicSalary.ppn10.label" default="Ppn10" />
								</dt>

								<dd>
									<g:fieldValue bean="${basicSalaryInstance}" field="ppn10" />
								</dd>

							</g:if>

							<g:if test="${basicSalaryInstance?.grandTotal}">
								<dt>
									<g:message code="basicSalary.grandTotal.label"
										default="Grand Total" />
								</dt>

								<dd>
									<g:fieldValue bean="${basicSalaryInstance}" field="grandTotal" />
								</dd>

							</g:if>

							<g:if test="${basicSalaryInstance?.pph23}">
								<dt>
									<g:message code="basicSalary.pph23.label" default="Pph23" />
								</dt>

								<dd>
									<g:fieldValue bean="${basicSalaryInstance}" field="pph23" />
								</dd>

							</g:if>


							<g:if test="${basicSalaryInstance?.empCount}">
								<dt>
									<g:message code="basicSalary.empCount.label"
										default="Jumlah Pegawai" />
								</dt>

								<dd>
									<g:fieldValue bean="${basicSalaryInstance}" field="empCount" />
								</dd>

							</g:if>

							<g:if test="${basicSalaryInstance?.netClaimed}">
								<dt>
									<g:message code="basicSalary.netClaimed.label"
										default="Net Claimed" />
								</dt>

								<dd>
									<g:fieldValue bean="${basicSalaryInstance}" field="netClaimed"
										style="font-size:20px;" />
								</dd>

							</g:if>

						</dl>
						<hr>
						<dl class="dl-horizontal">
							<dt>
								<g:message code="basicSalary.pph21.label" default="PPH21" />
							</dt>

							<dd>
								<g:fieldValue bean="${basicSalaryInstance}" field="pph21"
									style="font-size:20px;" />
							</dd>

							<dt>
								<g:message code="basicSalary.jamsostekCut.label"
									default="Pot Jamsostek" />
							</dt>

							<dd>
								<g:fieldValue bean="${basicSalaryInstance}" field="jamsostekCut"
									style="font-size:20px;" />
							</dd>

							<dt>
								<g:message code="basicSalary.bpjsCut.label" default="Pot BPJS" />
							</dt>

							<dd>
								<g:fieldValue bean="${basicSalaryInstance}" field="bpjsCut"
									style="font-size:20px;" />
							</dd>

							<dt>
								<g:message code="basicSalary.otherFee.label"
									default="Biaya Lain2" />
							</dt>

							<dd>
								<g:fieldValue bean="${basicSalaryInstance}" field="otherFee"
									style="font-size:20px;" />
							</dd>

							<dt>
								<g:message code="basicSalary.deposit.label"
									default="Deposit Seragam" />
							</dt>

							<dd>
								<g:fieldValue bean="${basicSalaryInstance}" field="deposit"
									style="font-size:20px;" />
							</dd>

							<dt>
								<g:message code="basicSalary.takeHomePay.label"
									default="Take Home Pay" />
							</dt>

							<dd>
								<strong><g:fieldValue bean="${basicSalaryInstance}"
										field="takeHomePay" style="font-size:20px;" /></strong>
							</dd>
						</dl>

						<g:form url="[resource:basicSalaryInstance, action:'delete']"
							method="DELETE">
							<fieldset class="buttons">
								<g:link class="btn btn-primary" action="edit"
									resource="${basicSalaryInstance}">
									<g:message code="default.button.edit.label" default="Edit" />
								</g:link>
								<g:actionSubmit class="btn btn-default" action="delete"
									value="${message(code: 'default.button.delete.label', default: 'Delete')}"
									onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
							</fieldset>
						</g:form>
					</div>
				</div>
			</div>
		</div>

	</div>
</body>
</html>
