<%@ page import="com.osem.BasicSalary"%>

<fieldset>
	<legend>Dari Unit/Perusahaan</legend>
	
<div
	class="form-group fieldcontain ${hasErrors(bean: basicSalaryInstance, field: 'unit', 'error')} ">
	<label class="col-lg-2 control-label" for="unit"> <g:message
			code="basicSalary.unit.label" default="Unit" />

	</label>
	<div class="col-lg-10">
		<g:select class="form-control" id="unit" name="unit.id"
			from="${com.osem.Unit.list()}" optionKey="id" required=""
			value="${basicSalaryInstance?.unit?.id}" />
	</div>

</div>

<div
	class="form-group fieldcontain ${hasErrors(bean: basicSalaryInstance, field: 'position', 'error')} ">
	<label class="col-lg-2 control-label" for="position"> <g:message
			code="basicSalary.position.label" default="Position" />

	</label>
	<div class="col-lg-10">
		<g:select class="form-control" id="position" name="position.id"
			from="${com.osem.Position.list()}" optionKey="id" required=""
			value="${basicSalaryInstance?.position?.id}" />
	</div>

</div>

<hr>

<div
	class="form-group fieldcontain ${hasErrors(bean: basicSalaryInstance, field: 'basicSalary', 'error')} ">
	<label class="col-lg-2 control-label" for="basicSalary"> <g:message
			code="basicSalary.basicSalary.label" default="Basic Salary" />

	</label>
	<div class="col-lg-10">
		<g:field class="form-control" min="0" type="number" name="basicSalary"
			pattern="0" value="${basicSalaryInstance.basicSalary}"
			onkeyup="countBenefit()" onblur="countTunjangan(this.value)" />
	</div>

</div>

<div
	class="form-group fieldcontain ${hasErrors(bean: basicSalaryInstance, field: 'mealAllow', 'error')} ">
	<label class="col-lg-2 control-label" for="mealAllow"> <g:message
			code="basicSalary.mealAllow.label" default="Meal Allow" />

	</label>
	<div class="col-lg-10">
		<g:field class="form-control" min="0" type="number" name="mealAllow"
			value="${basicSalaryInstance.mealAllow}" onkeyup="countBenefit()" />
	</div>

</div>


<div
	class="form-group fieldcontain ${hasErrors(bean: basicSalaryInstance, field: 'transAllow', 'error')} ">
	<label class="col-lg-2 control-label" for="transAllow"> <g:message
			code="basicSalary.transAllow.label" default="Trans Allow" />

	</label>
	<div class="col-lg-10">
		<g:field class="form-control" min="0" type="number" name="transAllow"
			value="${basicSalaryInstance.transAllow}" onkeyup="countBenefit()" />
	</div>

</div>

<div
	class="form-group fieldcontain ${hasErrors(bean: basicSalaryInstance, field: 'commAllow', 'error')} ">
	<label class="col-lg-2 control-label" for="commAllow"> <g:message
			code="basicSalary.commAllow.label" default="Comm Allow" />

	</label>
	<div class="col-lg-10">
		<g:field class="form-control" min="0" type="number" name="commAllow"
			value="${basicSalaryInstance.commAllow}" onkeyup="countBenefit()" />
	</div>

</div>

<div
	class="form-group fieldcontain ${hasErrors(bean: basicSalaryInstance, field: 'uniformAllow', 'error')} ">
	<label class="col-lg-2 control-label" for="uniformAllow"> <g:message
			code="basicSalary.uniformAllow.label" default="Uniform Allow" />

	</label>
	<div class="col-lg-10">
		<g:field class="form-control" min="0" type="number"
			name="uniformAllow" value="${basicSalaryInstance.uniformAllow}"
			onkeyup="countBenefit()" />
	</div>

</div>

<div
	class="form-group fieldcontain ${hasErrors(bean: basicSalaryInstance, field: 'toolsEquipment', 'error')} ">
	<label class="col-lg-2 control-label" for="toolsEquipment"> <g:message
			code="basicSalary.toolsEquipment.label" default="Tools Equipment" />

	</label>
	<div class="col-lg-10">
		<g:field class="form-control" min="0" type="number"
			name="toolsEquipment" value="${basicSalaryInstance.toolsEquipment}"
			onkeyup="countBenefit()" />
	</div>

</div>


<div
	class="form-group fieldcontain ${hasErrors(bean: basicSalaryInstance, field: 'overtime', 'error')} ">
	<label class="col-lg-2 control-label" for="overtime"> <g:message
			code="basicSalary.overtime.label" default="Overtime" />

	</label>
	<div class="col-lg-10">
		<g:checkBox name="overtime" value="${basicSalaryInstance?.overtime}" />
	</div>

</div>

<div
	class="form-group fieldcontain ${hasErrors(bean: basicSalaryInstance, field: 'empBenefit', 'error')} ">
	<label class="col-lg-2 control-label" for="empBenefit"> <g:message
			code="basicSalary.empBenefit.label" default="Emp Benefit" />

	</label>
	<div class="col-lg-10">
		<span id="showBenefit" style="font-size: 20px"><g:if test="${basicSalaryInstance.empBenefit}"><g:formatNumber number="${basicSalaryInstance.empBenefit}" /></g:if><g:else>0</g:else></span>
		<g:hiddenField min="0" type="number" name="empBenefit"
			value="${basicSalaryInstance.empBenefit}" />
	</div>

</div>

<hr>

<div
	class="form-group fieldcontain ${hasErrors(bean: basicSalaryInstance, field: 'jamsostek', 'error')} ">
	<label class="col-lg-2 control-label" for="jamsostek"> <g:message
			code="basicSalary.jamsostek.label" default="Jamsostek" />

	</label>
	<div class="col-lg-10">
		<span id="showJamsos" style="font-size: 20px"><g:if test="${basicSalaryInstance.jamsostek}"><g:formatNumber number="${basicSalaryInstance.jamsostek}" /></g:if><g:else>0</g:else></span>
		<g:hiddenField min="0" type="number" name="jamsostek"
			value="${basicSalaryInstance.jamsostek}" onkeyup="countSubTotal()" />
	</div>

</div>

<div
	class="form-group fieldcontain ${hasErrors(bean: basicSalaryInstance, field: 'bpjsKesehatan', 'error')} ">
	<label class="col-lg-2 control-label" for="bpjsKesehatan"> <g:message
			code="basicSalary.bpjsKesehatan.label" default="Bpjs Kesehatan" />

	</label>
	<div class="col-lg-10">
		<span id="showBpjs" style="font-size: 20px"><g:if test="${basicSalaryInstance.bpjsKesehatan}"><g:formatNumber number="${basicSalaryInstance.bpjsKesehatan}" /></g:if><g:else>0</g:else></span>
		<g:hiddenField min="0" type="number" name="bpjsKesehatan"
			value="${basicSalaryInstance.bpjsKesehatan}"
			onkeyup="countSubTotal()" />
	</div>

</div>


<div
	class="form-group fieldcontain ${hasErrors(bean: basicSalaryInstance, field: 'thr', 'error')} ">
	<label class="col-lg-2 control-label" for="thr"> <g:message
			code="basicSalary.thr.label" default="Thr" />

	</label>
	<div class="col-lg-10">
		<g:checkBox name="thr" value="${basicSalaryInstance?.thr}" />
	</div>

</div>

<div
	class="form-group fieldcontain ${hasErrors(bean: basicSalaryInstance, field: 'subTotal', 'error')} ">
	<label class="col-lg-2 control-label" for="subTotal"> <g:message
			code="basicSalary.subTotal.label" default="Sub Total" />

	</label>
	<div class="col-lg-10">
		<span id="showSub" style="font-size: 20px"><g:if test="${basicSalaryInstance.subTotal}"><g:formatNumber number="${basicSalaryInstance.subTotal}" /></g:if><g:else>0</g:else></span>
		<g:hiddenField min="0" type="number" name="subTotal"
			value="${basicSalaryInstance.subTotal}" />
	</div>

</div>

<hr>

<div
	class="form-group fieldcontain ${hasErrors(bean: basicSalaryInstance, field: 'fee', 'error')} ">
	<label class="col-lg-2 control-label" for="fee"> <g:message
			code="basicSalary.fee.label" default="Fee" /> (%)

	</label>
	<div class="col-lg-10">
		<g:field class="form-control" min="0" type="number" name="fee"
			value="${basicSalaryInstance.fee}" onkeyup="countManfee(this.value)" />
	</div>

</div>

<div
	class="form-group fieldcontain ${hasErrors(bean: basicSalaryInstance, field: 'manFee', 'error')} ">
	<label class="col-lg-2 control-label" for="manFee"> <g:message
			code="basicSalary.manFee.label" default="Man Fee" />

	</label>
	<div class="col-lg-10">
		<span id="showManfee" style="font-size: 20px"><g:if test="${basicSalaryInstance.manFee}"><g:formatNumber number="${basicSalaryInstance.manFee}" /></g:if><g:else>0</g:else></span>
		<g:hiddenField min="0" type="number" name="manFee"
			value="${basicSalaryInstance.manFee}" />
	</div>

</div>


<div
	class="form-group fieldcontain ${hasErrors(bean: basicSalaryInstance, field: 'totalClaim', 'error')} ">
	<label class="col-lg-2 control-label" for="totalClaim"> <g:message
			code="basicSalary.totalClaim.label" default="Total Claim" />

	</label>
	<div class="col-lg-10">
		<span id="showTotalclaim" style="font-size: 20px"><g:if test="${basicSalaryInstance.totalClaim}"><g:formatNumber number="${basicSalaryInstance.totalClaim}" /></g:if><g:else>0</g:else></span>
		<g:hiddenField min="0" type="number" name="totalClaim"
			value="${basicSalaryInstance.totalClaim}" />
	</div>

</div>

<hr>

<div
	class="form-group fieldcontain ${hasErrors(bean: basicSalaryInstance, field: 'ppn10', 'error')} ">
	<label class="col-lg-2 control-label" for="ppn10"> <g:message
			code="basicSalary.ppn10.label" default="Ppn10" />

	</label>
	<div class="col-lg-10">
		<span id="showPpn10" style="font-size: 20px"><g:if test="${basicSalaryInstance.ppn10}"><g:formatNumber number="${basicSalaryInstance.ppn10}" /></g:if><g:else>0</g:else></span>
		<g:hiddenField min="0" type="number" name="ppn10"
			value="${basicSalaryInstance.ppn10}" />
	</div>

</div>

<div
	class="form-group fieldcontain ${hasErrors(bean: basicSalaryInstance, field: 'grandTotal', 'error')} ">
	<label class="col-lg-2 control-label" for="grandTotal"> <g:message
			code="basicSalary.grandTotal.label" default="Grand Total" />

	</label>
	<div class="col-lg-10">
		<span id="showGrand" style="font-size: 20px"><g:if test="${basicSalaryInstance.grandTotal}"><g:formatNumber number="${basicSalaryInstance.grandTotal}" /></g:if><g:else>0</g:else></span>
		<g:hiddenField min="0" type="number" name="grandTotal"
			value="${basicSalaryInstance.grandTotal}" />
	</div>

</div>

<div
	class="form-group fieldcontain ${hasErrors(bean: basicSalaryInstance, field: 'pph23', 'error')} ">
	<label class="col-lg-2 control-label" for="pph23"> <g:message
			code="basicSalary.pph23.label" default="Pph23" />

	</label>
	<div class="col-lg-10">
		<span id="showPph23" style="font-size: 20px"><g:if test="${basicSalaryInstance.pph23}"><g:formatNumber number="${basicSalaryInstance.pph23}" /></g:if><g:else>0</g:else></span>
		<g:hiddenField min="0" type="number" name="pph23"
			value="${basicSalaryInstance.pph23}" />
	</div>

</div>

<div
	class="form-group fieldcontain ${hasErrors(bean: basicSalaryInstance, field: 'empCount', 'error')} ">
	<label class="col-lg-2 control-label" for="empCount"> <g:message
			code="basicSalary.empCount.label" default="Jumlah Pegawai" />

	</label>
	<div class="col-lg-10">
		<g:field class="form-control" min="0" type="number" name="empCount"
			value="${basicSalaryInstance.empCount}"
			onkeyup="coutNetClaimed(this.value)" />
	</div>

</div>

<div
	class="form-group fieldcontain ${hasErrors(bean: basicSalaryInstance, field: 'netClaimed', 'error')} ">
	<label class="col-lg-2 control-label" for="netClaimed"> <g:message
			code="basicSalary.netClaimed.label" default="Net Claimed" />

	</label>
	<div class="col-lg-10">
		<span id="showNetto" style="font-size: 20px"><g:if test="${basicSalaryInstance.netClaimed}"><g:formatNumber number="${basicSalaryInstance.netClaimed}" /></g:if><g:else>0</g:else></span>
		<g:hiddenField min="0" type="number" name="netClaimed"
			value="${basicSalaryInstance.netClaimed}" />
	</div>

</div>

</fieldset>

<hr>

<fieldset>
	<legend>Kepada Karyawan</legend>
	<div
		class="form-group fieldcontain ${hasErrors(bean: basicSalaryInstance, field: 'pph21', 'error')} ">
		<label class="col-lg-2 control-label" for="pph21"> <g:message
				code="takeHomePay.pph21.label" default="PPH21" />

		</label>
		<div class="col-lg-10">
			<g:field class="form-control" type="number" name="pph21"
				value="${basicSalaryInstance.pph21}" />
		</div>

	</div>

	<div
		class="form-group fieldcontain ${hasErrors(bean: basicSalaryInstance, field: 'jamsostekCut', 'error')} ">
		<label class="col-lg-2 control-label" for="jamsostekCut"> <g:message
				code="takeHomePay.jamsostekCut.label" default="Pot Jamsostek 2%" />

		</label>
		<div class="col-lg-10">
			<span id="showJamsCut" style="font-size: 20px"><g:if test="${basicSalaryInstance.jamsostekCut}"><g:formatNumber number="${basicSalaryInstance.jamsostekCut}" /></g:if><g:else>0</g:else></span>
			<g:hiddenField class="form-control" type="number" name="jamsostekCut"
				value="${basicSalaryInstance.jamsostekCut}" />
		</div>

	</div>

	<div
		class="form-group fieldcontain ${hasErrors(bean: basicSalaryInstance, field: 'bpjsCut', 'error')} ">
		<label class="col-lg-2 control-label" for="bpjsCut"> <g:message
				code="takeHomePay.bpjsCut.label" default="Pot BPJS" />

		</label>
		<div class="col-lg-10">
			<span id="showBpjsCut" style="font-size: 20px"><g:if test="${basicSalaryInstance.bpjsCut}"><g:formatNumber number="${basicSalaryInstance.bpjsCut}" /></g:if><g:else>0</g:else></span>
			<g:hiddenField class="form-control" type="number" name="bpjsCut"
				value="${basicSalaryInstance.bpjsCut}" />
		</div>

	</div>

	<div
		class="form-group fieldcontain ${hasErrors(bean: basicSalaryInstance, field: 'otherFee', 'error')} ">
		<label class="col-lg-2 control-label" for="otherFee"> <g:message
				code="takeHomePay.otherFee.label" default="Biaya Lain2" />

		</label>
		<div class="col-lg-10">
			<g:field class="form-control" type="number" name="otherFee"
				value="${basicSalaryInstance.otherFee}" onkeyup="countThp()" />
		</div>

	</div>

	<div
		class="form-group fieldcontain ${hasErrors(bean: basicSalaryInstance, field: 'deposit', 'error')} ">
		<label class="col-lg-2 control-label" for="deposit"> <g:message
				code="takeHomePay.deposit.label" default="Deposit" />

		</label>
		<div class="col-lg-10">
			<g:field class="form-control" type="number" name="deposit"
				value="${basicSalaryInstance.deposit}" onkeyup="countThp()" />
		</div>

	</div>

	<div
		class="form-group fieldcontain ${hasErrors(bean: basicSalaryInstance, field: 'takeHomePay', 'error')} ">
		<label class="col-lg-2 control-label" for="takeHomePay"> <g:message
				code="takeHomePay.takeHomePay.label" default="Take Home Pay" />

		</label>
		<div class="col-lg-10">
			<span id="showThp" style="font-size: 20px"><g:if test="${basicSalaryInstance.takeHomePay}"><g:formatNumber number="${basicSalaryInstance.takeHomePay}" /></g:if><g:else>0</g:else></span>
			<g:hiddenField class="form-control" type="number" name="takeHomePay"
				value="${basicSalaryInstance.takeHomePay}" />
		</div>

	</div>

</fieldset>

<script>
	function countBenefit() {
		var basic = parseInt($("#basicSalary").val());
		var meal = parseInt($("#mealAllow").val());
		var trans = parseInt($("#transAllow").val());
		var comm = parseInt($("#commAllow").val());
		var uniform = parseInt($("#uniformAllow").val());
		var tools = parseInt($("#toolsEquipment").val());
		var benefit = basic + meal + trans + comm + uniform + tools;
		$("#showBenefit").text(benefit);
		$("#empBenefit").val(benefit);
		
		var jamscut = (benefit*2)/100;
		var bpjscut = (benefit*0.5)/100;

		$("#showJamsCut").text(jamscut);
		$("#jamsostekCut").val(jamscut);
		$("#showBpjsCut").text(bpjscut);
		$("#bpjsCut").val(bpjscut);

		countSubTotal();
	}

	function countTunjangan(value) {
		var basic = parseInt(value);
		var jamsos = (basic * 4.24) / 100;
		$("#showJamsos").text(jamsos);
		$("#jamsostek").val(jamsos);

		var bpjs = (basic * 4) / 100;
		$("#showBpjs").text(bpjs);
		$("#bpjsKesehatan").val(bpjs);

		countSubTotal();
	}

	function countSubTotal() {
		var benefit = parseInt($("#empBenefit").val());
		var jamsos = parseInt($("#jamsostek").val());
		var bpjs = parseInt($("#bpjsKesehatan").val());
		var subTotal = benefit + jamsos + bpjs;
		$("#showSub").text(subTotal);
		$("#subTotal").val(subTotal);
		countTotalClaimed();
	}

	function countManfee(value) {
		var fee = parseInt(value);
		var subTot = parseInt($("#subTotal").val());
		var manfee = (subTot * fee) / 100;
		$("#showManfee").text(manfee);
		$("#manFee").val(manfee);
		countTotalClaimed();
	}

	function countTotalClaimed() {
		var subtotal = parseInt($("#subTotal").val());
		var manfee = parseInt($("#manFee").val());
		var total = subtotal + manfee;
		$("#showTotalclaim").text(total);
		$("#totalClaim").val(total);
		countPpn10(total);
	}

	function countPpn10(grand) {
		var manfee = parseInt($("#manFee").val());
		var ppn = (manfee * 10) / 100;
		$("#showPpn10").text(ppn);
		$("#ppn10").val(ppn);

		var grandtotal = parseInt(grand) + ppn;
		$("#showGrand").text(grandtotal);
		$("#grandTotal").val(grandtotal);

		var pph23 = (manfee * 2) / 100;
		$("#showPph23").text(pph23);
		$("#pph23").val(pph23);
	}

	function coutNetClaimed(value) {
		var emp = parseInt(value);
		var grand = parseInt($("#grandTotal").val());
		var pph23 = parseInt($("#pph23").val());
		var net = emp * (grand - pph23);
		$("#showNetto").text(net);
		$("#netClaimed").val(net);
	}
	
	function countThp() {
		var pph21 = parseInt($("#pph21").val());
		var jamscut = parseInt($("#jamsostekCut").val());
		var bpjscut = parseInt($("#bpjsCut").val());
		var others = parseInt($("#otherFee").val());
		var deposit = parseInt($("#deposit").val());
		var empbenefit = parseInt($("#empBenefit").val());
		
		var thp = empbenefit - (pph21+jamscut+bpjscut+others+deposit);
		$("#showThp").text(thp);
		$("#takeHomePay").val(thp);
	}

	$(function() {
		if($("#basicSalary").val()==""||$("#basicSalary").val()==null)
		$("#basicSalary").val("0");
		if($("#mealAllow").val()==""||$("#mealAllow").val()==null)
		$("#mealAllow").val("0");
		if($("#transAllow").val()==""||$("#transAllow").val()==null)
		$("#transAllow").val("0");
		if($("#commAllow").val()==""||$("#commAllow").val()==null)
		$("#commAllow").val("0");
		if($("#uniformAllow").val()==""||$("#uniformAllow").val()==null)
		$("#uniformAllow").val("0");
		if($("#toolsEquipment").val()==""||$("#toolsEquipment").val()==null)
		$("#toolsEquipment").val("0");
		if($("#empBenefit").val()==""||$("#empBenefit").val()==null)
		$("#empBenefit").val("0");
		if($("#jamsostek").val()==""||$("#jamsostek").val()==null)
		$("#jamsostek").val("0");
		if($("#bpjsKesehatan").val()==""||$("#bpjsKesehatan").val()==null)
		$("#bpjsKesehatan").val("0");
		if($("#subTotal").val()==""||$("#subTotal").val()==null)
		$("#subTotal").val("0");
		if($("#fee").val()==""||$("#fee").val()==null)
		$("#fee").val("0");
		if($("#manFee").val()==""||$("#manFee").val()==null)
		$("#manFee").val("0");
		if($("#totalClaim").val()==""||$("#totalClaim").val()==null)
		$("#totalClaim").val("0");
		if($("#ppn10").val()==""||$("#ppn10").val()==null)
		$("#ppn10").val("0");
		if($("#grandTotal").val()==""||$("#grandTotal").val()==null)
		$("#grandTotal").val("0");
		if($("#pph23").val()==""||$("#pph23").val()==null)
		$("#pph23").val("0");
		if($("#empCount").val()==""||$("#empCount").val()==null)
		$("#empCount").val("0");
		if($("#netClaimed").val()==""||$("#netClaimed").val()==null)
		$("#netClaimed").val("0");
		if($("#pph21").val()==""||$("#pph21").val()==null)
		$("#pph21").val("0");
		if($("#jamsostekCut").val()==""||$("#jamsostekCut").val()==null)
		$("#jamsostekCut").val("0");
		if($("#bpjsCut").val()==""||$("#bpjsCut").val()==null)
		$("#bpjsCut").val("0");
		if($("#otherFee").val()==""||$("#otherFee").val()==null)
		$("#otherFee").val("0");
		if($("#deposit").val()==""||$("#deposit").val()==null)
		$("#deposit").val("0");
		if($("#takeHomePay").val()==""||$("#takeHomePay").val()==null)
		$("#takeHomePay").val("0");
	});
</script>

