

<%@ page import="com.osem.SubUnit"%>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<g:set var="entityName"
	value="${message(code: 'subUnit.label', default: 'SubUnit')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
</head>
<body>
	<div class="col-md-2 bootstrap-admin-col-left">
		<ul class="nav navbar-collapse collapse bootstrap-admin-navbar-side">
			<li><g:link action="index">
					<i class="glyphicon glyphicon-list-alt"></i>
					<g:message code="default.list.label" args="[entityName]" />
				</g:link></li>
			<li><g:link action="create">
					<i class="glyphicon glyphicon-plus"></i>
					<g:message code="default.new.label" args="[entityName]" />
				</g:link></li>
		</ul>
	</div>

	<div class="col-md-10">
		<div class="row">
			<div class="col-lg-12">
				<div class="page-header">
					<h1>
						<g:message code="default.show.label" args="[entityName]" />
					</h1>
				</div>
				<g:if test="${flash.message}">
					<div class="page-body">
						<div class="alert alert-success" role="status">
							<a class="close" data-dismiss="alert" href="#">&times;</a>
							${flash.message}
						</div>
					</div>
				</g:if>
				<g:hasErrors bean="${subUnitInstance}">
					<div class="page-body">
						<ul class="alert alert-danger" role="alert">
							<g:eachError bean="${subUnitInstance}" var="error">
								<li
									<g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message
										error="${error}" /></li>
							</g:eachError>
						</ul>
					</div>
				</g:hasErrors>
			</div>
		</div>

		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default bootstrap-admin-no-table-panel">
					<div
						class="bootstrap-admin-no-table-panel-content bootstrap-admin-panel-content collapse in">
						<dl class="dl-horizontal">

							<g:if test="${subUnitInstance?.unit}">
								<dt>
									<g:message code="subUnit.unit.label" default="Unit" />
								</dt>

								<dd>
									<g:link controller="unit" action="show"
										id="${subUnitInstance?.unit?.id}">
										${subUnitInstance?.unit?.encodeAsHTML()}
									</g:link>
								</dd>

							</g:if>

							<g:if test="${subUnitInstance?.subUnitCode}">
								<dt>
									<g:message code="subUnit.subUnitCode.label"
										default="Kode Sub Unit" />
								</dt>

								<dd>
									<g:fieldValue bean="${subUnitInstance}" field="subUnitCode" />
								</dd>

							</g:if>

							<g:if test="${subUnitInstance?.subUnitName}">
								<dt>
									<g:message code="subUnit.subUnitName.label"
										default="Nama Sub Unit" />
								</dt>

								<dd>
									<g:fieldValue bean="${subUnitInstance}" field="subUnitName" />
								</dd>

							</g:if>

							<g:if test="${subUnitInstance?.subUnitAddress}">
								<dt>
									<g:message code="subUnit.subUnitAddress.label"
										default="Alamat Sub Unit" />
								</dt>

								<dd>
									<g:fieldValue bean="${subUnitInstance}" field="subUnitAddress" />
								</dd>

							</g:if>

						</dl>
						<g:form url="[resource:subUnitInstance, action:'delete']"
							method="DELETE">
							<fieldset class="buttons">
								<g:link class="btn btn-primary" action="edit"
									resource="${subUnitInstance}">
									<g:message code="default.button.edit.label" default="Edit" />
								</g:link>
								<g:actionSubmit class="btn btn-default" action="delete"
									value="${message(code: 'default.button.delete.label', default: 'Delete')}"
									onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
							</fieldset>
						</g:form>
					</div>
				</div>
			</div>
		</div>

	</div>
</body>
</html>
