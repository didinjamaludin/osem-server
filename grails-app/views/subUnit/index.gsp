

<%@ page import="com.osem.SubUnit"%>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<g:set var="entityName"
	value="${message(code: 'subUnit.label', default: 'SubUnit')}" />
<title><g:message code="default.list.label" args="[entityName]" /></title>
</head>
<body>
	<div class="col-md-2 bootstrap-admin-col-left">
		<ul class="nav navbar-collapse collapse bootstrap-admin-navbar-side">
			<li><g:link action="create">
					<i class="glyphicon glyphicon-plus"></i>
					<g:message code="default.new.label" args="[entityName]" />
				</g:link></li>
		</ul>
	</div>

	<div class="col-md-10">
		<div class="row">
			<div class="col-lg-12">
				<div class="page-header">
					<h1>
						<g:message code="default.list.label" args="[entityName]" />
					</h1>
				</div>
				<g:if test="${flash.message}">
					<div class="page-body">
						<div class="alert alert-success" role="status">
							<a class="close" data-dismiss="alert" href="#">&times;</a>
							${flash.message}
						</div>
					</div>
				</g:if>
			</div>
		</div>

		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="bootstrap-admin-panel-content">
						<g:if test="${subUnitInstanceList}">
							<table class="table bootstrap-admin-table-with-actions">
								<thead>
									<tr>

										<th><g:message code="subUnit.unit.label" default="Unit" /></th>

										<g:sortableColumn property="subUnitCode"
											title="${message(code: 'subUnit.subUnitCode.label', default: 'Sub Unit Code')}" />

										<g:sortableColumn property="subUnitName"
											title="${message(code: 'subUnit.subUnitName.label', default: 'Sub Unit Name')}" />

										<g:sortableColumn property="subUnitAddress"
											title="${message(code: 'subUnit.subUnitAddress.label', default: 'Sub Unit Address')}" />

									</tr>
								</thead>
								<tbody>
									<g:each in="${subUnitInstanceList}" status="i"
										var="subUnitInstance">
										<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">

											<td>
												${fieldValue(bean: subUnitInstance, field: "unit")}
											</td>

											<td><g:link action="show" id="${subUnitInstance.id}">
													${fieldValue(bean: subUnitInstance, field: "subUnitCode")}
												</g:link></td>

											<td>
												${fieldValue(bean: subUnitInstance, field: "subUnitName")}
											</td>

											<td>
												${fieldValue(bean: subUnitInstance, field: "subUnitAddress")}
											</td>

										</tr>
									</g:each>
								</tbody>
							</table>
							<div class="pagination">
								<g:paginate total="${subUnitInstanceCount ?: 0}" />
							</div>
						</g:if>
						<g:else>
							No subUnit found
						</g:else>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
