<%@ page import="com.osem.SubUnit"%>

<div
	class="form-group fieldcontain ${hasErrors(bean: subUnitInstance, field: 'unit', 'error')} ">
	<label class="col-lg-2 control-label" for="unit"> <g:message
			code="subUnit.unit.label" default="Unit" />

	</label>
	<div class="col-lg-10">
		<g:select class="form-control" id="unit" name="unit.id"
			from="${com.osem.Unit.list()}" optionKey="id" required=""
			value="${subUnitInstance?.unit?.id}" />
	</div>

</div>

<div
	class="form-group fieldcontain ${hasErrors(bean: subUnitInstance, field: 'subUnitCode', 'error')} ">
	<label class="col-lg-2 control-label" for="subUnitCode"> <g:message
			code="subUnit.subUnitCode.label" default="Kode Sub Unit" />

	</label>
	<div class="col-lg-10">
		<g:textField class="form-control" name="subUnitCode"
			value="${subUnitInstance?.subUnitCode}" />
	</div>

</div>

<div
	class="form-group fieldcontain ${hasErrors(bean: subUnitInstance, field: 'subUnitName', 'error')} ">
	<label class="col-lg-2 control-label" for="subUnitName"> <g:message
			code="subUnit.subUnitName.label" default="Nama Sub Unit" />

	</label>
	<div class="col-lg-10">
		<g:textField class="form-control" name="subUnitName"
			value="${subUnitInstance?.subUnitName}" />
	</div>

</div>

<div
	class="form-group fieldcontain ${hasErrors(bean: subUnitInstance, field: 'subUnitAddress', 'error')} ">
	<label class="col-lg-2 control-label" for="subUnitAddress"> <g:message
			code="subUnit.subUnitAddress.label" default="Alamat Sub Unit" />

	</label>
	<div class="col-lg-10">
		<g:textField class="form-control" name="subUnitAddress"
			value="${subUnitInstance?.subUnitAddress}" />
	</div>

</div>