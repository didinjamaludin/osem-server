

<%@ page import="com.osem.Permanent" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<g:set var="entityName"
	value="${message(code: 'permanent.label', default: 'Permanent')}" />
<title><g:message code="default.list.label" args="[entityName]" /></title>
</head>
<body>
	<div class="col-md-2 bootstrap-admin-col-left">
		<ul class="nav navbar-collapse collapse bootstrap-admin-navbar-side">
			<li><g:link action="create">
					<i class="glyphicon glyphicon-plus"></i>
					<g:message code="default.new.label" args="[entityName]" />
				</g:link></li>
		</ul>
	</div>

	<div class="col-md-10">
		<div class="row">
			<div class="col-lg-12">
				<div class="page-header">
					<h1>
						<g:message code="default.list.label" args="[entityName]" />
					</h1>
				</div>
				<g:if test="${flash.message}">
					<div class="page-body">
						<div class="alert alert-success" role="status">
							<a class="close" data-dismiss="alert" href="#">&times;</a>${flash.message}
						</div>
					</div>
				</g:if>
			</div>
		</div>

		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="bootstrap-admin-panel-content">
						<g:if test="${permanentInstanceList}">
							<table class="table bootstrap-admin-table-with-actions">
								<thead>
									<tr>
										
						<g:sortableColumn property="createDate" title="${message(code: 'permanent.createDate.label', default: 'Create Date')}" />
					
						<g:sortableColumn property="permDate" title="${message(code: 'permanent.permDate.label', default: 'Perm Date')}" />
					
						<g:sortableColumn property="permNotes" title="${message(code: 'permanent.permNotes.label', default: 'Perm Notes')}" />
					
						<th><g:message code="permanent.person.label" default="Person" /></th>
					
									</tr>
								</thead>
								<tbody>
									<g:each in="${permanentInstanceList}" status="i"
										var="permanentInstance">
										<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
											
						<td><g:link action="show" id="${permanentInstance.id}">${fieldValue(bean: permanentInstance, field: "createDate")}</g:link></td>
					
						<td><g:formatDate date="${permanentInstance.permDate}" /></td>
					
						<td>${fieldValue(bean: permanentInstance, field: "permNotes")}</td>
					
						<td>${fieldValue(bean: permanentInstance, field: "person")}</td>
					
										</tr>
									</g:each>
								</tbody>
							</table>
							<div class="pagination">
								<g:paginate total="${permanentInstanceCount ?: 0}" />
							</div>
						</g:if>
						<g:else>
							No permanent found
						</g:else>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
