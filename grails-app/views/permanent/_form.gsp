<%@ page import="com.osem.Permanent" %>



<div class="form-group fieldcontain ${hasErrors(bean: permanentInstance, field: 'createDate', 'error')} ">
	<label class="col-lg-2 control-label" for="createDate">
		<g:message code="permanent.createDate.label" default="Create Date" />
		
	</label>
	<div class="col-lg-10"><g:datePicker class="form-control" name="createDate" precision="day" value="${permanentInstance?.createDate}" /></div>

</div>

<div class="form-group fieldcontain ${hasErrors(bean: permanentInstance, field: 'permDate', 'error')} ">
	<label class="col-lg-2 control-label" for="permDate">
		<g:message code="permanent.permDate.label" default="Perm Date" />
		
	</label>
	<div class="col-lg-10"><g:datePicker class="form-control" name="permDate" precision="day" value="${permanentInstance?.permDate}" /></div>

</div>

<div class="form-group fieldcontain ${hasErrors(bean: permanentInstance, field: 'permNotes', 'error')} ">
	<label class="col-lg-2 control-label" for="permNotes">
		<g:message code="permanent.permNotes.label" default="Perm Notes" />
		
	</label>
	<div class="col-lg-10"><g:textField class="form-control" name="permNotes" value="${permanentInstance?.permNotes}" /></div>

</div>

<div class="form-group fieldcontain ${hasErrors(bean: permanentInstance, field: 'person', 'error')} ">
	<label class="col-lg-2 control-label" for="person">
		<g:message code="permanent.person.label" default="Person" />
		
	</label>
	<div class="col-lg-10"><g:select class="form-control" id="person" name="person.id" from="${com.osem.Person.list()}" optionKey="id" required="" value="${permanentInstance?.person?.id}"/></div>

</div>

