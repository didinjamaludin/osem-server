package com.osem



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional
import grails.plugin.springsecurity.annotation.Secured

@Secured(['ROLE_HR'])
@Transactional(readOnly = true)
class WarningTypeController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond WarningType.list(params), model:[warningTypeInstanceCount: WarningType.count()]
    }

    def show(WarningType warningTypeInstance) {
        respond warningTypeInstance
    }

    def create() {
        respond new WarningType(params)
    }

    @Transactional
    def save(WarningType warningTypeInstance) {
        if (warningTypeInstance == null) {
            notFound()
            return
        }

        if (warningTypeInstance.hasErrors()) {
            respond warningTypeInstance.errors, view:'create'
            return
        }

        warningTypeInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'warningType.label', default: 'WarningType'), warningTypeInstance.id])
                redirect warningTypeInstance
            }
            '*' { respond warningTypeInstance, [status: CREATED] }
        }
    }

    def edit(WarningType warningTypeInstance) {
        respond warningTypeInstance
    }

    @Transactional
    def update(WarningType warningTypeInstance) {
        if (warningTypeInstance == null) {
            notFound()
            return
        }

        if (warningTypeInstance.hasErrors()) {
            respond warningTypeInstance.errors, view:'edit'
            return
        }

        warningTypeInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'WarningType.label', default: 'WarningType'), warningTypeInstance.id])
                redirect warningTypeInstance
            }
            '*'{ respond warningTypeInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(WarningType warningTypeInstance) {

        if (warningTypeInstance == null) {
            notFound()
            return
        }

        warningTypeInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'WarningType.label', default: 'WarningType'), warningTypeInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'warningType.label', default: 'WarningType'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
