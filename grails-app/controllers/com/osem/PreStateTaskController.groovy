package com.osem



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional
import grails.plugin.springsecurity.annotation.Secured
import java.text.DecimalFormat
import com.osem.auth.User

@Secured(['ROLE_HR'])
@Transactional(readOnly = true)
class PreStateTaskController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]
	
    def springSecurityService

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond PreStateTask.list(params), model:[preStateTaskInstanceCount: PreStateTask.count()]
    }

    def show(PreStateTask preStateTaskInstance) {
        respond preStateTaskInstance
    }

    def create() {
        def user = User.get(springSecurityService.currentUser.id)
        def date = new Date()
        def month = date.getAt(Calendar.MONTH)
        def rmonth
        switch(month) {
        case 0:
            rmonth = "I"
            break
        case 1:
            rmonth = "II"
            break
        case 2:
            rmonth = "III"
            break
        case 3:
            rmonth = "IV"
            break
        case 4:
            rmonth = "V"
            break
        case 5:
            rmonth = "VI"
            break
        case 6:
            rmonth = "VII"
            break
        case 7:
            rmonth = "VIII"
            break
        case 8:
            rmonth = "IX"
            break
        case 9:
            rmonth = "X"
            break
        case 10:
            rmonth = "XI"
            break
        case 11:
            rmonth = "XII"
        }
        def pst = PreStateTask.last()
        def pstNbr

        if(pst) {
            Integer pseq = pst.pstCode[22..24].toInteger()
            def newseq = pseq+1
            if(pst.pstCode[14..15]==rmonth&&pst.pstCode[17..20]==date.getAt(Calendar.YEAR).toString()) {
                pstNbr = "SIAPTUGAS/"+user.company.compInitial+"/"+rmonth+"/"+date.getAt(Calendar.YEAR)+"/"+new DecimalFormat("000").format(newseq)
            } else {
                pstNbr = "SIAPTUGAS/"+user.company.compInitial+"/"+rmonth+"/"+date.getAt(Calendar.YEAR)+"/001"
            }
        } else {
            pstNbr = "SIAPTUGAS/"+user.company.compInitial+"/"+rmonth+"/"+date.getAt(Calendar.YEAR)+"/001"
        }
        respond new PreStateTask(params), model:[userInstance:user,pstNbr:pstNbr]
    }

    @Transactional
    def save(PreStateTask preStateTaskInstance) {
        def user = User.get(springSecurityService.currentUser.id)
        
        if (preStateTaskInstance == null) {
            notFound()
            return
        }

        if (preStateTaskInstance.hasErrors()) {
            respond preStateTaskInstance.errors, view:'create'
            return
        }

        preStateTaskInstance.save flush:true
		
        def lpquery = Person.withCriteria {
            isNotNull("pid")
            maxResults(1)
            order("id","desc")
        }
        def lastperson = lpquery.getAt(0)
        def date = new Date()
        def nik
        if(lastperson) {
            Integer lastnik = lastperson.pid[4..7].toInteger()
            def newlast = lastnik+1
            if(lastperson.pid[2..3]==date.getAt(Calendar.YEAR).toString()[2..3]) {
                nik = "03"+date.getAt(Calendar.YEAR).toString()[2..3]+new DecimalFormat("0000").format(newlast)
            } else {
                nik = "03"+date.getAt(Calendar.YEAR).toString()[2..3]+"0001"
            }
        } else {
            nik = "03"+date.getAt(Calendar.YEAR).toString()[2..3]+"0001"
        }
		
        def personInstance = Person.get(preStateTaskInstance.person.id)
        personInstance.pid = nik
        personInstance.empStatus = "SIAP TUGAS"
        personInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'preStateTask.label', default: 'PreStateTask'), preStateTaskInstance.id])
                if(params.printPst=="on") {
                    render view:"printPst",model:[userInstance:user,preStateTaskInstance:preStateTaskInstance]
                } else {
                    redirect controller: "hr"
                }
            }
            '*' { respond preStateTaskInstance, [status: CREATED] }
        }
    }

    def edit(PreStateTask preStateTaskInstance) {
        respond preStateTaskInstance
    }

    @Transactional
    def update(PreStateTask preStateTaskInstance) {
        if (preStateTaskInstance == null) {
            notFound()
            return
        }

        if (preStateTaskInstance.hasErrors()) {
            respond preStateTaskInstance.errors, view:'edit'
            return
        }

        preStateTaskInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'PreStateTask.label', default: 'PreStateTask'), preStateTaskInstance.id])
                redirect preStateTaskInstance
            }
            '*'{ respond preStateTaskInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(PreStateTask preStateTaskInstance) {

        if (preStateTaskInstance == null) {
            notFound()
            return
        }

        preStateTaskInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'PreStateTask.label', default: 'PreStateTask'), preStateTaskInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'preStateTask.label', default: 'PreStateTask'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
