package com.osem

import grails.plugin.springsecurity.annotation.Secured
import com.osem.auth.User

@Secured(['ROLE_HR'])
class HrController {
	
	def springSecurityService

    def index(Integer max) {
		def subunit = SubUnit.findByUnitAndSubUnitCode(params.byunit,params.subunit)

		def query = {
			and {
				eq('company',User.get(springSecurityService.currentUser.id).company)
				if(subunit) {
					eq('subunit',subunit)
				}
				if(params.bynik) {
					like('pid',params.bynik+"%")
				}
				if(params.byname) {
					ilike('fullname',params.byname+"%")
				}
				if(params.byposisi) {
					eq('position',Position.findByPosName(params.byposisi))
				}
				if(params.bystatus) {
					ilike('empStatus',params.bystatus+"%")
				}
			}
		}
		def criteria = Person.createCriteria()
		params.max = Math.min(params.max ? params.int('max') : 10, 100)
		def personInstanceList = criteria.list(query, max: params.max, offset: params.offset)
		def filters = [byunit:params.byunit,bysubunit:params.bysubunit,bynik:params.bynik,byname:params.byname,byposisi:params.byposisi,bystatus:params.bystatus]
		def model = [personInstanceList: personInstanceList, personInstanceCount: personInstanceList.totalCount, filters:filters]

		if(request.xhr) {
			render(template: "persongrid", model: model)
		} else {
			model
		}
	}
}
