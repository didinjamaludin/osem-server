package com.osem



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional
import grails.plugin.springsecurity.annotation.Secured

@Secured(['ROLE_HR'])
@Transactional(readOnly = true)
class DeptController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond Dept.list(params), model:[deptInstanceCount: Dept.count()]
    }

    def show(Dept deptInstance) {
        respond deptInstance
    }

    def create() {
        respond new Dept(params)
    }

    @Transactional
    def save(Dept deptInstance) {
        if (deptInstance == null) {
            notFound()
            return
        }

        if (deptInstance.hasErrors()) {
            respond deptInstance.errors, view:'create'
            return
        }

        deptInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'dept.label', default: 'Dept'), deptInstance.id])
                redirect deptInstance
            }
            '*' { respond deptInstance, [status: CREATED] }
        }
    }

    def edit(Dept deptInstance) {
        respond deptInstance
    }

    @Transactional
    def update(Dept deptInstance) {
        if (deptInstance == null) {
            notFound()
            return
        }

        if (deptInstance.hasErrors()) {
            respond deptInstance.errors, view:'edit'
            return
        }

        deptInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'Dept.label', default: 'Dept'), deptInstance.id])
                redirect deptInstance
            }
            '*'{ respond deptInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(Dept deptInstance) {

        if (deptInstance == null) {
            notFound()
            return
        }

        deptInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Dept.label', default: 'Dept'), deptInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'dept.label', default: 'Dept'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
