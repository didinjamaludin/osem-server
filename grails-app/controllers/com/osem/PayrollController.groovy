package com.osem



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

import java.text.DecimalFormat

import com.osem.auth.User

import grails.plugin.springsecurity.annotation.Secured

@Secured(['ROLE_HR'])
@Transactional(readOnly = true)
class PayrollController {

	static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

	def index(Integer max) {
		params.max = Math.min(max ?: 10, 100)
		respond Payroll.list(params), model:[payrollInstanceCount: Payroll.count()]
	}

	def show(Payroll payrollInstance) {
		respond payrollInstance
	}

	def create() {
		respond new Payroll(params)
	}

	def process() {
	}

	def processPayroll() {
		if(params.allUnit=='on') {
			println "on"
		} else {
			def unit = Unit.get(params.unit.id)

			def personList = Person.findAllByUnit(unit)

			personList.each { p ->
				def basic = BasicSalary.findByUnitAndPosition(unit,p.position)
				if(basic) {
					def proll = Payroll.findByMonthAndYearAndPerson(params.myDate_month,params.myDate_year,p)
					if(!proll) {
						new Payroll(person:p,month:params.myDate_month,year:params.myDate_year,basicSalary:basic.basicSalary,mealAllow:basic.mealAllow,transAllow:basic.transAllow,commAllow:basic.commAllow,uniformAllow:basic.uniformAllow,toolsEquipment:basic.toolsEquipment,empBenefit:basic.empBenefit,monthlyAtt:params.monthlyAtt,totalAtt:0,attCut:0,totalOt:0,amountOt:0,pph21:basic.pph21,jamsostekCut:basic.jamsostekCut,bpjsCut:basic.bpjsCut,otherFee:basic.otherFee,deposit:basic.deposit,takeHomePay:basic.takeHomePay).save flush:true,failOnError:true
					}
				}
			}
		}

		flash.message = "Penggajian berhasil di proses, silahkan edit untuk menambahkan/mengurangi"
		redirect action:"index"
	}

	@Transactional
	def save(Payroll payrollInstance) {
		if (payrollInstance == null) {
			notFound()
			return
		}

		if (payrollInstance.hasErrors()) {
			respond payrollInstance.errors, view:'create'
			return
		}

		payrollInstance.save flush:true

		request.withFormat {
			form multipartForm {
				flash.message = message(code: 'default.created.message', args: [
					message(code: 'payroll.label', default: 'Payroll'),
					payrollInstance.id
				])
				redirect payrollInstance
			}
			'*' { respond payrollInstance, [status: CREATED] }
		}
	}

	def edit(Payroll payrollInstance) {
		respond payrollInstance
	}

	@Transactional
	def update(Payroll payrollInstance) {
		if (payrollInstance == null) {
			notFound()
			return
		}

		if (payrollInstance.hasErrors()) {
			respond payrollInstance.errors, view:'edit'
			return
		}

		payrollInstance.save flush:true

		request.withFormat {
			form multipartForm {
				flash.message = message(code: 'default.updated.message', args: [
					message(code: 'Payroll.label', default: 'Payroll'),
					payrollInstance.id
				])
				redirect action:'index'
			}
			'*'{ respond payrollInstance, [status: OK] }
		}
	}

	@Transactional
	def delete(Payroll payrollInstance) {

		if (payrollInstance == null) {
			notFound()
			return
		}

		payrollInstance.delete flush:true

		request.withFormat {
			form multipartForm {
				flash.message = message(code: 'default.deleted.message', args: [
					message(code: 'Payroll.label', default: 'Payroll'),
					payrollInstance.id
				])
				redirect action:"index", method:"GET"
			}
			'*'{ render status: NO_CONTENT }
		}
	}
	
	def printStruct() {
		
	}
	
	def print() {
		def payrollInstance = Payroll.withCriteria {
			and {
				eq('month',params.bulanGaji_month.toInteger())
				eq('year',params.bulanGaji_year.toInteger())
				'in'('person',Person.findAllByUnit(Unit.get(params.unit.id)))
			}
		}
		
		render view:"print",model:[payrollInstanceList:payrollInstance]
	}

	protected void notFound() {
		request.withFormat {
			form multipartForm {
				flash.message = message(code: 'default.not.found.message', args: [
					message(code: 'payroll.label', default: 'Payroll'),
					params.id
				])
				redirect action: "index", method: "GET"
			}
			'*'{ render status: NOT_FOUND }
		}
	}
}
