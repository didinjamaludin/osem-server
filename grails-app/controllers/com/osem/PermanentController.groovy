package com.osem



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional
import grails.plugin.springsecurity.annotation.Secured

@Secured(['ROLE_HR'])
@Transactional(readOnly = true)
class PermanentController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond Permanent.list(params), model:[permanentInstanceCount: Permanent.count()]
    }

    def show(Permanent permanentInstance) {
        respond permanentInstance
    }

    def create() {
        respond new Permanent(params)
    }

    @Transactional
    def save(Permanent permanentInstance) {
        if (permanentInstance == null) {
            notFound()
            return
        }

        if (permanentInstance.hasErrors()) {
            respond permanentInstance.errors, view:'create'
            return
        }

        permanentInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'permanent.label', default: 'Permanent'), permanentInstance.id])
                redirect permanentInstance
            }
            '*' { respond permanentInstance, [status: CREATED] }
        }
    }

    def edit(Permanent permanentInstance) {
        respond permanentInstance
    }

    @Transactional
    def update(Permanent permanentInstance) {
        if (permanentInstance == null) {
            notFound()
            return
        }

        if (permanentInstance.hasErrors()) {
            respond permanentInstance.errors, view:'edit'
            return
        }

        permanentInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'Permanent.label', default: 'Permanent'), permanentInstance.id])
                redirect permanentInstance
            }
            '*'{ respond permanentInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(Permanent permanentInstance) {

        if (permanentInstance == null) {
            notFound()
            return
        }

        permanentInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Permanent.label', default: 'Permanent'), permanentInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'permanent.label', default: 'Permanent'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
