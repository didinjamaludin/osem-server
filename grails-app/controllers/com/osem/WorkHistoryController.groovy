package com.osem



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional
import grails.plugin.springsecurity.annotation.Secured

@Secured(['ROLE_HR'])
@Transactional(readOnly = true)
class WorkHistoryController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond WorkHistory.list(params), model:[workHistoryInstanceCount: WorkHistory.count()]
    }

    def show(WorkHistory workHistoryInstance) {
        respond workHistoryInstance
    }

    def create() {
        respond new WorkHistory(params)
    }

    @Transactional
    def save(WorkHistory workHistoryInstance) {
        if (workHistoryInstance == null) {
            notFound()
            return
        }

        if (workHistoryInstance.hasErrors()) {
            respond workHistoryInstance.errors, view:'create'
            return
        }

        workHistoryInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'workHistory.label', default: 'WorkHistory'), workHistoryInstance.id])
                redirect controller:"person",action:"show",id:workHistoryInstance.person.id
            }
            '*' { respond workHistoryInstance, [status: CREATED] }
        }
    }

    def edit(WorkHistory workHistoryInstance) {
        respond workHistoryInstance
    }

    @Transactional
    def update(WorkHistory workHistoryInstance) {
        if (workHistoryInstance == null) {
            notFound()
            return
        }

        if (workHistoryInstance.hasErrors()) {
            respond workHistoryInstance.errors, view:'edit'
            return
        }

        workHistoryInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'WorkHistory.label', default: 'WorkHistory'), workHistoryInstance.id])
                redirect workHistoryInstance
            }
            '*'{ respond workHistoryInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(WorkHistory workHistoryInstance) {

        if (workHistoryInstance == null) {
            notFound()
            return
        }

        workHistoryInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'WorkHistory.label', default: 'WorkHistory'), workHistoryInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'workHistory.label', default: 'WorkHistory'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
