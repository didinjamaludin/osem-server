package com.osem



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional
import grails.plugin.springsecurity.annotation.Secured

@Secured(['ROLE_HR'])
@Transactional(readOnly = true)
class WarningController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond Warning.list(params), model:[warningInstanceCount: Warning.count()]
    }

    def show(Warning warningInstance) {
        respond warningInstance
    }

    def create() {
        respond new Warning(params)
    }

    @Transactional
    def save(Warning warningInstance) {
        if (warningInstance == null) {
            notFound()
            return
        }

        if (warningInstance.hasErrors()) {
            respond warningInstance.errors, view:'create'
            return
        }

        warningInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'warning.label', default: 'Warning'), warningInstance.id])
                redirect warningInstance
            }
            '*' { respond warningInstance, [status: CREATED] }
        }
    }

    def edit(Warning warningInstance) {
        respond warningInstance
    }

    @Transactional
    def update(Warning warningInstance) {
        if (warningInstance == null) {
            notFound()
            return
        }

        if (warningInstance.hasErrors()) {
            respond warningInstance.errors, view:'edit'
            return
        }

        warningInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'Warning.label', default: 'Warning'), warningInstance.id])
                redirect warningInstance
            }
            '*'{ respond warningInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(Warning warningInstance) {

        if (warningInstance == null) {
            notFound()
            return
        }

        warningInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Warning.label', default: 'Warning'), warningInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'warning.label', default: 'Warning'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
