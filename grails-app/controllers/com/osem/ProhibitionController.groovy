package com.osem



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional
import grails.plugin.springsecurity.annotation.Secured
import java.text.DecimalFormat
import com.osem.auth.User

@Secured(['ROLE_HR'])
@Transactional(readOnly = true)
class ProhibitionController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]
	
    def springSecurityService

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond Prohibition.list(params), model:[prohibitionInstanceCount: Prohibition.count()]
    }

    def show(Prohibition prohibitionInstance) {
        respond prohibitionInstance
    }

    def create() {
        def user = User.get(springSecurityService.currentUser.id)
        def date = new Date()
        def month = date.getAt(Calendar.MONTH)
        def rmonth
        switch(month) {
        case 0:
            rmonth = "I"
            break
        case 1:
            rmonth = "II"
            break
        case 2:
            rmonth = "III"
            break
        case 3:
            rmonth = "IV"
            break
        case 4:
            rmonth = "V"
            break
        case 5:
            rmonth = "VI"
            break
        case 6:
            rmonth = "VII"
            break
        case 7:
            rmonth = "VIII"
            break
        case 8:
            rmonth = "IX"
            break
        case 9:
            rmonth = "X"
            break
        case 10:
            rmonth = "XI"
            break
        case 11:
            rmonth = "XII"
        }
        def prob = Prohibition.last()
        def probNbr

        if(prob) {
            def probCodes = prob.probCode.split("/")
            Integer aseq = probCodes[4].toInteger()
            def newseq = aseq+1
            if(probCodes[2]==rmonth&&probCodes[3]==date.getAt(Calendar.YEAR).toString()) {
                probNbr = "PERCOBAAN/"+user.company.compInitial+"/"+rmonth+"/"+date.getAt(Calendar.YEAR)+"/"+new DecimalFormat("000").format(newseq)
            } else {
                probNbr = "PERCOBAAN/"+user.company.compInitial+"/"+rmonth+"/"+date.getAt(Calendar.YEAR)+"/001"
            }
        } else {
            probNbr = "PERCOBAAN/"+user.company.compInitial+"/"+rmonth+"/"+date.getAt(Calendar.YEAR)+"/001"
        }
		
        respond new Prohibition(params), model:[userInstance:user,probNbr:probNbr]
    }

    @Transactional
    def save(Prohibition prohibitionInstance) {
        if (prohibitionInstance == null) {
            notFound()
            return
        }

        if (prohibitionInstance.hasErrors()) {
            respond prohibitionInstance.errors, view:'create'
            return
        }

        prohibitionInstance.save flush:true
		
        def personInstance = Person.get(prohibitionInstance.person.id)
        personInstance.empStatus = "PERCOBAAN"
        personInstance.save flush:true,failOnError:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'prohibition.label', default: 'Prohibition'), prohibitionInstance.id])
                redirect controller: "hr"
            }
            '*' { respond prohibitionInstance, [status: CREATED] }
        }
    }

    def edit(Prohibition prohibitionInstance) {
        respond prohibitionInstance
    }

    @Transactional
    def update(Prohibition prohibitionInstance) {
        if (prohibitionInstance == null) {
            notFound()
            return
        }

        if (prohibitionInstance.hasErrors()) {
            respond prohibitionInstance.errors, view:'edit'
            return
        }

        prohibitionInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'Prohibition.label', default: 'Prohibition'), prohibitionInstance.id])
                redirect prohibitionInstance
            }
            '*'{ respond prohibitionInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(Prohibition prohibitionInstance) {

        if (prohibitionInstance == null) {
            notFound()
            return
        }

        prohibitionInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Prohibition.label', default: 'Prohibition'), prohibitionInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'prohibition.label', default: 'Prohibition'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
