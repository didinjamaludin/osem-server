package com.osem



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional
import grails.plugin.springsecurity.annotation.Secured
import grails.converters.JSON

@Secured(['ROLE_HR'])
@Transactional(readOnly = true)
class SubUnitController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond SubUnit.list(params), model:[subUnitInstanceCount: SubUnit.count()]
    }

    def show(SubUnit subUnitInstance) {
        respond subUnitInstance
    }

    def create() {
        respond new SubUnit(params)
    }

    @Transactional
    def save(SubUnit subUnitInstance) {
        if (subUnitInstance == null) {
            notFound()
            return
        }

        if (subUnitInstance.hasErrors()) {
            respond subUnitInstance.errors, view:'create'
            return
        }

        subUnitInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'subUnit.label', default: 'SubUnit'), subUnitInstance.id])
                redirect subUnitInstance
            }
            '*' { respond subUnitInstance, [status: CREATED] }
        }
    }

    def edit(SubUnit subUnitInstance) {
        respond subUnitInstance
    }

    @Transactional
    def update(SubUnit subUnitInstance) {
        if (subUnitInstance == null) {
            notFound()
            return
        }

        if (subUnitInstance.hasErrors()) {
            respond subUnitInstance.errors, view:'edit'
            return
        }

        subUnitInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'SubUnit.label', default: 'SubUnit'), subUnitInstance.id])
                redirect subUnitInstance
            }
            '*'{ respond subUnitInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(SubUnit subUnitInstance) {

        if (subUnitInstance == null) {
            notFound()
            return
        }

        subUnitInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'SubUnit.label', default: 'SubUnit'), subUnitInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }
	
	def getSubUnitFromUnit() {
		def unit = Unit.get(params.id)
		render unit?.subUnits as JSON
	}

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'subUnit.label', default: 'SubUnit'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
