package com.osem



import static org.springframework.http.HttpStatus.*
import java.text.DecimalFormat
import com.osem.auth.User
import com.osem.Person
import grails.transaction.Transactional
import grails.plugin.springsecurity.annotation.Secured

@Secured(['ROLE_HR'])
@Transactional(readOnly = true)
class ApprovalController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def springSecurityService

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond Approval.list(params), model:[approvalInstanceCount: Approval.count()]
    }

    def show(Approval approvalInstance) {
        respond approvalInstance
    }

    def create() {
        def user = User.get(springSecurityService.currentUser.id)
        def date = new Date()
        def month = date.getAt(Calendar.MONTH)
        def rmonth
        switch(month) {
        case 0:
            rmonth = "I"
            break
        case 1:
            rmonth = "II"
            break
        case 2:
            rmonth = "III"
            break
        case 3:
            rmonth = "IV"
            break
        case 4:
            rmonth = "V"
            break
        case 5:
            rmonth = "VI"
            break
        case 6:
            rmonth = "VII"
            break
        case 7:
            rmonth = "VIII"
            break
        case 8:
            rmonth = "IX"
            break
        case 9:
            rmonth = "X"
            break
        case 10:
            rmonth = "XI"
            break
        case 11:
            rmonth = "XII"
        }
        def appr = Approval.last()
        def apprNbr

        if(appr) {
            def appCodes = appr.appCode.split("/")
            Integer aseq = appCodes[4].toInteger()
            def newseq = aseq+1
            if(appCodes[2]==rmonth&&appCodes[3]==date.getAt(Calendar.YEAR).toString()) {
                apprNbr = "APPROVAL/"+user.company.compInitial+"/"+rmonth+"/"+date.getAt(Calendar.YEAR)+"/"+new DecimalFormat("000").format(newseq)
            } else {
                apprNbr = "APPROVAL/"+user.company.compInitial+"/"+rmonth+"/"+date.getAt(Calendar.YEAR)+"/001"
            }
        } else {
            apprNbr = "APPROVAL/"+user.company.compInitial+"/"+rmonth+"/"+date.getAt(Calendar.YEAR)+"/001"
        }

        respond new Approval(params), model:[userInstance:user,apprNbr:apprNbr]
    }

    @Transactional
    def save(Approval approvalInstance) {
        if (approvalInstance == null) {
            notFound()
            return
        }

        if (approvalInstance.hasErrors()) {
            respond approvalInstance.errors, view:'create'
            return
        }

        approvalInstance.save flush:true
		
        def lpquery = Person.withCriteria {
            isNotNull("pid")
            maxResults(1)
            order("id","desc")
        }
        def lastperson = lpquery.getAt(0)
        def date = new Date()
        def nik
        if(lastperson) {
            Integer lastnik = lastperson.pid[4..7].toInteger()
            def newlast = lastnik+1
            if(lastperson.pid[2..3]==date.getAt(Calendar.YEAR).toString()[2..3]) {
                nik = "03"+date.getAt(Calendar.YEAR).toString()[2..3]+new DecimalFormat("0000").format(newlast)
            } else {
                nik = "03"+date.getAt(Calendar.YEAR).toString()[2..3]+"0001"
            }
        } else {
            nik = "03"+date.getAt(Calendar.YEAR).toString()[2..3]+"0001"
        }

        def personInstance = Person.get(approvalInstance.person.id)
        personInstance.pid = nik
        personInstance.empStatus = "DITERIMA"
        personInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [
                        message(code: 'approval.label', default: 'Approval'),
                        approvalInstance.id
                    ])
                redirect controller: "hr"
            }
			'*' { respond approvalInstance, [status: CREATED] }
        }
    }

    def edit(Approval approvalInstance) {
        respond approvalInstance
    }

    @Transactional
    def update(Approval approvalInstance) {
        if (approvalInstance == null) {
            notFound()
            return
        }

        if (approvalInstance.hasErrors()) {
            respond approvalInstance.errors, view:'edit'
            return
        }

        approvalInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [
                        message(code: 'Approval.label', default: 'Approval'),
                        approvalInstance.id
                    ])
                redirect approvalInstance
            }
			'*'{ respond approvalInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(Approval approvalInstance) {

        if (approvalInstance == null) {
            notFound()
            return
        }

        approvalInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [
                        message(code: 'Approval.label', default: 'Approval'),
                        approvalInstance.id
                    ])
                redirect action:"index", method:"GET"
            }
			'*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [
                        message(code: 'approval.label', default: 'Approval'),
                        params.id
                    ])
                redirect action: "index", method: "GET"
            }
			'*'{ render status: NOT_FOUND }
        }
    }
}
