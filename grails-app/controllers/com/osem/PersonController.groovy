package com.osem



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional
import grails.plugin.springsecurity.annotation.Secured
import com.osem.auth.User
import java.text.DecimalFormat

@Secured(['ROLE_HR'])
@Transactional(readOnly = true)
class PersonController {

	static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

	def springSecurityService

	def index(Integer max) {
		params.max = Math.min(max ?: 10, 100)
		respond Person.list(params), model:[personInstanceCount: Person.count()]
	}

	def show(Person personInstance) {
		respond personInstance
	}

	def create() {
		def user = User.get(springSecurityService.currentUser.id)

//		def lastperson = Person.last()
//		def date = new Date()
//		def nik
//		if(lastperson) {
//			Integer lastnik = lastperson.pid[4..7].toInteger()
//			def newlast = lastnik+1
//			if(lastperson.pid[2..3]==date.getAt(Calendar.YEAR).toString()[2..3]) {
//				nik = "03"+date.getAt(Calendar.YEAR).toString()[2..3]+new DecimalFormat("0000").format(newlast)
//			} else {
//				nik = "03"+date.getAt(Calendar.YEAR).toString()[2..3]+"0001"
//			}
//		} else {
//			nik = "03"+date.getAt(Calendar.YEAR).toString()[2..3]+"0001"
//		}

		respond new Person(params),model:[company:user.company]
	}

	@Transactional
	def save(Person personInstance) {
		if (personInstance == null) {
			notFound()
			return
		}

		if (personInstance.hasErrors()) {
			respond personInstance.errors, view:'create'
			return
		}

		personInstance.save flush:true

		request.withFormat {
			form multipartForm {
				flash.message = message(code: 'default.created.message', args: [
					message(code: 'person.label', default: 'Person'),
					personInstance.id
				])
				redirect personInstance
			}
			'*' { respond personInstance, [status: CREATED] }
		}
	}

	def edit(Person personInstance) {
		respond personInstance
	}

	@Transactional
	def update(Person personInstance) {
		if (personInstance == null) {
			notFound()
			return
		}

		if (personInstance.hasErrors()) {
			respond personInstance.errors, view:'edit'
			return
		}

		personInstance.save flush:true

		request.withFormat {
			form multipartForm {
				flash.message = message(code: 'default.updated.message', args: [
					message(code: 'Person.label', default: 'Person'),
					personInstance.id
				])
				redirect personInstance
			}
			'*'{ respond personInstance, [status: OK] }
		}
	}

	@Transactional
	def delete(Person personInstance) {

		if (personInstance == null) {
			notFound()
			return
		}

		personInstance.delete flush:true

		request.withFormat {
			form multipartForm {
				flash.message = message(code: 'default.deleted.message', args: [
					message(code: 'Person.label', default: 'Person'),
					personInstance.id
				])
				redirect action:"index", method:"GET"
			}
			'*'{ render status: NO_CONTENT }
		}
	}

	protected void notFound() {
		request.withFormat {
			form multipartForm {
				flash.message = message(code: 'default.not.found.message', args: [
					message(code: 'person.label', default: 'Person'),
					params.id
				])
				redirect action: "index", method: "GET"
			}
			'*'{ render status: NOT_FOUND }
		}
	}
}
