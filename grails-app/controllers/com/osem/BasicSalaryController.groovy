package com.osem



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional
import grails.plugin.springsecurity.annotation.Secured

@Secured(['ROLE_HR'])
@Transactional(readOnly = true)
class BasicSalaryController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond BasicSalary.list(params), model:[basicSalaryInstanceCount: BasicSalary.count()]
    }

    def show(BasicSalary basicSalaryInstance) {
        respond basicSalaryInstance
    }

    def create() {
        respond new BasicSalary(params)
    }

    @Transactional
    def save(BasicSalary basicSalaryInstance) {
        if (basicSalaryInstance == null) {
            notFound()
            return
        }

        if (basicSalaryInstance.hasErrors()) {
            respond basicSalaryInstance.errors, view:'create'
            return
        }
		
//		basicSalaryInstance.empBenefit = basicSalaryInstance.basicSalary + basicSalaryInstance.mealAllow + basicSalaryInstance.transAllow + basicSalaryInstance.commAllow + basicSalaryInstance.uniformAllow + basicSalaryInstance.toolsEquipment
//		basicSalaryInstance.subTotal = basicSalaryInstance.empBenefit + basicSalaryInstance.jamsostek + basicSalaryInstance.bpjsKesehatan
//		basicSalaryInstance.manFee = (basicSalaryInstance.subTotal * basicSalaryInstance.fee)/100
//		basicSalaryInstance.totalClaim = basicSalaryInstance.subTotal + basicSalaryInstance.manFee
//		basicSalaryInstance.ppn10 = (basicSalaryInstance.manFee * 10)/100
//		basicSalaryInstance.grandTotal = basicSalaryInstance.totalClaim + basicSalaryInstance.ppn10
//		basicSalaryInstance.pph23 = (basicSalaryInstance.manFee * 2)/100
//		basicSalaryInstance.netClaimed = (basicSalaryInstance.grandTotal - basicSalaryInstance.pph23) * basicSalaryInstance.empCount

        basicSalaryInstance.save flush:true
		
		

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'basicSalary.label', default: 'BasicSalary'), basicSalaryInstance.id])
                redirect basicSalaryInstance
            }
            '*' { respond basicSalaryInstance, [status: CREATED] }
        }
    }

    def edit(BasicSalary basicSalaryInstance) {
        respond basicSalaryInstance
    }

    @Transactional
    def update(BasicSalary basicSalaryInstance) {
        if (basicSalaryInstance == null) {
            notFound()
            return
        }

        if (basicSalaryInstance.hasErrors()) {
            respond basicSalaryInstance.errors, view:'edit'
            return
        }
		
//		basicSalaryInstance.empBenefit = basicSalaryInstance.basicSalary + basicSalaryInstance.mealAllow + basicSalaryInstance.transAllow + basicSalaryInstance.commAllow + basicSalaryInstance.uniformAllow + basicSalaryInstance.toolsEquipment
//		basicSalaryInstance.subTotal = basicSalaryInstance.empBenefit + basicSalaryInstance.jamsostek + basicSalaryInstance.bpjsKesehatan
//		basicSalaryInstance.manFee = (basicSalaryInstance.subTotal * basicSalaryInstance.fee)/100
//		basicSalaryInstance.totalClaim = basicSalaryInstance.subTotal + basicSalaryInstance.manFee
//		basicSalaryInstance.ppn10 = (basicSalaryInstance.manFee * 10)/100
//		basicSalaryInstance.grandTotal = basicSalaryInstance.totalClaim + basicSalaryInstance.ppn10
//		basicSalaryInstance.pph23 = (basicSalaryInstance.manFee * 2)/100
//		basicSalaryInstance.netClaimed = (basicSalaryInstance.grandTotal - basicSalaryInstance.pph23) * basicSalaryInstance.empCount

        basicSalaryInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'BasicSalary.label', default: 'BasicSalary'), basicSalaryInstance.id])
                redirect basicSalaryInstance
            }
            '*'{ respond basicSalaryInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(BasicSalary basicSalaryInstance) {

        if (basicSalaryInstance == null) {
            notFound()
            return
        }

        basicSalaryInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'BasicSalary.label', default: 'BasicSalary'), basicSalaryInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'basicSalary.label', default: 'BasicSalary'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
