package com.osem



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional
import grails.plugin.springsecurity.annotation.Secured

@Secured(['ROLE_HR'])
@Transactional(readOnly = true)
class MutationController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond Mutation.list(params), model:[mutationInstanceCount: Mutation.count()]
    }

    def show(Mutation mutationInstance) {
        respond mutationInstance
    }

    def create() {
        respond new Mutation(params)
    }

    @Transactional
    def save(Mutation mutationInstance) {
        if (mutationInstance == null) {
            notFound()
            return
        }

        if (mutationInstance.hasErrors()) {
            respond mutationInstance.errors, view:'create'
            return
        }

        mutationInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'mutation.label', default: 'Mutation'), mutationInstance.id])
                redirect mutationInstance
            }
            '*' { respond mutationInstance, [status: CREATED] }
        }
    }

    def edit(Mutation mutationInstance) {
        respond mutationInstance
    }

    @Transactional
    def update(Mutation mutationInstance) {
        if (mutationInstance == null) {
            notFound()
            return
        }

        if (mutationInstance.hasErrors()) {
            respond mutationInstance.errors, view:'edit'
            return
        }

        mutationInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'Mutation.label', default: 'Mutation'), mutationInstance.id])
                redirect mutationInstance
            }
            '*'{ respond mutationInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(Mutation mutationInstance) {

        if (mutationInstance == null) {
            notFound()
            return
        }

        mutationInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Mutation.label', default: 'Mutation'), mutationInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'mutation.label', default: 'Mutation'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
