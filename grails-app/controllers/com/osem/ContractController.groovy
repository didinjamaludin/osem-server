package com.osem



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional
import grails.plugin.springsecurity.annotation.Secured
import com.osem.auth.User

@Secured(['ROLE_HR'])
@Transactional(readOnly = true)
class ContractController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]
    
    def springSecurityService

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond Contract.list(params), model:[contractInstanceCount: Contract.count()]
    }

    def show(Contract contractInstance) {
        respond contractInstance
    }

    def create() {
        respond new Contract(params)
    }

    @Transactional
    def save(Contract contractInstance) {
        def user = User.get(springSecurityService.currentUser.id)
        
        if (contractInstance == null) {
            notFound()
            return
        }

        if (contractInstance.hasErrors()) {
            respond contractInstance.errors, view:'create'
            return
        }

        contractInstance.save flush:true
		
        def personInstance = Person.get(contractInstance.person.id)
        personInstance.empStatus = "KONTRAK"
        personInstance.save flush:true,failOnError:true
        
        def assigmentInstance = Assigment.withCriteria {
            eq('person', personInstance)
            order('assDate','desc')
            maxResults(1)
        }
        
        def salaryInstance = BasicSalary.findByUnit(assigmentInstance[0].unit)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'contract.label', default: 'Contract'), contractInstance.id])
                if(params.printCont=="on") {
                    render view: "printCont",model:[userInstance:user, contractInstance:contractInstance, salaryInstance:salaryInstance]
                } else {
                    redirect controller: "hr"
                }
            }
            '*' { respond contractInstance, [status: CREATED] }
        }
    }
    
    def printContract(Long id) {
        def contractInstance = Contract.get(id)
        def user = User.get(springSecurityService.currentUser.id)
        def assigmentInstance = Assigment.withCriteria {
            eq('person', Person.get(contractInstance.person.id))
            order('assDate','desc')
            maxResults(1)
        }
        
        def salaryInstance = BasicSalary.findByUnit(assigmentInstance[0].unit)
        
        render view: "printCont",model:[userInstance:user, contractInstance:contractInstance, salaryInstance:salaryInstance]
    }

    def edit(Contract contractInstance) {
        respond contractInstance
    }

    @Transactional
    def update(Contract contractInstance) {
        if (contractInstance == null) {
            notFound()
            return
        }

        if (contractInstance.hasErrors()) {
            respond contractInstance.errors, view:'edit'
            return
        }

        contractInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'Contract.label', default: 'Contract'), contractInstance.id])
                redirect contractInstance
            }
            '*'{ respond contractInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(Contract contractInstance) {

        if (contractInstance == null) {
            notFound()
            return
        }

        contractInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Contract.label', default: 'Contract'), contractInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'contract.label', default: 'Contract'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
