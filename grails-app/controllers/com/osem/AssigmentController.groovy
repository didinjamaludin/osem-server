package com.osem



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional
import java.text.DecimalFormat
import com.osem.auth.User
import grails.plugin.springsecurity.annotation.Secured

@Secured(['ROLE_HR'])
@Transactional(readOnly = true)
class AssigmentController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]
	
    def springSecurityService

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond Assigment.list(params), model:[assigmentInstanceCount: Assigment.count()]
    }

    def show(Assigment assigmentInstance) {
        respond assigmentInstance
    }

    def create() {
        def user = User.get(springSecurityService.currentUser.id)
        def date = new Date()
        def month = date.getAt(Calendar.MONTH)
        def rmonth
        switch(month) {
        case 0:
            rmonth = "I"
            break
        case 1:
            rmonth = "II"
            break
        case 2:
            rmonth = "III"
            break
        case 3:
            rmonth = "IV"
            break
        case 4:
            rmonth = "V"
            break
        case 5:
            rmonth = "VI"
            break
        case 6:
            rmonth = "VII"
            break
        case 7:
            rmonth = "VIII"
            break
        case 8:
            rmonth = "IX"
            break
        case 9:
            rmonth = "X"
            break
        case 10:
            rmonth = "XI"
            break
        case 11:
            rmonth = "XII"
        }
        def assg = Assigment.last()
        def assgNbr

        if(assg) {
            def assCodes = assg.assCode.split("/")
            Integer aseq = assCodes[0].toInteger()
            def newseq = aseq+1
            if(assCodes[2]==rmonth&&assCodes[3]==date.getAt(Calendar.YEAR).toString()) {
                assgNbr = new DecimalFormat("000").format(newseq)+"/SPT-"+user.company.compInitial+"/"+rmonth+"/"+date.getAt(Calendar.YEAR)
            } else {
                assgNbr = "001/SPT-"+user.company.compInitial+"/"+rmonth+"/"+date.getAt(Calendar.YEAR)
            }
        } else {
            assgNbr = "001/SPT-"+user.company.compInitial+"/"+rmonth+"/"+date.getAt(Calendar.YEAR)
        }
		
        respond new Assigment(params), model:[userInstance:user,assgNbr:assgNbr]
    }

    @Transactional
    def save(Assigment assigmentInstance) {
        def user = User.get(springSecurityService.currentUser.id)
        
        if (assigmentInstance == null) {
            notFound()
            return
        }

        if (assigmentInstance.hasErrors()) {
            respond assigmentInstance.errors, view:'create'
            return
        }

        assigmentInstance.save flush:true
		
        def personInstance = Person.get(assigmentInstance.person.id)
        personInstance.unit = assigmentInstance.unit
        personInstance.subUnit = assigmentInstance.subunit
        personInstance.dept = assigmentInstance.dept
        personInstance.position = assigmentInstance.position
        personInstance.save flush:true,failOnError:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'assigment.label', default: 'Assigment'), assigmentInstance.id])
                if(params.printAssg=="on") {
                    render view:"printAssigment",model:[userInstance:user, assigmentInstance:assigmentInstance]
                } else {
                    redirect controller: "hr"
                }
            }
            '*' { respond assigmentInstance, [status: CREATED] }
        }
    }

    def edit(Assigment assigmentInstance) {
        respond assigmentInstance
    }

    @Transactional
    def update(Assigment assigmentInstance) {
        if (assigmentInstance == null) {
            notFound()
            return
        }

        if (assigmentInstance.hasErrors()) {
            respond assigmentInstance.errors, view:'edit'
            return
        }

        assigmentInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'Assigment.label', default: 'Assigment'), assigmentInstance.id])
                redirect assigmentInstance
            }
            '*'{ respond assigmentInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(Assigment assigmentInstance) {

        if (assigmentInstance == null) {
            notFound()
            return
        }

        assigmentInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Assigment.label', default: 'Assigment'), assigmentInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'assigment.label', default: 'Assigment'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
