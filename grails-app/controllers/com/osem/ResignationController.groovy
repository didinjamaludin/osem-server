package com.osem



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional
import grails.plugin.springsecurity.annotation.Secured

@Secured(['ROLE_HR'])
@Transactional(readOnly = true)
class ResignationController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond Resignation.list(params), model:[resignationInstanceCount: Resignation.count()]
    }

    def show(Resignation resignationInstance) {
        respond resignationInstance
    }

    def create() {
        respond new Resignation(params)
    }

    @Transactional
    def save(Resignation resignationInstance) {
        if (resignationInstance == null) {
            notFound()
            return
        }

        if (resignationInstance.hasErrors()) {
            respond resignationInstance.errors, view:'create'
            return
        }

        resignationInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'resignation.label', default: 'Resignation'), resignationInstance.id])
                redirect resignationInstance
            }
            '*' { respond resignationInstance, [status: CREATED] }
        }
    }

    def edit(Resignation resignationInstance) {
        respond resignationInstance
    }

    @Transactional
    def update(Resignation resignationInstance) {
        if (resignationInstance == null) {
            notFound()
            return
        }

        if (resignationInstance.hasErrors()) {
            respond resignationInstance.errors, view:'edit'
            return
        }

        resignationInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'Resignation.label', default: 'Resignation'), resignationInstance.id])
                redirect resignationInstance
            }
            '*'{ respond resignationInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(Resignation resignationInstance) {

        if (resignationInstance == null) {
            notFound()
            return
        }

        resignationInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Resignation.label', default: 'Resignation'), resignationInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'resignation.label', default: 'Resignation'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
