package com.osem

class Contract {
	
    static belongsTo = [person:Person]
    Integer contractLength
    Date startDate
    Date endDate
    String occupation
    Position position
    String contractNotes
    Date createDate

    static constraints = {
        createDate nullable: true
    }
    
    static mapping = {
        person index:true
    }
	
    def beforeInsert() {
        createDate = new Date()
    }
}
