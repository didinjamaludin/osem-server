package com.osem

class Warning {
	
    static belongsTo = [person:Person]
    String warnCode
    WarningType warnType
    Date warnStart
    Date warnEnd
    String investigator
    String investPosition
    String warnNotes
    Date createDate

    static constraints = {
    }
    
    static mapping = {
        person index:true
    }
}
