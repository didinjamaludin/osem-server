package com.osem

class PreStateTask {
	
    static belongsTo = [person:Person]
    Person person
    String pstCode
    Date pstDate

    static constraints = {
        pstDate nullable:true
    }
    
    static mapping = {
        person index:true
    }
	
    def beforeInsert() {
        pstDate = new Date()
    }
}
