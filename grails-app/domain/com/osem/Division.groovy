package com.osem

class Division {
	
    String divCode
    String divName

    static constraints = {
    }
    
    static mapping = {
        divCode index:true
        divName index:true
    }
}
