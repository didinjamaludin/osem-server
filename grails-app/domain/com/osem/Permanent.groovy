package com.osem

import java.util.Date;

class Permanent {
	
    static belongsTo = [person:Person]
    Date permDate
    String permNotes
    Date createDate

    static constraints = {
    }
    
    static mapping = {
        person index:true
    }
}
