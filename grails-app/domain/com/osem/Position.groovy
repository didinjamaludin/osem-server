package com.osem

class Position {
	
    String posName

    static constraints = {
    }
    
    static mapping = {
        posName index:true
    }
	
    String toString() {
        posName
    }
}
