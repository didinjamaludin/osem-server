package com.osem

class Person {

    Company company
    Unit unit
    SubUnit subUnit
    String pid
    String fullname
    Position position
    Dept dept
    String idAddr
    String idCity
    String currentAddr
    String currentCity
    String postCode
    String phone
    String hp
    String email
    String idType	// KTP, SIM, PASSPORT
    String idNumber
    String bloodGroup
    String pob
    Date dob
    Integer height
    Integer weight
    String nationality
    String race
    String religion
    String gender
    String maritalStatus
    String empStatus	// PELAMAR, DITERIMA, PERCOBAAN, KONTRAK, TETAP, MAGANG, FREELANCE
    Date createDate
    static hasMany = [languages:Language,educations:Education,families:Family,workHistories:WorkHistory,contracts:Contract,assigments:Assigment,warnings:Warning,mutations:Mutation,promotions:Promotion]
    static hasOne = [approval:Approval,permanent:Permanent,preStateTask:PreStateTask,termination:Termination,prohibition:Prohibition,resignation:Resignation]


    static constraints = {
        unit nullable:true
        subUnit nullable:true
        pid nullable:true
        fullname blank:false
        position nullable: true
        dept nullable: true
        idAddr nullable: true
        idCity nullable: true
        currentAddr nullable: true
        currentCity nullable: true
        postCode nullable: true
        phone nullable: true
        hp nullable: true
        email nullable: true
        idType blank: false
        idNumber blank: false
        bloodGroup blank: false
        pob blank: false
        dob blank: false
        height blank: false
        weight blank: false
        nationality blank: false
        race blank: false
        religion blank: false
        gender blank: false
        maritalStatus blank: false
        empStatus nullable: true
        createDate nullable: true
        approval nullable: true
        permanent nullable: true
        preStateTask nullable: true
        prohibition nullable: true
        resignation nullable: true
        termination nullable: true
    }
	
    def beforeInsert() {
        createDate = new Date()
    }
    
    static mapping = {
        company index:true
        unit index:true
        subUnit index:true
    }
	 
    String toString() {
        fullname
    }
}
