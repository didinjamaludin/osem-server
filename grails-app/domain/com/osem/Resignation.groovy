package com.osem

class Resignation {
	
    static belongsTo = [person:Person]
    String resCode
    Date resDate
    String resReason
    String resNotes
    Date createDate

    static constraints = {
    }
    
    static mapping = {
        person index:true
    }
}
