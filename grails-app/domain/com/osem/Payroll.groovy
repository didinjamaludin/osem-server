package com.osem

class Payroll {
	
    Person person
    Integer month
    Integer year
    Double basicSalary
    Double mealAllow
    Double transAllow
    Double commAllow
    Double uniformAllow
    Double toolsEquipment
    Double empBenefit
    Integer monthlyAtt
    Integer totalAtt
    Double attCut
    Integer totalOt		// in hours
    Double amountOt		
    Double pph21
    Double jamsostekCut
    Double bpjsCut
    Double otherFee
    Double deposit
    Double takeHomePay
    String notes

    static constraints = {
        totalAtt nullable: true
        totalOt nullable: true
        amountOt nullable: true
        notes nullable:true
    }
    
    static mapping = {
        person index:true
    }
}
