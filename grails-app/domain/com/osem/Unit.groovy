package com.osem

class Unit {
	
    Company company
    String unitCode
    String unitName
    String unitHead
    static hasMany = [subUnits:SubUnit]

    static constraints = {
        unitHead nullable: true
    }
    
    static mapping = {
        company index:true
    }
	
    String toString() {
        unitCode+" - "+unitName
    }
}
