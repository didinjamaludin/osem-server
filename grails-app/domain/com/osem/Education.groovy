package com.osem

class Education {

    static belongsTo = [person:Person]
    String eduLevel
    String institution
    Integer yearIn
    Integer yearOut
    String faculty
    String majors
    String title

    static constraints = {
        eduLevel inList:['SD','SMP','SMU','D1','D3','S1','S2','S3']
        faculty nullable:true
        majors nullable:true
        title nullable:true
    }
    
    static mapping = {
        person index:true
    }
	
    String toString() {
        institution
    }
}
