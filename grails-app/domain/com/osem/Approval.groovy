package com.osem

class Approval {
	
    static belongsTo = [person:Person]
	
    String appCode
    Date appDate
    String testScore
    String appNotes

    static constraints = {
        appDate nullable:true
        appCode nullable:true
        appNotes nullable:true
        testScore nullable:true
    }
    
    static mapping = {
        person index:true
        appCode index:true
    }
	
    def beforeInsert() {
        appDate = new Date()
    }
}
