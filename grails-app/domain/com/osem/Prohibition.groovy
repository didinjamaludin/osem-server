package com.osem

class Prohibition {
	
    static belongsTo = [person:Person]
	
    String probCode
    Integer probLength
    Date probStart
    Date probEnd
    String probNotes
    Date createDate

    static constraints = {
        probCode nullable:true
        probLength nullable:true
        probStart nullable:true
        probEnd nullable:true
        probNotes nullable:true
        createDate nullable:true
    }
    
    static mapping = {
        person index:true
    }
	
    def beforeInsert() {
        createDate = new Date()
    }
}
