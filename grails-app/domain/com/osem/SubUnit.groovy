package com.osem

class SubUnit {
	
    static belongsTo = [unit:Unit]
    String subUnitCode
    String subUnitName
    String subUnitAddress
    static hasMany = [persons:Person]

    static constraints = {
        subUnitName nullable: true
        subUnitAddress nullable: true
    }
    
    static mapping = {
        unit index:true
    }
	
    String toString() {
        subUnitCode
    }
}
