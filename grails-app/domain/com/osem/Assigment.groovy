package com.osem

class Assigment {
	
    static belongsTo = [person:Person]
    String assCode
    Date assDate
    Integer assGroup
    Unit unit
    SubUnit subunit
    Dept dept
    Position position
    String assStatus    // baru, pengganti
    String repName      // nama yg di gantikan

    static constraints = {
        assGroup nullable:true
        repName nullable:true
    }
    
    static mapping = {
        person index:true
        assCode index:true
    }
}
