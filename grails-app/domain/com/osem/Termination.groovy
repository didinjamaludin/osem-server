package com.osem

class Termination {
	
    static belongsTo = [person:Person]
    String termCode
    Date termDate
    Date investDate
    String investigator
    String investPosition
    String termNotes
    Date createDate

    static constraints = {
    }
    
    static mapping = {
        person index:true
    }
}
