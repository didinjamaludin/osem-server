package com.osem

class Mutation {
	
    static belongsTo = [person:Person]
    String mutCode
    Date mutDate
    SubUnit fromSubUnit
    SubUnit toSubUnit
    String oldDept
    String newDept
    String mutNotes
    Date createDate

    static constraints = {
    }
    
    static mapping = {
        person index:true
    }
}
