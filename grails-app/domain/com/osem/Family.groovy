package com.osem

class Family {

    static belongsTo = [person:Person]
    String famName
    String famRelationship
    String famGender
    String famAge
    String famEducation
    String famJob
    String famPhone

    static constraints = {
        famAge nullable:true
        famEducation nullable:true
        famJob nullable:true
        famPhone nullable:true
    }
    
    static mapping = {
        person index:true
    }

    String toString() {
        famName+" - "+famRelationship
    }
}
