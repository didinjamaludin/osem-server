package com.osem

import com.osem.auth.User

class Company {
	
    String compInitial
    String compName
    String compAddress
    String compCity
    String compPhone
    String compLogo
    String compNPWP
    String compEmail
    String compWebsite

    static constraints = {
        compLogo nullable: true
        compNPWP nullable: true
        compWebsite nullable: true
    }
    
    static mapping = {
        compInitial index:true
        compName index:true
    }
}
