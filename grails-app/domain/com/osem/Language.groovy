package com.osem

class Language {
	
    static belongsTo = [person:Person]
    String langName
    String speakLevel
    String listenLevel
    String writeLevel

    static constraints = {
        speakLevel inList:['Kurang','Sedang','Baik']
        listenLevel inList:['Kurang','Sedang','Baik']
        writeLevel inList:['Kurang','Sedang','Baik']
    }
    
    static mapping = {
        person index:true
    }
	
    String toString() {
        langName
    }
}
