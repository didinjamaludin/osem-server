package com.osem

class WorkHistory {
	
    static belongsTo = [person:Person]
    String workInstitution
    String workAddress
    String workIn
    String workOut
    String workPosition
    Double lastSalary

    static constraints = {
        workAddress nullable:true
        workIn nullable:true
        workOut nullable:true
        workPosition nullable:true
        lastSalary nullable:true
    }
    
    static mapping = {
        person index:true
    }
	
    String toString() {
        workInstitution
    }
}
