package com.osem

class Promotion {
	
    static belongsTo = [person:Person]
    String promCode
    Date promDate
    String oldPosition
    String newPosition
    SubUnit oldSubUnit
    SubUnit newSubUnit
    String promNotes
    Date createDate

    static constraints = {
    }
    
    static mapping = {
        person index:true
    }
}
