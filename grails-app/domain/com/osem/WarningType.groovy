package com.osem

class WarningType {
	
    String warningType

    static constraints = {
    }
    
    static mapping = {
        warningType index:true
    }
}
