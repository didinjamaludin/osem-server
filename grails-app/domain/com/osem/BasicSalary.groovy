package com.osem

class BasicSalary {
	
    Unit unit
    Position position
    Double basicSalary
    Double mealAllow
    Double transAllow
    Double commAllow
    Double uniformAllow
    Double toolsEquipment
    boolean overtime
    Double empBenefit    // basic + trans + comm + meal + uniform + tools + overtime
    Double jamsostek
    Double bpjsKesehatan
    boolean thr
    Double subTotal		 // empBenefit + jamsostek + bpjs + thr
    Double fee 			 // %
    Double manFee		 // (subTotal * fee)/100
    Double totalClaim	 // subTotal + manFee
    Double ppn10		 // (manFee * 10)/100
    Double grandTotal	 // totalClaim + ppn10
    Double pph23		 // (manFee * 2)/100
    Double empCount		 // jumlah tenaga kerja
    Double netClaimed	 // (grandTotal - pph23) * empCount 
    Double pph21
    Double jamsostekCut
    Double bpjsCut
    Double otherFee
    Double deposit
    Double takeHomePay			// empBenefit - (pph21 + jamsostek + bpjs + others + deposit)

    static constraints = {
    }
    
    static mapping = {
        unit index:true
        position index:true
    }
}
