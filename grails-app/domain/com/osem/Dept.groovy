package com.osem

class Dept {

    String deptName

    static constraints = {
    }
    
    static mapping = {
        deptName index:true
    }
	
    String toString() {
        deptName
    }
}
