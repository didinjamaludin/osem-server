package com.osem

import java.util.List;

import grails.transaction.Transactional
import grails.plugin.springsecurity.SpringSecurityUtils
import grails.plugin.springsecurity.userdetails.GrailsUser
import grails.plugin.springsecurity.userdetails.GrailsUserDetailsService

import org.springframework.security.core.authority.GrantedAuthorityImpl
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UsernameNotFoundException

import com.osem.auth.User

@SuppressWarnings("deprecation")
@Transactional
class MyUserDetailsService implements GrailsUserDetailsService {

	static final List NO_ROLES = [new GrantedAuthorityImpl(SpringSecurityUtils.NO_ROLE)]

	UserDetails loadUserByUsername(String username, boolean loadRoles)
	throws UsernameNotFoundException {
		return loadUserByUsername(username)
	}

	UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

		User.withTransaction { status ->

			User user = User.findByUsername(username)
			if (!user) throw new UsernameNotFoundException(
				'User not found', username)

			def authorities = user.authorities.collect {
				new GrantedAuthorityImpl(it.authority)
			}

			return new MyUserDetails(user.username, user.password, user.enabled,
					!user.accountExpired, !user.passwordExpired,
					!user.accountLocked, authorities ?: NO_ROLES, user.id,
					user.fullname)
		}
	}
}
