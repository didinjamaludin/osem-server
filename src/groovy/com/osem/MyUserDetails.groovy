package com.osem

import grails.plugin.springsecurity.userdetails.GrailsUser

import org.springframework.security.core.GrantedAuthority 
import org.springframework.security.core.userdetails.User
import com.osem.Company

class MyUserDetails extends GrailsUser {
    final String fullname

    MyUserDetails(String username, String password, boolean enabled,
        boolean accountNonExpired, boolean credentialsNonExpired,
        boolean accountNonLocked,
        Collection<GrantedAuthority> authorities,
        long id, String fullname) {
        
        super(username, password, enabled, accountNonExpired,
            credentialsNonExpired, accountNonLocked, authorities, id)
        
        this.fullname = fullname
    }
}